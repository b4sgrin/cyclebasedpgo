//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef PGO_PROJECT_LINEARLEASTSQUARES_H
#define PGO_PROJECT_LINEARLEASTSQUARES_H

#define __SLAM_PLUS_PLUS_ECPGO_COLUMN_MAJOR_ALLOCATE_SYSTEM_MATRIX

#include <iostream>
#include <map>
#include <vector>


/**@brief linear solver, block cholesky */
#include "slam/LinearSolver_UberBlock.h"


#include "slam/FlatSystem.h"


/**@brief DeductionPlan is used to construct Hessian from Jacobian.
 * We can simplify reuse the deduction plan in the file:
 *  EC-PGO/ECPGO_DeductionPlan.h
 */
#include "ECPGO_DeductionPlan.h"



/**@brief Implementation to solve linear least squares
 *
 *    Sum || A_1 * x_1  +  A_2 * x_2  - b ||
 *    Here, we assume A_1, A_2 have same dimension
 *    x_1, x_2 have same dimension.
 *    Namely, A_1, A_2 are n*n matrices, x_1, x_2 are n*1 vectors.
 */


template <const int t_m_n_dimension_const>
class LinearLeastSquares
{
private:

    enum
    {
        n_pool_page_size = 4096, /**< @brief deduction pool page size */
        n_pool_memory_align = 0,  /**< @brief deduction pool memory alignment */ // see no benefit in alignment right now, this stores the elements, those do not need to be aligned
        m_n_dimension = t_m_n_dimension_const
    };


public:

    /**< @brief Jacobian matrix type */
    typedef Eigen::Matrix<double, t_m_n_dimension_const, t_m_n_dimension_const, Eigen::ColMajor> _TyMatrix;

    typedef Eigen::Matrix<double, t_m_n_dimension_const, 1, Eigen::ColMajor> _TyVector;


protected:

    class TEdge
    {
    public:

        typedef typename LinearLeastSquares::_TyMatrix _TyJacobianMatrix;  // interface typedef to deduction plan

        enum {n_dimension = t_m_n_dimension_const}; // interface variable to DeductionPlan

        int m_n_end_nodes[2];

        _TyMatrix A1;

        _TyMatrix A2;

        _TyVector b;

    public:

        TEdge(){}

        ~TEdge(){}

        TEdge(int t_id1, int t_id2, _TyMatrix t_A1, _TyMatrix t_A2, _TyVector t_b)
            : A1(t_A1), A2(t_A2), b(t_b)
        {
            m_n_end_nodes[0] = t_id1;
            m_n_end_nodes[1] = t_id2;
        }

    };


    class TVertex
    {
    public:

        int m_n_order; // for a fixed vertex, set m_n_order to negative

        _TyVector x;

    public:

        TVertex () : m_n_order(-1) {}

        ~TVertex(){}

        TVertex (int t_n_order) : m_n_order (t_n_order) {}
    };

protected:

    typedef typename MakeTypelist(TEdge) _TyEdgeList; /**<@brief TypeList with EdgeType */
    typedef typename MakeTypelist(TVertex) _TyVertexList; /**<@brief TypeList with VertexType */


    /** @brief CMultiPool types for edge, cycle, vertex */
    typedef multipool::CMultiPool<TEdge, _TyEdgeList, n_pool_page_size> _TyEdgePool; /**<@brief edge pool type */
    typedef multipool::CMultiPool<TVertex, _TyVertexList, n_pool_page_size> _TyVertexPool; /**<@brief vertex pool type */


    typedef CECPGO_DeductionPlan<TEdge> _TyDeductionPlan;

    typedef typename MakeTypelist(_TyMatrix) _TyMatrixList;

    typedef CLinearSolver_UberBlock<_TyMatrixList> CLinearSolverType; /**<@brief Linear Solver Type */


private:

    _TyEdgePool m_edge_pool;

    _TyVertexPool m_vertex_pool;

    _TyDeductionPlan m_deduction_plan;

    CUberBlockMatrix m_m_SystemMatrix; /**<@brief system matrix storage */

    Eigen::VectorXd m_v_RightHandSide;

    CLinearSolverType m_solver;

    int m_n_num_edges;

    int m_n_num_vertices;


private:

    /** @brief deduce non-zero entries in the final sytem matrix */
    void Deduce_SystemMatrixBlocks()
    {
        m_v_RightHandSide.setZero();

        for (int i = 0, n = (int) m_edge_pool.n_Size(); i < n; ++i)
        {
            TEdge & e = m_edge_pool[i];

            int ci = m_vertex_pool[ e.m_n_end_nodes[0] ].m_n_order;
            int cj = m_vertex_pool[ e.m_n_end_nodes[1] ].m_n_order;

            // this implies four nonzero blocks
            //  <ci, ci>, <ci, cj>
            //  <cj, ci>, <cj, cj>
            // We only need to assign upper-trangular blocks in system matrix
            //  <ci, ci>, <ci, cj>
            //            <cj, cj>
            // Here ci < cj, so that its upper-trangular.

            /**@note To ensure this, we have to force ci < cj in TEdge. */

            /**@brief To accommodate block sortings,
             * in the AddProductENtry function,
             * first input is column, second input is row,
             * AddProductEntry: (col ,row)  */

            // thus, we need to input
            // <ci, ci>, <cj, ci>, <cj,cj> instead.

            // In function, the contribution to system matrix is calculated as:
            // dest_map +=
            // _TyMapMatrix(r_red.src_list[j].second).transpose()
            // _TyMapMatrix(r_red.src_list[j].first);
            // Namely, Matrix2.transpose() * Matrix1

            // The contribution we want to add is:
            // H =sum (Jk^T * Jk), with Jk = [... A1 ... A2 ...]
            // Jk^T * Jk =
            // |  ... A1^T * A1   ...   A1^T * A2 ...  |
            // |  ... A2^T * A1   ...   A2^T * A2 ...  |

            // Therefore, we arrange the input to AddProductEntry() as follow:
            // <ci, ci> : A1, A1; => (ci, ci): A1^T * A1
            // <cj, ci> : A2, A1; => (ci, cj): A1^T * A2
            // <cj, cj> : A2, A2; => (cj, cj): A2^T * A2

            if(ci < 0 && cj < 0)
            {
                printf("\nfatal error: both variables in a constraint are fixed.!\n");
                exit(1);
            }

            if(ci >= 0)
                m_deduction_plan.AddProductEntry(std::pair<size_t, size_t>(ci, ci),
                                std::pair<double*, double*>(e.A1.data(), e.A1.data()));

            if(ci>=0 && cj>=0)
                m_deduction_plan.AddProductEntry(std::pair<size_t, size_t>(cj, ci),
                                std::pair<double*, double*>(e.A2.data(), e.A1.data()));

            if(cj>=0)
                m_deduction_plan.AddProductEntry(std::pair<size_t, size_t>(cj, cj),
                                std::pair<double*, double*>(e.A2.data(), e.A2.data()));


            if(ci >= 0 && cj >=0)
            {
                m_v_RightHandSide.segment<m_n_dimension>(ci) += e.A1.transpose() * e.b;
                m_v_RightHandSide.segment<m_n_dimension>(cj) += e.A2.transpose() * e.b;
            }
            else if (ci >= 0 && cj < 0)
            {
                _TyVector bb = e.b - e.A2 * m_vertex_pool[e.m_n_end_nodes[1]].x;
                m_v_RightHandSide.segment<m_n_dimension>(ci) += e.A1.transpose() * bb;
            }
            else if (ci < 0 && cj >= 0)
            {
                _TyVector bb = e.b - e.A1 * m_vertex_pool[e.m_n_end_nodes[0]].x;
                m_v_RightHandSide.segment<m_n_dimension>(cj) += e.A2.transpose() * bb;
            }

//            printf("process edge (%d, %d)\n", ci, cj);

        }
    }




public:

    LinearLeastSquares()
            : m_n_num_edges(0), m_n_num_vertices(0)
    {}


    int edgeNum ()
    {
        return m_n_num_edges;
    }


    int vertexNum ()
    {
        return m_n_num_vertices;
    }


    bool addEdge (int id1, int id2, const _TyMatrix & A1, const _TyMatrix & A2, const _TyVector & b)
    {
        if (id1 < 0 || id2 < 0 || id1 == id2)
            return false;

        // augment vertex pool if vertex does not exist
        while (std::max(id1, id2) > m_n_num_vertices -1)
        {
            // these vertices may never appear later on.
            // set to null
            TVertex vertex(-1);
            m_vertex_pool.r_Add_Element( vertex );
            ++ m_n_num_vertices;
        }

        m_vertex_pool[id1].m_n_order = 0; // appeared vertex
        m_vertex_pool[id2].m_n_order = 0; // appeared vertex

        // make sure TEdge::m_n_end_nodes[0] < TEdge::m_n_end_nodes[1]
        if (id1 < id2)
        {
            TEdge edge (id1, id2, A1, A2, b);
            m_edge_pool.r_Add_Element( edge );
        }
        else
        {
            TEdge edge (id2, id1, A2, A1, b);
            m_edge_pool.r_Add_Element( edge );
        }

        ++ m_n_num_edges;
        return true;
    }


    void fixVertex (int id, const _TyVector & xv)
    {
        m_vertex_pool[id].m_n_order = -1;
        m_vertex_pool[id].x = xv;
//        printf("\nfix vertex [%d] to value: ", id);
//        std::cout << m_vertex_pool[id].x.transpose() << "\n";
    }


    _TyVector & getVertex (int id)
    {
        return m_vertex_pool[id].x;
    }


    bool solve ()
    {
        int n_system_dimension = 0;

        /** allocate m_n_order (i.e., index in Hessian) one by one */
        for (int i = 0, n = (int) m_vertex_pool.n_Size(); i < n; ++i)
        {
            if (m_vertex_pool[i].m_n_order != -1) // not a fixed vertex.
            {
                m_vertex_pool[i].m_n_order = n_system_dimension;
                n_system_dimension += m_n_dimension;
            }
        }


        /** reisize right-hand-vector to the correct dimension */
        m_v_RightHandSide.resize(n_system_dimension);

        /** calculate system-block contributions, and m_v_RightHandSide */
        this->Deduce_SystemMatrixBlocks();

        this->m_deduction_plan.Alloc_SystemMatrixBlocks(this->m_m_SystemMatrix);


        /** assert final system matrix to be square, and dimension equals to cycle dimension */
        if (m_m_SystemMatrix.n_Row_Num() != m_m_SystemMatrix.n_Column_Num() ||
                 m_m_SystemMatrix.n_Row_Num() != n_system_dimension )
        {
            printf("fatal error: incorrect system matrix dimension \n");
            printf("SystemMatrix: (%zd, %zd) \n", m_m_SystemMatrix.n_Row_Num(), m_m_SystemMatrix.n_Column_Num());
            printf("n_system_dimension: %d\n", n_system_dimension);
            exit(1);
        }


        /**@brief Calculate Blocks for System Matrix Accorindg to updated Jacobians */
        m_deduction_plan.Calculate_SystemMatrixBlocks();  ///< running parallel inside



        bool b_cholesky_result = false;

        /** calculate variable ordering */
        // solver.SymbolicDecomposition_Blocky(m_m_SystemMatrix/*, true*/);
        /**< not necessary, automatically called in Solve_PosDef_Blocky() */

        /**@brief Calculate: h = [ (A*J*L) * (A*J*L)^{T} ]^{-1} * [-A*J*eta + b] */
        /**@brief enable reuse previous calculated symbolic factorization */

        // solver.Solve_PosDef(this->m_m_SystemMatrix, this->m_v_RightHandSide);
        // this function won't reuse variable ordering

        /** result in m_v_RightHandSide */
        b_cholesky_result = m_solver.Solve_PosDef_Blocky(this->m_m_SystemMatrix, this->m_v_RightHandSide);

        if(!b_cholesky_result)
        {
            printf("\nLeast Squares Failed @ Cholesky.\n");
            return false;
        }


//        std::cout << "solution: " << m_v_RightHandSide.transpose() << "\n";

        for (int i = 0, n = (int) m_vertex_pool.n_Size(); i < n; ++i)
        {
            int n_order = m_vertex_pool[i].m_n_order;
            if (n_order != -1)
                m_vertex_pool[i].x = m_v_RightHandSide.segment<m_n_dimension>(n_order);
        }

//        printf("\nLeast Squares Succeed.\n");

        return true;
    }



};



#endif //PGO_PROJECT_LINEARLEASTSQUARES_H
