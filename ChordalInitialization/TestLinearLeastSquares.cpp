//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#include <iostream>


#include "LinearLeastSquares.h"

#include "ChordalInitialization.h"


// the matlab script for this example
/*

A = [  2  1   2  1   0   0;
1  2   1  2   0   0;
1  6   2  5   0   0;
6  1   1  2   0   0;
1  2   2  1   0   0;
3  4   1  2   0   0;
10 50 0  0  10 -4;
3 4     0  0  3   8;
-1  1   0  0  2    7;
9   3   0  0   11  0;
];

b = [-5; 5; -1; 5; -2; 5; 7; 10; 9; 1];

% fix node i;
i = 2;
v = [1; 1];

b = b - A(:, [3, 4]) * v;

A = A (:, [ 1, 2, 5, 6]);

x = (A'*A)\( A'*b)

*/

int main (int argc, char** argv)
{

    typedef LinearLeastSquares<2> TyLinearLeastSquares;

    TyLinearLeastSquares ls;

    typedef TyLinearLeastSquares::_TyVector _TyVector;


    Eigen::Matrix2d A1;
    Eigen::Matrix2d A2;
    Eigen::Vector2d b;


    A1 << 2, 1, 1, 2;
    A2 << 2, 1, 1, 2;
    b << -5, 5;

    ls.addEdge(0, 1, A1, A2, b);



    A1 << 1, 6, 6, 1;
    A2 << 2, 5, 1, 2;
    b << -1, 5;

    ls.addEdge(0, 1, A1, A2, b);



    A1 << 1, 2, 3, 4;
    A2 << 2, 1, 1, 2;
    b << -2, 5;

    ls.addEdge(0, 1, A1, A2, b);


    A1 << 10, 50, 3, 4;
    A2 << 10, -4, 3, 8;
    b << 7, 10;

    ls.addEdge(0, 3, A1, A2, b);


    A1 << -1, 1, 9, 3;
    A2 << 2, 7, 11, 0;
    b << 9, 1;

    ls.addEdge(0, 3, A1, A2, b);




    _TyVector v; v << 1, 1;
    ls.fixVertex(1, v);

    ls.solve();


    for (int i = 0; i < ls.vertexNum(); ++i)
    {
        std::cout << "vertex [" << i << "]: " << ls.getVertex(i).transpose() << "\n";
    }


    return 0;
}
