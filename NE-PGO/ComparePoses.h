//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef PGO_PROJECT_COMPAREPOSES_H
#define PGO_PROJECT_COMPAREPOSES_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iterator>
#include "LieGroup.h"

namespace msc
{
    template<class Container>
    void SplitString(const std::string &str, Container &container) {
        std::istringstream iss(str);
        std::copy(std::istream_iterator<std::string>(iss),
                  std::istream_iterator<std::string>(),
                  std::back_inserter(container));
    }


    template <bool b_2D_flag>
    class CCompPose
    {

    public:

        enum
        {
            n_v_dimension = (b_2D_flag) ? 3 : 6,
            n_m_dimension = (b_2D_flag) ? 3 : 4
        };

        typedef typename LieGroup<b_2D_flag>::_TySE _TySE;

        typedef Eigen::Matrix<double, n_m_dimension, n_m_dimension> _TyMatrix;

        typedef Eigen::Matrix<double, n_v_dimension, 1> _TyVector;

    public:

        CCompPose () {};

        inline double operator () (const _TyMatrix & pose1, const _TyMatrix &  pose2)
        {
            _TyVector v_error = _TySE::Log ( _TySE::Inv (pose1) * pose2 );
            return v_error.squaredNorm();
        }


        inline double operator () (const _TyVector & pose1, const _TyVector &  pose2)
        {
            _TyVector v_error = _TySE::Log ( _TySE::Inv ( _TySE::V2M(pose1) ) * _TySE::V2M(pose2) );
            return v_error.squaredNorm();
        }

    };


    template <bool b_2D_flag>
    class CGetLineData
    {
    public:

        enum { n_v_dimension = (b_2D_flag) ? 3 : 6 };

        typedef Eigen::Matrix<double, n_v_dimension, 1> _TyVector;

    public:

        CGetLineData () {};

        inline _TyVector operator () (const std::vector<std::string> & tokens)
        {
            _TyVector v;

            assert ( (tokens.size() - 2 == n_v_dimension) && "dimension doesn't match @ CGetLineData");

            if (b_2D_flag)
            {
                v << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str());
            }
            else
            {
                v << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()),
                        std::atof(tokens[5].c_str()), std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str());
            }
            return v;
        }
    };



    template <bool b_2D_flag>
    double CompFile (const char * p_s_filename_1,  const char * p_s_filename_2)
    {

        enum { n_v_dimension = (b_2D_flag) ? 3 : 6 };

        typedef Eigen::Matrix<double, n_v_dimension, 1> _TyVector;

        CCompPose<b_2D_flag> CompPoseFunc;

        CGetLineData<b_2D_flag> GetLineDataFunc;


        std::ifstream ifs1 (p_s_filename_1);
        std::ifstream ifs2 (p_s_filename_2);

        double tmp_error = 0;

        if( ifs1.peek() == std::ifstream::traits_type::eof() || ifs2.peek() == std::ifstream::traits_type::eof() )
        {
            std::cerr << "Empty measurement data (edge) file!" << std::endl;
            return 0;
        }

        if (ifs1.is_open() && ifs2.is_open())
        {
            std::string line1, line2;
            while (std::getline(ifs1, line1) && std::getline(ifs2, line2) )
            {
                std::vector<std::string> tokens1, tokens2;
                msc::SplitString(line1, tokens1);
                msc::SplitString(line2, tokens2);

                if( !tokens1.empty() && !tokens2.empty() )
                {
                    assert( ( (tokens1[0].compare(0, 6, "Vertex") == 0
                               && tokens2[0].compare(0, 6, "Vertex") == 0) ||
                              (tokens1[0].compare(0, 6, "VERTEX") == 0
                               && tokens2[0].compare(0, 6, "VERTEX") == 0) )
                            && "Not Pure Vertex File! Abort @ComparePose.h");

                    assert( (tokens1[0].compare(tokens2[0]) == 0
                         && tokens1[1].compare(tokens2[1]) == 0
                         && tokens1.size() == tokens2.size() )
                            && "File Error! Abort @ComparePose.h");

                    _TyVector v1 = GetLineDataFunc (tokens1);
                    _TyVector v2 = GetLineDataFunc (tokens2);

                    tmp_error += CompPoseFunc (v1, v2);
                }

            }


        }
        else{
            std::cerr << "Read edge data failed! Aborting.." << std::endl;
            return 0;
        }

        ifs1.close();
        ifs2.close();

        return std::sqrt(tmp_error);
    }



}

#endif //PGO_PROJECT_COMPAREPOSES_H
