//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#include "NEPGO_NonlinearSolver.h"

namespace msc_func
{
    int CheckDataDimension(const char *p_s_file_name) {
        int n_dimension = 0;
        std::ifstream ifs(p_s_file_name);
        std::string line;
        if (ifs.is_open()) {
            while (std::getline(ifs, line)) {
                std::vector<std::string> tokens;
                msc_func::SplitString(line, tokens);
                if (!tokens.empty()) {
                    size_t n_elements = tokens.size();
                    if (tokens[0].compare(0, 5, "Edge2") == 0 || tokens[0].compare(0, 5, "EDGE2") == 0 ||
                        tokens[0].compare(0, 8, "Edge_SE2") == 0 || tokens[0].compare(0, 8, "EDGE_SE2") == 0) {
                        n_dimension = 3;
                        _ASSERTE(n_elements == 3 + n_dimension + n_dimension * (n_dimension + 1) / 2);
                        ifs.close();
                        return n_dimension;
                    } else if (tokens[0].compare(0, 5, "Edge3") == 0 || tokens[0].compare(0, 5, "EDGE3") == 0 ||
                               tokens[0].compare(0, 8, "Edge_SE3") == 0 || tokens[0].compare(0, 8, "EDGE_SE3") == 0) {
                        n_dimension = 6;
                        _ASSERTE(n_elements == 3 + n_dimension + n_dimension * (n_dimension + 1) / 2);
                        ifs.close();
                        return n_dimension;
                    } else if(tokens[0].compare("Edge") == 0 ||
                              tokens[0].compare("EDGE") == 0 ||
                              tokens[0].compare("ODOMETRY") == 0)
                    {
                        if(n_elements == 12)
                        {
                            n_dimension = 3;
                            ifs.close();
                            return n_dimension;
                        }
                        else if(n_elements == 30)
                        {
                            n_dimension = 6;
                            ifs.close();
                            return n_dimension;
                        }
                    }
                } else{
                    std::cerr << "tokens are empty" << std::endl;
                }
            }
        }
        else {
            std::cerr << "Open data (edge file) failed.!" << std::endl;
            return n_dimension;
        }
        ifs.close();
        return n_dimension;
    }
}

void print_usage(char * program)
{
    std::cout << "Usage: " << program
              << "\n\t [-i input_edge_data]"
              << "\n\t [<-v output_vertex_estimate>]"
              << "\n\t [<-s output_execution summary>]"
              << "\n\t [<-c use_chordal_initialization>]"
              << "\n\t [<-n max_iterations (default 20)>]"
              << "\n\t [<-t threshold_of_precision_to_stop_algorithm (default 0.001)>]"
              << "\n\t [<-d dump_vertex_dir>]"
              << "\n\t [<-g ground_true_file or initial_guess_file>]"
              << "\n\t [<-m plot_matrix_given_scalar>]"
              << "\n\t [<-p <--plot_trajectory-->>]"
              << "\n\t [<-h <--help-->>]"
              << std::endl;
}

struct RunningInfo
{
    /**@brief template parameter of ECPGO_NonlinearSolver */
    bool b_2D_SLAM;

    /**@brief option parameters for input and output files */
    char * p_s_edge_file;
    char * p_s_output_edge_file;
    char * p_s_output_vertex_file;
    char * p_s_output_summary_file;

    char * p_s_dump_vertex_dir;

    char * p_s_ground_truth_file;

    bool b_use_chordal_init;

    int n_max_iterations;
    double f_error_threshold;

    bool b_plot_trajectory;
    bool b_plot_matrix;

    bool b_dump_vertex_per_iter;


    int n_scalar;

    RunningInfo():
            b_2D_SLAM(true),
            p_s_edge_file(NULL),
            p_s_output_edge_file(NULL),
            p_s_output_vertex_file(NULL),
            p_s_output_summary_file(NULL),
            p_s_dump_vertex_dir(NULL),
            p_s_ground_truth_file(NULL),
            b_use_chordal_init(false),
            n_max_iterations(20),
            f_error_threshold(0.001),
            b_plot_trajectory(false),
            b_plot_matrix(false),
            b_dump_vertex_per_iter(false),
            n_scalar(5)
    {}
};


template <class NonlinearSolverType>
int Run(NonlinearSolverType & NEPGO, RunningInfo & info)
{
    int n_iterations = 0; /// used iterations

    CTimer timer;
    CTimerSampler tic(timer);

    double f_time_unused = .0;
    double f_time_load_data = .0;
    double f_time_optimization =.0;
    double f_time_chordal_init = .0;
    double f_time_dump_data =.0;

    std::string prefix("./default.txt");
    if(info.p_s_output_summary_file != NULL)
        prefix.assign(info.p_s_output_summary_file);
    prefix = prefix.substr(0, prefix.find_last_of("/\\")+1);

    std::cout << "Initialized as "
              << ( info.b_2D_SLAM ? "2D" : "3D")
              << " NE-PGO SLAM solver" << std::endl << std::endl;

    tic.Accum_DiffSample(f_time_unused);

    std::cout << "Loading data ..." << std::endl;

    if(!NEPGO.Load_Edges(info.p_s_edge_file))
    {
        std::cerr << "Load measurement data (edges in NE-PGO) failed!" << std::endl;
        return 2;
    }

    tic.Accum_DiffSample(f_time_load_data);


    // set initial value, by ground true or whatever. Otherwise using odometry
    if(info.p_s_ground_truth_file != NULL)
    {
        if(!NEPGO.SetVertexEstimate(info.p_s_ground_truth_file))
        {
            std::cerr << "Read ground-truth (or initial-value) file failed!" << std::endl;
            return 6;
        }
    }


    // use chordal initialization
    if (info.b_use_chordal_init)
    {
        tic.Accum_DiffSample(f_time_unused);
        if(!NEPGO.ChordalInit ()) {
            std::cerr << "Chordal Initialization failed!" << std::endl;
            return 1;
        }
        tic.Accum_DiffSample(f_time_chordal_init);
    }


    if(info.b_plot_trajectory)
    {
        std::string initial = prefix + "initial.tga";
        NEPGO.PlotTrajectory(initial.c_str());
    }


    if (info.b_dump_vertex_per_iter)
    {
        NEPGO.SetDumpVertexPerIter(true, info.p_s_dump_vertex_dir);
    }


    tic.Accum_DiffSample(f_time_unused);
    /**@brief optimize the problem */
    n_iterations = NEPGO.Optimize( info.n_max_iterations, info.f_error_threshold );
    tic.Accum_DiffSample(f_time_optimization);


    if (info.b_use_chordal_init)
        std::cout << "time_chord_init " << f_time_chordal_init << "\n\n";


    if(info.b_plot_matrix)
    {
        std::string sys_matr_fig = prefix + "systemMatrix.tga";
        NEPGO.PlotSystemMatrix(sys_matr_fig.c_str(), info.n_scalar);
    }

    std::cout << "Writing data ..." << std::endl;

    tic.Accum_DiffSample(f_time_unused);

    if(info.p_s_output_vertex_file != NULL)
    {
        NEPGO.SaveSolution(info.p_s_output_vertex_file);
    }

    tic.Accum_DiffSample(f_time_dump_data);


    if(info.b_plot_trajectory)
    {
        std::string solution = prefix + "solution.tga";
        NEPGO.PlotTrajectory(solution.c_str());
    }

    if(info.p_s_output_summary_file != NULL)
    {
        std::ofstream ofs(info.p_s_output_summary_file);

        ofs << "input ( edge: " << info.p_s_edge_file
            << " )" << std::endl << std::endl;

        ofs << "data_type ( " << ( info.b_2D_SLAM ? "2D" : "3D")
            << " )" << std::endl << std::endl;

        NEPGO.PrintSummaryInfo(ofs);

        if(info.b_use_chordal_init)
            ofs << "time_chord_init " << f_time_chordal_init << "\n\n";

        ofs << "time_process ( load data: " << f_time_load_data
            << "  optimization: " << f_time_optimization
            << "  dump data: " << f_time_dump_data
            << " )" << std::endl << std::endl;

        ofs << "n_iterations: " << n_iterations << std::endl;

        ofs.close();
    }

    return 0;
}




int main (int argc, char** argv) {

    RunningInfo info;

    if(argc < 2)
    {
        std::cerr << "Not enough input arguments for " << argv[0] << std::endl;
        print_usage(argv[0]);
        return 0;
    }

    /**@brief option parser*/
    opterr = 0;
    int c;
    while ((c = getopt(argc, argv, "i:v:s:g:hn:pm:t:d:c")) != -1)
        switch (c)
        {
            case 'i':
                info.p_s_edge_file = optarg;
                break;
            case 'v':
                info.p_s_output_vertex_file = optarg;
                break;
            case 's':
                info.p_s_output_summary_file = optarg;
                break;
            case 'g':
                info.p_s_ground_truth_file = optarg;
                break;
            case 'n':
                info.n_max_iterations = std::atoi( optarg );
                break;
            case 't':
                info.f_error_threshold = std::atof( optarg );
                break;
            case 'p':
                info.b_plot_trajectory = true;
                break;
            case 'm':
                info.b_plot_matrix = true;
                info.n_scalar = std::atoi( optarg );
                break;
            case 'd':
                info.b_dump_vertex_per_iter = true;
                info.p_s_dump_vertex_dir = optarg;
                break;
            case 'c':
                info.b_use_chordal_init = true;
                break;
            case 'h':
            case '?':
            default:
                print_usage(argv[0]);
                return 0;
        }

    if(info.p_s_edge_file == NULL)
    {
        std::cerr << "Please use <-i input_edge_file > !" << std::endl;
        return 1;
    }

    switch (msc_func::CheckDataDimension(info.p_s_edge_file))
    {
        case 3:
            info.b_2D_SLAM = true;
            break;
        case 6:
            info.b_2D_SLAM = false;
            break;
        case 0:
        default:
            std::cerr <<"Cannot recognize data type!" << std::endl;
            return 1;
    }

    typedef CNEPGO_NonlinearSolver<true> NEPGO_Solver_2D;

    typedef CNEPGO_NonlinearSolver<false> NEPGO_Solver_3D;


    int result;

    if(info.b_2D_SLAM)
    {
        NEPGO_Solver_2D NEPGO;
        result = Run(NEPGO, info);
    } else
    {
        NEPGO_Solver_3D NEPGO;
        result = Run(NEPGO, info);
    }

    return result;
}
