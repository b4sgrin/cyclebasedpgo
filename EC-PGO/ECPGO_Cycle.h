//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_ECPGO_CYCLE_H
#define SLAM_PLUS_PLUS_ECPGO_CYCLE_H


template < class EdgeType >
class CECPGO_Cycle {

public:

    typedef EdgeType _TyEdge;
    typedef _TyEdge* _PtEdgeType; /**<@brief pointer to edge type */

    enum { n_dimension = _TyEdge::n_dimension }; /**<@brief lie algebra dimension */

    typedef typename _TyEdge::_TyLieGroupMatrix _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef typename _TyEdge::_TyJacobianMatrix _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef typename _TyEdge::_TyLieAlgebraVector _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename _TyEdge::_TySE _TySE; /**<@brief lie group implementations */


    typedef CECPGO_SparseVector<_TyJacobianMatrix> _TySparseVector;
    typedef typename _TySparseVector::EntryType _TyEntry;

private:

    size_t m_n_edges_num; /**<@brief number of edges on cycle, also called cycle length */

    std::vector<size_t> m_v_n_edge_id; /**<@brief reference edge ids */

    std::vector<_PtEdgeType> m_v_ptr_edges; /**<@brief pointers to edges, faster than using IDs in pool*/

    std::vector<bool> m_v_edge_orientation; /**<@brief orientation of edges on cycle: true=forward, false=backward */
    
private:

    std::vector<_TyJacobianMatrix> m_v_m_Jacobians; /**<@brief Jacobians for linearized cycle */

    _TyLieAlgebraVector m_v_cycle_error; /**<@brief error of the cycle. used for linearization, checking equality of cycle */

private:

    /**@brief Set this to the index of first elements in linear system
     * in accordance with SLAM++'s default implementation.*/
    size_t m_n_order; /**<@brief order of cycle in linear system. Ax=b. Reordering is done automatically by Linear Solver. Not need to do it here. */

    /**@brief sparse vector is nothing but binding <edgeID, Jacobian> together for easy jacobian operations */
    _TySparseVector m_spvec_jacobian_edge_index; /**< sparse vector of Jacobians for system matrix calculation */

    /**@brief assign an unique id for each cycle, UNUSED */
    size_t m_n_cycle_id;

public:

    /** @brief default constructor, disabled, m_n_edges_num must be given, for speed up storage */
    CECPGO_Cycle(){}


    /** @brief constructor given length of cycle, edge IDs, pointers to referenced edges, orientations of edges */
    /** @param num_edges, edge_IDs, edge_Ptrs, edge_orientations */
    /** @arg size_t, std::vector<size_t>, std::vector<_PtEdgeType>, std::vector<bool> */
    CECPGO_Cycle ( /*size_t n_edges_num, */ const std::vector<size_t> & v_n_edge_id,
                 const std::vector<_PtEdgeType> & v_ptr_edges,
                 const std::vector<bool> & v_edge_orientation)
    :
            m_n_edges_num(v_n_edge_id.size()), m_v_n_edge_id(v_n_edge_id),
            m_v_ptr_edges(v_ptr_edges), m_v_edge_orientation(v_edge_orientation),
            m_v_m_Jacobians(m_n_edges_num),
            m_spvec_jacobian_edge_index(m_n_edges_num)  // sort indices of jacobian blocks by edges
    {}

    /** @brief constructor given the length of cycle */
    /** @param [in] n_edges_num is the length of cycle */
    explicit CECPGO_Cycle ( size_t n_edges_num)
            :
            m_n_edges_num(n_edges_num), m_v_n_edge_id(n_edges_num),
            m_v_ptr_edges(n_edges_num), m_v_edge_orientation(n_edges_num),
            m_v_m_Jacobians(n_edges_num), // initialize std::vector holding Jacobians
            m_spvec_jacobian_edge_index(m_n_edges_num)
    {}

    /**@brief binding together pointers to jacobian and edgeID, by a --SEPARATE-- function. */
    /**@note call this function --AFTER-- copy constructors in push_back() */
    void Alloc_Jacobian()
    {
        for (size_t i = 0; i < m_n_edges_num; ++i)
        {
            m_spvec_jacobian_edge_index[i].first = m_v_n_edge_id[i];
            m_spvec_jacobian_edge_index[i].second = m_v_m_Jacobians.at(i).data(); ///< at for check
        }
        m_spvec_jacobian_edge_index.sort();
    }
    
    inline const std::vector<size_t> & r_Get_Edge_Ids () const{
        return this->m_v_n_edge_id;
    }

    inline const std::vector<_PtEdgeType> & r_Get_Ptr_Edges () const {
        return this->m_v_ptr_edges;
    };


    inline const _TySparseVector & r_Get_Spvec_Jacobian_Edge_Index () const {
        return this->m_spvec_jacobian_edge_index;
    }

    /** @brief get the error of this cycle according to current estimate.
     *  used as Stop Criteria */
    inline double f_Get_CycleErrorSquaredNorm() const {
        return this->m_v_cycle_error.squaredNorm();
    }

    /** @brief return number of edges on cycle, i.e., length of the cycle */
    inline size_t n_Num_Edges () const {
        return this->m_n_edges_num;
    }

    /** @brief return pointer to the cycle object, this pointer */
    /** @return this */
    inline CECPGO_Cycle * Ptr () const {
        return this;
    }

    /** @brief set the order of cycle in final Jacobian construction */
    inline void Set_Order (size_t n_order) {
        m_n_order = n_order;
    }

    /** @brief get the order of cycle in final Jacobian construction */
    inline size_t n_Order () const {
        return m_n_order;
    }



    /**@brief calcualte jacobians for each cycle. */
    /**@attention Note the weighted jacobian (AJL) is stored as (AJL)^{T} to later calculation. */
    inline void Calculate_CycleJacobian_And_Rhs(Eigen::VectorXd & r_v_rhs)
    {
        _TyLieGroupMatrix matrix_cache = _TyLieGroupMatrix::Identity();
        _TyLieAlgebraVector vector_cache = _TyLieAlgebraVector::Zero();
        _TyJacobianMatrix matrix_raw_jacobian;
        for (size_t i = 0; i < m_n_edges_num; ++i) {
            /**@brief pointer to current edge */
            _PtEdgeType p_tmp = this->m_v_ptr_edges[i];
            if ( this->m_v_edge_orientation[i] ) // forward direction
            {
                // If in forward direction, at first, multiply current edge
                matrix_cache *= p_tmp->r_Get_EdgeEstimate();  ///< DO IT AT BEGINNING
                // calculate _TySE Adjoint matrix
                matrix_raw_jacobian = _TySE::Ad( matrix_cache );
                //  weighted Cycle Jacobain =  A*J*L
                _TyJacobianMatrix & matr = m_v_m_Jacobians[i];
                matr = matrix_raw_jacobian * p_tmp->r_Get_EdgeJacobian();  ///< sign (+1)
                matr.transposeInPlace();  ///< We store (A*J*L)^T here
                // accumulate vector_cache for -A*J*eta
                vector_cache += matrix_raw_jacobian * p_tmp->r_Get_EdgeRhs();  ///< sign (+1)
            }
            else {  // backward direction
                // calculate _TySE Adjoint matrix
                matrix_raw_jacobian = _TySE::Ad( matrix_cache );
                // Weighted Cycle Jacobain =  A*J*L
                _TyJacobianMatrix & matr = m_v_m_Jacobians[i];
                matr = -matrix_raw_jacobian * p_tmp->r_Get_EdgeJacobian();  ///< sign (-1)
                matr.transposeInPlace();  ///< We store (A*J*L)^T here
                // accumulate vector_cache for -A*J*eta
                vector_cache -= matrix_raw_jacobian * p_tmp->r_Get_EdgeRhs();  ///< sign (-1)
                // If in backward direction, at last, multiply inverse of current edge
                matrix_cache *= _TySE::Inv ( p_tmp->r_Get_EdgeEstimate() );  ///< DO IT AT LAST
            }
        }
        // accumulated cycle error
        this->m_v_cycle_error = _TySE::Log( matrix_cache );
        /**@brief  CycleRhs = -A*J*eta + b.   b = Jl(error)*error   */
        r_v_rhs.segment<n_dimension>(m_n_order) = vector_cache + _TySE::Jl(this->m_v_cycle_error) * this->m_v_cycle_error;
    }


    ~CECPGO_Cycle()
    {
        {
            std::vector<size_t> empty;
            m_v_n_edge_id.swap(empty);
        }
        {
            std::vector<_PtEdgeType> empty;
            m_v_ptr_edges.swap(empty);
        }
        {
            std::vector<bool> empty;
            m_v_edge_orientation.swap(empty);
        }
    }

};



#endif //SLAM_PLUS_PLUS_ECPGO_CYCLE_H
