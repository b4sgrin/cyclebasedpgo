//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_ECPGO_SPARSEVECTOR_H
#define SLAM_PLUS_PLUS_ECPGO_SPARSEVECTOR_H

#include <utility>


template <class ValueType>
class CECPGO_SparseVectorBase
{

public:


    /** used for vector mulplication */
    typedef std::pair<ValueType, ValueType> ProductEntryType;
    typedef std::vector< ProductEntryType > ProductChainType;   //  a vector of un-summed value pairs


    typedef std::pair<size_t, ValueType> EntryType;

    typedef typename std::vector<EntryType>::iterator iterator;
    typedef typename std::vector<EntryType>::const_iterator const_iterator;


protected:

    std::vector<EntryType> m_entry;

public:

    CECPGO_SparseVectorBase () {}

    // copy constructor
    CECPGO_SparseVectorBase (const CECPGO_SparseVectorBase & other )
            :m_entry(other.m_entry)
    {}

    // constructor
    CECPGO_SparseVectorBase (const std::vector<EntryType> & entry)
            :m_entry(entry)
    {}

    explicit CECPGO_SparseVectorBase (size_t n_size)
            :m_entry(n_size)
    {}

    ~CECPGO_SparseVectorBase() {}


    static inline bool compare( const EntryType & term1, const EntryType & term2)
    {
        return (term1.first < term2.first) ;
    }


    inline iterator begin () const
    {
        return m_entry.begin();
    }
    
    inline iterator end () const
    {
        return m_entry.end();
    }

    inline EntryType front() const
    {
        return m_entry.front();
    }

    inline EntryType back() const
    {
        return m_entry.back();
    }

    inline void reserve( const size_t n_size)
    {
        m_entry.reserve(n_size);
    }

    inline void append (const EntryType & entry)
    {
        m_entry.push_back(entry);
    }

    inline void append (size_t index, ValueType value)
    {
        m_entry.push_back( std::make_pair(index, value) );
    }

    inline EntryType & operator [] (size_t n_index)
    {
        return m_entry[n_index];
    }

    inline const EntryType & operator [] (size_t n_index) const
    {
        return m_entry[n_index];
    }

    inline size_t size () const
    {
        return m_entry.size();
    }


    // sort elements in ascending order according to indices.
    void sort()
    {
        std::sort(m_entry.begin(), m_entry.end(), CECPGO_SparseVectorBase::compare);
    }


    inline bool NotIntersect (const CECPGO_SparseVectorBase & other ) const
    {
        return ( m_entry.front().first > other.m_entry.back().first ||
                 m_entry.back().first < other.m_entry.front().first );
    }


    bool MultiplyWith (const CECPGO_SparseVectorBase & other, ProductChainType & result) const
    {
        if(this->NotIntersect(other))
            return false;

        result.resize(0);

        const_iterator iter_this = m_entry.begin();
        const_iterator iter_other = other.m_entry.begin();

        const_iterator end_this = m_entry.end();
        const_iterator end_other = other.m_entry.end();

        while (iter_this != end_this && iter_other != end_other)
        {
            size_t index_this = iter_this->first;
            size_t index_other = iter_other->first;
            if (index_this > index_other)
            {
                ++iter_other;
            }
            else if (index_this < index_other)
            {
                ++iter_this;
            }
            else if(index_this == index_other)
            {
                result.push_back( std::make_pair(iter_this->second, iter_other->second) );
                ++iter_this;
                ++iter_other;
            }
        }
        return (result.size() > 0);
    }


    bool IsIntersectWith (const CECPGO_SparseVectorBase & other) const
    {
        if(this->NotIntersect(other))
            return false;

        const_iterator iter_this = m_entry.begin();
        const_iterator iter_other = other.m_entry.begin();

        while (iter_this != m_entry.end() && iter_other != other.m_entry.end())
        {
            size_t index_this = iter_this->first;
            size_t index_other = iter_other->first;
            if (index_this > index_other)
            {
                ++iter_other;
            }
            else if (index_this < index_other)
            {
                ++iter_this;
            }
            else if(index_this == index_other)
            {
                return true;
            }
        }

        return false;
    }


    void print(std::ostream & os) const
    {
        for (size_t i = 0; i < m_entry.size(); ++i )
        {
            os << m_entry[i].first << " ";
        }
        os << std::endl;
    }

};





template <class MatrixType>
class CECPGO_SparseVector
        :public CECPGO_SparseVectorBase <double*>
{
public:

    typedef Eigen::Map<MatrixType, CUberBlockMatrix::map_Alignment> MapMatrixType;

    typedef CECPGO_SparseVectorBase<double*>::EntryType EntryType;


public:

    CECPGO_SparseVector()
            :CECPGO_SparseVectorBase <double*> ()
    {}

    // copy constructor
    CECPGO_SparseVector (const CECPGO_SparseVector & other )
            :CECPGO_SparseVectorBase <double*> (other)
    {}

    // constructor
    CECPGO_SparseVector (const EntryType & entry)
            :CECPGO_SparseVectorBase <double*> (entry)
    {}

    explicit CECPGO_SparseVector (size_t n_size)
            :CECPGO_SparseVectorBase <double*> (n_size)
    {}



public:

    MatrixType operator * (const CECPGO_SparseVector & other)
    {
        MatrixType matr;

        matr.setZero();

        const_iterator iter_this = m_entry.begin();
        const_iterator iter_other = other.m_entry.begin();

        const_iterator end_this = m_entry.end();
        const_iterator end_other = other.m_entry.end();

        while (iter_this != end_this && iter_other != end_other)
        {
            size_t index_this = iter_this->first;
            size_t index_other = iter_other->first;
            if (index_this > index_other)
            {
                ++iter_other;
            }
            else if (index_this < index_other)
            {
                ++iter_this;
            }
            else if(index_this == index_other)
            {
                MapMatrixType tmp1(iter_this->second);
                MapMatrixType tmp2(iter_other->second);
                matr += tmp1.transpose() * tmp2;
                ++iter_this;
                ++iter_other;
            }
        }
        return matr;
    }

};





template <class ValueType, class ResultType>
struct SymbolicVectorProductBase
{

public:

    typedef std::pair<ValueType, ValueType> ProductEntryType;

    typedef std::vector< ProductEntryType > ProductChainType;

public:

    ProductChainType m_product;  // symbolic product chain, a pair pointers to vector storage

    ResultType m_result;   // ptr to result storage;

    virtual void CalculateResults() = 0; // function to compute value paris in m_product into m_result.
};




template <class MatrixType>
struct SymbolicVectorProduct : public SymbolicVectorProductBase<double*, double*>
{
public:

    typedef Eigen::Map<MatrixType, CUberBlockMatrix::map_Alignment> MapMatrixType;

public:

    // this function is meant ot be running in parallel
    void CalculateResults()
    {
        MapMatrixType matr(m_result);
        matr.setZero();
        for (size_t i = 0; i < m_product.size(); ++i)
        {
            MapMatrixType tmp1(m_product[i].first);
            MapMatrixType tmp2(m_product[i].second);
            matr += tmp1.transpose() * tmp2;
        }
    }

};





#endif //SLAM_PLUS_PLUS_ECPGO_SPARSEVECTOR_H
