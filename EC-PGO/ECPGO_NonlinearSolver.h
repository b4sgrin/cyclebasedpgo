//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef SLAM_PLUS_PLUS_ECPGO_NONLINEARSOLVER_H
#define SLAM_PLUS_PLUS_ECPGO_NONLINEARSOLVER_H

/** use column major system matrix allocation, much faster than row allocation */
/** column major allocation use p_GetBlock_Log() in stead of p_FindBlock() */
#define __SLAM_PLUS_PLUS_ECPGO_COLUMN_MAJOR_ALLOCATE_SYSTEM_MATRIX


#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>


//#include "slam/OrderingMagic.h"
//#include "slam/LinearSolverTags.h"  //
#include "slam/LinearSolver_UberBlock.h"
#include "slam/FlatSystem.h"
//#include "slam/BlockMatrix.h"
//#include "slam/Segregated.h"

#include "slam/Timer.h"


#include "LieGroup.h"

#include "ECPGO_SparseVector.h"

#include "ECPGO_DeductionPlan.h"

#include "ECPGO_Vertex.h"
#include "ECPGO_Edge.h"
#include "ECPGO_Cycle.h"
#include "ECPGO_EdgeCycleVertexSystem.h"

#include "MinimumCycleBasis.h"
#include "FundamentalCycleBasis.h"

#include "ChordalInitialization.h" // chordal initialization

/**
 * @brief plotting utilities copied from SLAM++ library
 */
#include "PlottingUtilities.h"

#include "ComparePoses.h"



namespace msc_func {

    /**
     * @brief Template function to split string into words
     * @param [in] str is the std::string to be splited, i.e., a sentence.
     * @param [out] container is the container to collect the splitted words.
     * @note default delimiter is space
     * @brief reference: http://www.martinbroadhurst.com/how-to-split-a-string-in-c.html
     */
    template<class Container>
    void SplitString(const std::string &str, Container &container) {
        std::istringstream iss(str);
        std::copy(std::istream_iterator<std::string>(iss),
                  std::istream_iterator<std::string>(),
                  std::back_inserter(container));
    }

}


/**
 * @class EC-PGO nonlinear solver
 * @tparam ty2D is the solver type, true for 2D, false for 3D
 * @brief use Load_Edge, Load_Cycle to load data, then execute optimize().
 */
template <const bool ty2D = true >
class CECPGO_NonlinearSolver {

public:

#ifndef USE_FUNDAMENTAL_CYCLE_BASIS
    typedef MinimumCycleBasis CTypeCycleBasis; /// default to use mcb
#else
    typedef FundamentalCycleBasis CTypeCycleBasis;
#endif

public:

    typedef CTypeTraits<ty2D> _TyTraits;
    typedef CECPGO_Vertex<_TyTraits> _TyVertex;
    typedef CECPGO_Edge<_TyVertex> _TyEdge;
    typedef CECPGO_Cycle<_TyEdge> _TyCycle;

    typedef typename _TyEdge::_PtVertexType _PtVertexType; /**<@brief pointer to vertex type */
    typedef typename _TyCycle::_PtEdgeType _PtEdgeType; /**<@brief pointer to edge type */

    enum { n_dimension = _TyEdge::n_dimension }; /**<@brief lie algebra dimension */

public:

    typedef CECPGO_EdgeCycleVertexSystem<_TyEdge, _TyCycle, _TyVertex> CSystemType; /**<@brief optimization system type */

    typedef typename CSystemType::_TyLieGroupMatrix _TyLieGroupMatrix; /**< @brief Lie Group matrix type */
    typedef typename CSystemType::_TyJacobianMatrix _TyJacobianMatrix; /**< @brief Jacobian matrix type */
    typedef typename CSystemType::_TyLieAlgebraVector _TyLieAlgebraVector; /**< @brief Lie algebra vector space type */

    typedef typename CSystemType::_TyJacobianMatrixList _TyJacobianMatrixList;

    typedef typename CSystemType::_TyEdgePool _TyEdgePool; /**<@brief edge pool type */
    typedef typename CSystemType::_TyCyclePool _TyCyclePool; /**<@brief cycle pool type */
    typedef typename CSystemType::_TyVertexPool _TyVertexPool; /**<@brief vertex pool type */

    typedef CLinearSolver_UberBlock<_TyJacobianMatrixList> CLinearSolverType; /**<@brief Linear Solver Type */

    typedef typename CSystemType::_TySE _TySE; /**<@brief lie group implementations */


    enum { n_rot_matrix_dimension = (ty2D)? 2 : 3};


private:

    CSystemType m_system; /**< @brief optimization system containg: Edge, Cycle, Vertex */

    CLinearSolverType m_solver; /**<@brief linear solver type */

    int m_n_max_iteration; /**<@brief maximum iterations */

    double m_f_error_threshold; /**< @brief stop threshold */

    std::vector<_TyEdge *> m_v_edge_id_vs_ptr; /**< @brief map edge id to ptr */

    CTypeCycleBasis m_g_cb;  /**<@brief  cycle basis */


private:

    bool m_b_dump_vertex_per_iter;

    int m_n_used_iteration;

    double m_f_time_cycle_basis;

    double m_f_time_symbolic_factorization;

    double m_f_time_allocate_system_matrix;

    double m_f_time_calculate_system_matrix;

    double m_f_time_solve_cycle_system;

    double m_f_time_update_edge_estimate;

    CTimer m_timer;

    CTimerSampler m_tic;


    std::vector<double> m_v_f_objective_function;

    std::vector<double> m_v_f_cycle_error;

    std::vector<double> m_v_f_perturbation;

    std::string m_str_output_dir;

    std::vector<double> m_v_f_objFunc_vertex_based;


public:

    explicit CECPGO_NonlinearSolver()
            :m_n_max_iteration(20),
             m_f_error_threshold(.001),
             m_v_edge_id_vs_ptr(0),
             m_b_dump_vertex_per_iter(false),
             m_n_used_iteration(0),
             m_f_time_cycle_basis(.0),
             m_f_time_symbolic_factorization(.0),
             m_f_time_allocate_system_matrix(.0),
             m_f_time_calculate_system_matrix(.0),
             m_f_time_solve_cycle_system(.0),
             m_f_time_update_edge_estimate(.0),
             m_tic(m_timer)
    {};


    CECPGO_NonlinearSolver(size_t n_max_iteration, double f_cycle_error_threshold)
            :m_n_max_iteration(n_max_iteration),
             m_f_error_threshold(f_cycle_error_threshold),
             m_v_edge_id_vs_ptr(0),
             m_b_dump_vertex_per_iter(false),
             m_n_used_iteration(0),
             m_f_time_cycle_basis(.0),
             m_f_time_symbolic_factorization(.0),
             m_f_time_allocate_system_matrix(.0),
             m_f_time_calculate_system_matrix(.0),
             m_f_time_solve_cycle_system(.0),
             m_f_time_update_edge_estimate(.0),
             m_tic(m_timer)
    {};


    ~CECPGO_NonlinearSolver(){};


    int ComputeCycleBasis ()
    {

        m_g_cb.run(); /// run cycle basis algorithm

        const std::vector< std::vector< std::pair<int, bool> > > &  v_cb = m_g_cb.getCycleBasis();
        /// add cycle basis to the sytem.

        for (auto cit = v_cb.begin(); cit != v_cb.end(); ++cit)
        {
            std::vector<size_t>  v_n_edge_id;

            std::vector<bool> v_edge_orientation;

            std::vector<_PtEdgeType>  v_ptr_edges;

            for (auto it = (*cit).begin(); it != (*cit).end(); ++it)
            {
                v_n_edge_id.emplace_back((*it).first);

                v_edge_orientation.emplace_back((*it).second);

                v_ptr_edges.emplace_back(m_v_edge_id_vs_ptr[(*it).first]);
            }

            _TyCycle cycle(v_n_edge_id, v_ptr_edges, v_edge_orientation);

            m_system.r_Add_Cycle(cycle);

        }

//        double f_time_add_cycles = T.toc();

//        std::cout << m_g_mcb;
//
//        printf("\n\nTime used by MCB: %f", f_time_mcb);
//        printf("\n\nTime used by adding cycles: %f\n\n", f_time_add_cycles);

        return 0;
    }



    void SetDumpVertexPerIter (bool b_flag_set = false, std::string t_str_dir = std::string())
    {
        m_b_dump_vertex_per_iter = b_flag_set;
        m_str_output_dir = t_str_dir;
        if ( t_str_dir.size() > 0 && *(m_str_output_dir.end()-1) != '/' )
        {
            m_str_output_dir.push_back('/');
        }
    }





    int Optimize(size_t n_max_iteration, double f_error_threshold)
    {

        m_n_used_iteration = 0;

        double f_time_unused;


        m_tic.Accum_DiffSample(f_time_unused);

        this->ComputeCycleBasis();

        m_tic.Accum_DiffSample(m_f_time_cycle_basis);



        m_tic.Accum_DiffSample(f_time_unused);

        m_system.Alloc_SystemMatrixBlocks();

        m_tic.Accum_DiffSample(m_f_time_allocate_system_matrix);


        while (m_n_used_iteration <= n_max_iteration)
        {
            printf("\n=== nonlinear optimization: iter #%d ===\n\n", m_n_used_iteration);

            m_tic.Accum_DiffSample(f_time_unused);

            m_system.Calculate_SystemMatrix(); ///< linearization

            m_tic.Accum_DiffSample(m_f_time_calculate_system_matrix);

            double cycle_error = m_system.f_Get_CycleErrorNorm();

            double edge_perturb = m_system.f_Get_EdgePerturbationNorm();

            double objFunc = m_system.f_ObjFunc();


            if (m_b_dump_vertex_per_iter)
            {
                std::string str_file_name = m_str_output_dir +
                        "v" + std::to_string(m_n_used_iteration) + ".txt";
                this->Dump_Vertices (str_file_name.c_str());

                std::string str_fig_name = m_str_output_dir +
                        "v" + std::to_string(m_n_used_iteration) + ".tga";
                this->Plot(str_fig_name.c_str());

                double objFuncVertexBased = m_system.f_Calculate_VertexBased_ObjFunc();
                m_v_f_objFunc_vertex_based.push_back(objFuncVertexBased);

                printf("objective function vertex based: %.6f\n", objFuncVertexBased);
            }


            m_v_f_objective_function.push_back(objFunc);

            m_v_f_cycle_error.push_back(cycle_error);

            m_v_f_perturbation.push_back(edge_perturb);



            printf("edge perturbation norm: %.6f\n", edge_perturb);

            printf("cycle error norm: %.6f\n", cycle_error);

            printf("objective function: %.6f\n", objFunc);


            /** stop criterion; check cycle error and perturbation error */
            if(cycle_error < f_error_threshold && edge_perturb < f_error_threshold && m_n_used_iteration)
                break;


            m_tic.Accum_DiffSample(f_time_unused);

            if(!m_system.Solve_Cycle_System(m_solver) )  ///< solve
            {
                std::cerr << "Cholesky factorization failed! Aborting..." << std::endl;
                return -1;
            }

            m_tic.Accum_DiffSample(m_f_time_solve_cycle_system);

            m_system.Update_EdgeEstimate();   ///< update edge estimate

            m_tic.Accum_DiffSample(m_f_time_update_edge_estimate);

            ++m_n_used_iteration;
        }


        if (m_n_used_iteration > n_max_iteration)  // or --m_n_used_iteration in this case.
        {
            std::cerr << "\n@Note: The algorithm does not converge in given max_iterations!\n";
            m_n_used_iteration = n_max_iteration;
        }

        // time for variable ordering.
        // variable ordering is calculated only once in the algorithm.
        m_f_time_symbolic_factorization = m_system.m_f_time_symbolic_factorization;
        m_f_time_solve_cycle_system = m_f_time_solve_cycle_system - m_f_time_symbolic_factorization;


        this->PrintSummaryInfo(std::cout);

        return  m_n_used_iteration;
    };

    int Optimize(size_t n_max_iteration)
    {
        return this->Optimize(n_max_iteration, m_f_error_threshold);
    }

    int Optimize(double f_error_threshold)
    {
        return this->Optimize(m_n_max_iteration, f_error_threshold);
    }

    int Optimize()
    {
        return this->Optimize(m_n_max_iteration, m_f_error_threshold);
    }


    bool ChordalInit () {
        ChordalInitialization<n_rot_matrix_dimension> chordInit;
        // add edge from graph system
        _TyEdgePool &r_edge_pool = m_system.r_Edge_Pool();
        for (size_t i = 0, n = r_edge_pool.n_Size(); i < n; ++i) {
            _TyEdge &edge = r_edge_pool[i];
            const _TyLieGroupMatrix &val_edge = edge.r_Get_Measurement();
            chordInit.addEdge(edge.n_Vertex_Id(0), edge.n_Vertex_Id(1),
                              val_edge.template topLeftCorner<n_rot_matrix_dimension, n_rot_matrix_dimension>());
        }
        // solve chordal optimization
        if (!chordInit.solve()) {
            printf("\nfatal error: chordalInitialization failed @solve()\n");
            return false;
        }
        // set vertex orientation values
        _TyVertexPool &r_vertex_pool = m_system.r_Vertex_Pool();
        for (size_t i = 0, n = r_vertex_pool.n_Size(); i < n; ++i) {
            _TyVertex &vertex = r_vertex_pool[i];
            vertex.r_m_State(). template topLeftCorner<n_rot_matrix_dimension, n_rot_matrix_dimension>() = chordInit.getVertex(
                    i);
        }
        // recompute edge initial estimate: rotation part only
        for (size_t i = 0, n = r_edge_pool.n_Size(); i < n; ++i) {
            _TyEdge &edge = r_edge_pool[i];
            size_t id1 =  edge.n_Vertex_Id(0);
            size_t id2 =  edge.n_Vertex_Id(1);
            const Eigen::Matrix<double, n_rot_matrix_dimension, n_rot_matrix_dimension> & R1 =
                    r_vertex_pool[id1].r_m_State(). template topLeftCorner<n_rot_matrix_dimension, n_rot_matrix_dimension>();
            const Eigen::Matrix<double, n_rot_matrix_dimension, n_rot_matrix_dimension> & R2 =
                    r_vertex_pool[id2].r_m_State(). template topLeftCorner<n_rot_matrix_dimension, n_rot_matrix_dimension>();
            edge.r_Get_EdgeEstimate(). template topLeftCorner<n_rot_matrix_dimension, n_rot_matrix_dimension>() = R1.transpose() * R2;
        }
        //return true;
        return initTranslation(); // initialize the translation part as well.
    }


    /**@brief initialize translational estimate by given rotation */
    bool initTranslation ()
    {
        LinearLeastSquares<n_rot_matrix_dimension> linearLeastSquares;
        Eigen::Matrix<double, n_rot_matrix_dimension, n_rot_matrix_dimension> I_n;
        I_n.setIdentity();
        _TyEdgePool & r_edge_pool = m_system.r_Edge_Pool();
        for (size_t i = 0, n = r_edge_pool.n_Size(); i < n; ++i)
        {
            _TyEdge & edge = r_edge_pool[i];
            int id1 = (int) edge.n_Vertex_Id(0);
            int id2 = (int) edge.n_Vertex_Id(1);
            const _TyLieGroupMatrix & val_edge  = edge.r_Get_Measurement ();
            Eigen::Matrix<double, n_rot_matrix_dimension, 1> vt= val_edge.template topRightCorner <n_rot_matrix_dimension, 1>();
            _TyVertexPool & r_vertex_pool = m_system.r_Vertex_Pool();
            vt = r_vertex_pool[id1].r_m_State().template topLeftCorner<n_rot_matrix_dimension, n_rot_matrix_dimension>() * vt;
            linearLeastSquares.addEdge(id1, id2, -I_n, I_n, vt);
        }
        linearLeastSquares.fixVertex(0, Eigen::Matrix<double, n_rot_matrix_dimension, 1>::Zero());
        if(!linearLeastSquares.solve())
            return false;
        _TyVertexPool &r_vertex_pool = m_system.r_Vertex_Pool();
        for (size_t i = 0, n = r_vertex_pool.n_Size(); i < n; ++i)
        {
            _TyVertex &vertex = r_vertex_pool[i];
            vertex.r_m_State(). template topRightCorner <n_rot_matrix_dimension, 1>() = linearLeastSquares.getVertex(i);
        }
        // recompute edge initial estimate
        for (size_t i = 0, n = r_edge_pool.n_Size(); i < n; ++i)
        {
            _TyEdge & edge = r_edge_pool[i];
            int id1 = (int) edge.n_Vertex_Id(0);
            int id2 = (int) edge.n_Vertex_Id(1);
            edge.r_Get_EdgeEstimate() = _TySE::Inv (r_vertex_pool[id1].r_m_State()) * r_vertex_pool[id2].r_m_State();
        }
        return true;
    }


    /**@brief calculate estimates of vertices according to an arbitray spanning tree. */
    inline bool Calculate_Vertices()
    {
        return m_system.Calculate_VertexEstimate();
    }


    inline void Dump_Edges (const char* p_s_file_name)
    {
        m_system.Dump_EdgeEstimate(p_s_file_name);
    }

    inline void Dump_Vertices (const char* p_s_file_name)
    {
        if(this->Calculate_Vertices())
            m_system.Dump_VertexEstimate(p_s_file_name);
        else
            std::cerr << "Vertices are not calculated correctly!" << std::endl;
    }

    inline double f_ObjFunc()
    {
        return m_system.f_ObjFunc();
    }



    void PrintSummaryInfo (std::ostream & os)
    {
        os << "\nFinish EC-PGO in " << m_n_used_iteration << " iterations.\n";

        os << "\nFinal_ObjFunc ( " << *(m_v_f_objective_function.end() - 1) << " )\n\n";

        os << "\n\n##### --- summary info --- #####\n";

        os << m_g_cb;


        os << "\nused_iterations ( " << m_n_used_iteration << " )\n\n";

        os << "sys_info ( NumEdges: " << this->m_system.n_Edge_Num()
           << "  NumCycles: " << this->m_system.n_Cycle_Num()
           << "  NumVertices: " << this->m_system.n_Vertex_Num()
           << " )" << std::endl << std::endl;

        os << "Lambda_info ( Size: " << this->m_system.r_Lambda().n_BlockRow_Num() << " by " << this->m_system.r_Lambda().n_BlockColumn_Num()
           << "  NumNonZero: " << this->m_system.r_Lambda().n_Symmetric_NonZero_Num()/(n_dimension*n_dimension)
           << "  NonZeroRatio: " << this->m_system.r_Lambda().f_Symmetric_NonZero_Ratio()
           << " )" << std::endl << std::endl;


        size_t n_sys_size = m_system.n_System_Allocation_Size();
        size_t n_lam_size = m_system.n_SystemMatrix_Allocation_Size();
        size_t n_rp_size = m_system.n_DeductionPlan_Allocation_Size();
        size_t n_mem_size = n_sys_size + n_rp_size + n_lam_size;
        os << "memory_used ( sys: " << n_sys_size / 1048576.0 << " MB"
           << ", redplan: " << n_rp_size / 1048576.0 << " MB"
           << ", Lambda: " << n_lam_size / 1048576.0 << " MB"
           << ", Overall: " << n_mem_size / 1048576.0 << " MB"
           << " )" << std::endl << std::endl;


        double f_time_solve =  m_f_time_allocate_system_matrix + m_f_time_update_edge_estimate + m_f_time_calculate_system_matrix + m_f_time_solve_cycle_system;


        os << "time_overall ( cycle_basis: " << m_f_time_cycle_basis << " sec"
           << ", ordering: " << m_f_time_symbolic_factorization << " sec"
           << ", solve: " << f_time_solve << " sec"
           << " ) \t sum: " << m_f_time_cycle_basis + m_f_time_symbolic_factorization + f_time_solve << " sec"
           << "\n\n";


        os << "time_used_solve ( linearization: " << m_f_time_calculate_system_matrix << " sec"
           << ", cholesky_solve: " << m_f_time_solve_cycle_system << " sec"
           << ", update: " << m_f_time_update_edge_estimate << " sec"
           << ", system_matrix_allocation: " << m_f_time_allocate_system_matrix << " sec"
           << " )\n\n";

        os << "time_used_solve_per_iteration ( linearization: " << m_f_time_calculate_system_matrix/m_n_used_iteration << " sec"
           << ", cholesky_solve: " << m_f_time_solve_cycle_system/m_n_used_iteration << " sec"
           << ", update: " << m_f_time_update_edge_estimate/m_n_used_iteration << " sec"
           << ", system_matrix_allocation: " << m_f_time_allocate_system_matrix << " sec"
           << " )\n\n";

        os << "time_used ( Overall: "
           << m_f_time_cycle_basis + m_f_time_symbolic_factorization + f_time_solve
           << " )" << std::endl << std::endl;


        os << "perturbation (";
        for (size_t i = 0; i < m_v_f_perturbation.size(); ++i)
            os << " " << m_v_f_perturbation[i];
        os << " )\n\n";


        os << "cycle_error (";
        for (size_t i = 0; i < m_v_f_cycle_error.size(); ++i)
            os << " " << m_v_f_cycle_error[i];
        os << " )\n\n";


        os << "obj_func (";
        for (size_t i = 0; i < m_v_f_objective_function.size(); ++i)
            os << " " << m_v_f_objective_function[i];
        os << " )\n\n";


        if (m_b_dump_vertex_per_iter)
        {
            os << "odometry_error (";
            std::string str_opt_file = m_str_output_dir +
                    "v" + std::to_string(m_n_used_iteration) + ".txt";
            for ( int i = 0; i <= m_n_used_iteration; ++i)
            {
                std::string str_tmp_file = m_str_output_dir + "v" + std::to_string(i) + ".txt";
                double val = msc::CompFile<ty2D>(str_opt_file.c_str(), str_tmp_file.c_str());
                os << " " << val;
            }
            os << " )\n\n";

            os << "obj_func_vertex_based (";
            for(size_t i = 0; i < m_v_f_objFunc_vertex_based.size(); ++i)
                os << " " << m_v_f_objFunc_vertex_based[i];
            os << " )\n\n";
        }

    }




    /** @brief load edges from a given file
      * @attention interface function to be called in main */
    bool Load_Edges (const char * p_s_file_name)
    {

        std::ifstream ifs(p_s_file_name);

        std::string line;

        if( ifs.peek() == std::ifstream::traits_type::eof() )
        {
            std::cerr << "Empty measurement data (edge) file!" << std::endl;
            return false;
        }

        // maximum existing ID for nodes
        int n_max_vidx = -1;
        // edge id counter
        int n_edge_id_cnt = -1;


        if( ifs.is_open() )
        {
            while (std::getline(ifs, line))
            {
                std::vector<std::string> tokens;

                msc_func::SplitString(line, tokens);

                if( !tokens.empty() )
                    if( tokens[0].compare(0, 4, "Edge") == 0 || tokens[0].compare(0, 4, "EDGE") == 0 || tokens[0].compare("ODOMETRY") == 0)
                    {

                        int n_vertex_id1 = std::atoi(tokens[1].c_str());
                        int n_vertex_id2 = std::atoi(tokens[2].c_str());

                        _TyLieAlgebraVector v_measurement;
                        _TyJacobianMatrix m_information;

                        if(ty2D)
                        {
                            v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(
                                    tokens[5].c_str());

                            double m11 = std::atof(tokens[6].c_str());
                            double m12 = std::atof(tokens[7].c_str());
                            double m13 = std::atof(tokens[8].c_str());
                            double m22 = std::atof(tokens[9].c_str());
                            double m23 = std::atof(tokens[10].c_str());
                            double m33 = std::atof(tokens[11].c_str());

                            m_information << m11, m12, m13,
                                    m12, m22, m23,
                                    m13, m23, m33;
                        }
                        if(!ty2D)
                        {
                            if (tokens[0].compare(0, 13, "EDGE_SE3:QUAT") == 0)
                            {

                                double qw = std::atof(tokens[9].c_str());

                                Eigen::Vector3d rv (std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str()));


                                if (rv.norm() < 1e-15)
                                    rv.setZero();
                                else
                                    rv =  ( 2 * std::acos(qw) / rv.norm() ) * rv;


                                v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                                        rv[0], rv[1], rv[2];


                                double m11 = std::atof(tokens[10].c_str());
                                double m12 = std::atof(tokens[11].c_str());
                                double m13 = std::atof(tokens[12].c_str());
                                double m14 = std::atof(tokens[13].c_str());
                                double m15 = std::atof(tokens[14].c_str());
                                double m16 = std::atof(tokens[15].c_str());

                                double m22 = std::atof(tokens[16].c_str());
                                double m23 = std::atof(tokens[17].c_str());
                                double m24 = std::atof(tokens[18].c_str());
                                double m25 = std::atof(tokens[19].c_str());
                                double m26 = std::atof(tokens[20].c_str());

                                double m33 = std::atof(tokens[21].c_str());
                                double m34 = std::atof(tokens[22].c_str());
                                double m35 = std::atof(tokens[23].c_str());
                                double m36 = std::atof(tokens[24].c_str());

                                double m44 = std::atof(tokens[25].c_str());
                                double m45 = std::atof(tokens[26].c_str());
                                double m46 = std::atof(tokens[27].c_str());

                                double m55 = std::atof(tokens[28].c_str());
                                double m56 = std::atof(tokens[29].c_str());

                                double m66 = std::atof(tokens[30].c_str());

                                m_information << m11, m12, m13, m14, m15, m16,
                                        m12, m22, m23, m24, m25, m26,
                                        m13, m23, m33, m34, m35, m36,
                                        m14, m24, m34, m44, m45, m46,
                                        m15, m25, m35, m45, m55, m56,
                                        m16, m26, m36, m46, m56, m66;


                            }
                            else
                            {

                                v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                                        std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str());

                                double m11 = std::atof(tokens[9].c_str());
                                double m12 = std::atof(tokens[10].c_str());
                                double m13 = std::atof(tokens[11].c_str());
                                double m14 = std::atof(tokens[12].c_str());
                                double m15 = std::atof(tokens[13].c_str());
                                double m16 = std::atof(tokens[14].c_str());

                                double m22 = std::atof(tokens[15].c_str());
                                double m23 = std::atof(tokens[16].c_str());
                                double m24 = std::atof(tokens[17].c_str());
                                double m25 = std::atof(tokens[18].c_str());
                                double m26 = std::atof(tokens[19].c_str());

                                double m33 = std::atof(tokens[20].c_str());
                                double m34 = std::atof(tokens[21].c_str());
                                double m35 = std::atof(tokens[22].c_str());
                                double m36 = std::atof(tokens[23].c_str());

                                double m44 = std::atof(tokens[24].c_str());
                                double m45 = std::atof(tokens[25].c_str());
                                double m46 = std::atof(tokens[26].c_str());

                                double m55 = std::atof(tokens[27].c_str());
                                double m56 = std::atof(tokens[28].c_str());

                                double m66 = std::atof(tokens[29].c_str());

                                m_information << m11, m12, m13, m14, m15, m16,
                                        m12, m22, m23, m24, m25, m26,
                                        m13, m23, m33, m34, m35, m36,
                                        m14, m24, m34, m44, m45, m46,
                                        m15, m25, m35, m45, m55, m56,
                                        m16, m26, m36, m46, m56, m66;

                            }

                        }



                        _TyEdge edge(n_vertex_id1, n_vertex_id2, v_measurement, m_information);

                        _TyEdge & e = m_system.r_Add_Edge(edge);

                        m_v_edge_id_vs_ptr.push_back(e.Ptr());


                        /**@brief add to topological graph for MCB computation */
                        while ((int)n_vertex_id1 > n_max_vidx)
                        {
                            ++ n_max_vidx;
                            m_g_cb.addNode(n_max_vidx);
                        }
                        while ((int)n_vertex_id2 > n_max_vidx)
                        {
                            ++ n_max_vidx;
                            m_g_cb.addNode(n_max_vidx);
                        }
                        ++ n_edge_id_cnt;
                        m_g_cb.addEdge(n_edge_id_cnt, (int)n_vertex_id1, (int)n_vertex_id2);

                    }
            }
        } else{
            std::cerr << "Read edge data failed! Aborting.." << std::endl;
            return false;
        }

//        printf("\ngraph info: nodes %d, edges %d\n", n_max_vidx, n_edge_id_cnt);

        ifs.close();

        return true;
    }



    bool SetEdgeEstimate (const char * p_s_file_name)
    {
        std::ifstream ifs(p_s_file_name);

        std::string line;

        if( ifs.peek() == std::ifstream::traits_type::eof() )
        {
            std::cerr << "Empty measurement data (edge) file!" << std::endl;
            return false;
        }

        // edge id counter
        int n_edge_id_cnt = -1;

        _TyEdgePool & r_edge_pool = m_system.r_Edge_Pool();

        if( ifs.is_open() )
        {
            while (std::getline(ifs, line))
            {
                std::vector<std::string> tokens;

                msc_func::SplitString(line, tokens);

                if( !tokens.empty() )
                    if( tokens[0].compare(0, 4, "Edge") == 0 || tokens[0].compare(0, 4, "EDGE") == 0 || tokens[0].compare("ODOMETRY") == 0)
                    {
                        int n_vertex_id1 = std::atoi(tokens[1].c_str());
                        int n_vertex_id2 = std::atoi(tokens[2].c_str());

                        _TyLieAlgebraVector v_measurement;

                        if(ty2D)
                        {
                            v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(
                                    tokens[5].c_str());
                        }
                        if(!ty2D)
                        {
                            if (tokens[0].compare(0, 13, "EDGE_SE3:QUAT") == 0)
                            {
                                double qw = std::atof(tokens[9].c_str());

                                Eigen::Vector3d rv (std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str()));

                                if (rv.norm() < 1e-15)
                                    rv.setZero();
                                else
                                    rv =  ( 2 * std::acos(qw) / rv.norm() ) * rv;

                                v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                                        rv[0], rv[1], rv[2];
                            }
                            else
                            {
                                v_measurement << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                                        std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str());
                            }

                        }

                        _TyEdge & e = r_edge_pool[++n_edge_id_cnt];

                        if( e.n_Vertex_Id(0) == n_vertex_id1 && e.n_Vertex_Id(1) == n_vertex_id2 )
                        {
                            e.Set_EdgeEstimate(_TySE::V2M( v_measurement ));
                        }
                        else
                        {
                            std::cerr << "Endpoints of the edge do not match the existing ones!\n";
                            return false;
                        }
                    }
            }
        } else{
            std::cerr << "Read edge data failed! Aborting.." << std::endl;
            return false;
        }

        ifs.close();

        return true;
    }


    void Plot(const char * p_s_filename) const
    {
        if (ty2D)
            m_system.Plot2D(p_s_filename, plot_quality::plot_Printing);
        else
            m_system.Plot3D(p_s_filename, plot_quality::plot_Printing);
    }

    void PlotSystemMatrix(const char * p_s_system_fig = "system.tga", int n_scalar_size = 5) const
    {
        m_system.PrintSystemMatrix(p_s_system_fig, n_scalar_size);
    }




};







#endif //SLAM_PLUS_PLUS_ECPGO_NONLINEARSOLVER_H
