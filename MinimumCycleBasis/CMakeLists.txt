cmake_minimum_required(VERSION 3.5)
project(MinimumCycleBasis)

set(CMAKE_CXX_STANDARD 11)

SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Ofast -march=native -fopenmp")

option(Debug_APSP "DEBUG_APSP" OFF)
option(Use_LexDijkstra "use LexDijkstra implementation" ON)
option(Use_Omp_Parallelism "use OMP to parallelize Dijkstra" ON)
option(Use_BFS_Graph_Compression "use BFS to construct Compact Graph to increase memory vicinity" ON)
option(Use_STL_Priority_Queue "use std::priority_queue for Dijkstra, namely the binary heap." ON)
option(Reuse_Shorteset_Paths_in_APSP "Reuse shortest paths calculated in previous steps." OFF)
option(SmoothOut_DegreeTwo_Vertices "Smoothing out vertices of degree two" ON)


if (Debug_APSP)
    add_definitions( -DDEBUG_APSP )
endif(Debug_APSP)

if (Use_LexDijkstra)
    add_definitions( -DMCB_LEX_DIJKSTRA_IMPLEMENTATION )
endif (Use_LexDijkstra)

if (Use_Omp_Parallelism)
    add_definitions( -DMCB_USE_OMP_PARALLELIZED_DIJKSTRA )
endif (Use_Omp_Parallelism)

if (Use_BFS_Graph_Compression)
  add_definitions( -DMCB_BFS_BASED_COMPRESSION )
endif (Use_BFS_Graph_Compression)
    
if (Use_STL_Priority_Queue)
    add_definitions( -DMCB_PRIORITY_QUEUE_BASED_IMPLEMENTATION )
endif (Use_STL_Priority_Queue)

if (Reuse_Shorteset_Paths_in_APSP)
    add_definitions( -DMCB_APSP_REUSE_SHORTEST_PATHS )
endif (Reuse_Shorteset_Paths_in_APSP)

if (SmoothOut_DegreeTwo_Vertices)
    add_definitions( -DMCB_USE_COMPRESSED_GRAPH_BY_ELIMINATING_DEGREE_TWO_VERTICES )
endif (SmoothOut_DegreeTwo_Vertices)


include_directories(include)

# add_subdirectory(test)

add_subdirectory(src)
