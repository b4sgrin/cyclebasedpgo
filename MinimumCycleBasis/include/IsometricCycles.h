//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_ISOMETRICCYCLES_H
#define MINIMUMCYCLEBASIS_ISOMETRICCYCLES_H

#include <list>
#include <vector>

#include "APSP.h"




struct TimingInfo
{
    double f_time_link_representation;
    double f_time_find_connected_component;
    double f_time_setup_cycle_vector;
    double f_time_sort_cycles_ascending;

    TimingInfo ():
            f_time_link_representation(.0),
            f_time_find_connected_component(.0),
            f_time_setup_cycle_vector(.0),
            f_time_sort_cycles_ascending(.0)
    {}

    friend std::ostream & operator<< (std::ostream & os, const struct TimingInfo & obj)
    {
        os << "\n";
        os << "Time used by linking representation: " << obj.f_time_link_representation << "\n";
        os << "Time used by finding connected component: " << obj.f_time_find_connected_component << "\n";
        os << "Time used by setting up isometric cycle vector: " << obj.f_time_setup_cycle_vector << "\n";
        os << "Time used by sorting cycles ascending: " << obj.f_time_sort_cycles_ascending << "\n";
        os << "\n";
        return os;
    }

};


class IsometricCycles
{

public:

    typedef struct CycleContent
    {
        struct CycleInfoTag
        {
            int m_n_weight;

            int m_n_length;

        } m_info;

        std::vector<int> m_edges;

        bool operator < (const CycleContent & other) const
        {
            return m_info.m_n_weight < other.m_info.m_n_weight;
        }

    } TyCycleContent;


    // write an allocator here.
    typedef std::list<TyCycleContent> TyCycleContentContainer;


private:

    int m_n_num_nodes;

    int m_n_num_edges;

    ShortestPaths * m_p_graph;   /// interface for shortest-path-trees, and edge-set

    int m_n_num_isometric_cycles;


    /**@brief Each node in dgraph represents an element (i.e., a cycle) in Horton Set
     * @remark A cycle in horton set can be expressed as a <node, edge> pair
     * @note The <node, edge> pair can be mapped to a unique index as an integer
     * if node/edge ID is bounded, by number M/N.
     * The mapping is continuous iff edge and node IDs are continuous. */

    /**@brief there are 2 and only 2 outgoing edges for each node in dgraph*/
    int * m_v_dgraph_adjacentNodes_buffer;
    /// two slots for one node. For a node with index k, the adjacent lists are stored at slot 2k and 2k+1.

    int * m_v_isometric_index_buffer;


    /**@brief container to hold a collection of isometric cycles */
    TyCycleContentContainer m_v_isometric_cycle_buffer;




private:

    IsometricCycles (const IsometricCycles &);

    IsometricCycles & operator= (const IsometricCycles &);

    TimingInfo m_time_info;

public:

    IsometricCycles (ShortestPaths * t_graph)
            :m_n_num_nodes(t_graph->n_num_nodes()),
             m_n_num_edges(t_graph->n_num_edges()),
             m_p_graph(t_graph),
             m_n_num_isometric_cycles(0)

    {
        int t_n_MN = m_n_num_nodes * m_n_num_edges;
        m_v_dgraph_adjacentNodes_buffer = (int*) malloc( 2 * t_n_MN * sizeof(int) ); // 2 slots for one node
        m_v_isometric_index_buffer = (int*) malloc ( t_n_MN * sizeof(int) );
    }



    inline int n_num_nodes ()
    {
        return m_n_num_nodes;
    }



    inline int n_num_edges ()
    {
        return m_n_num_edges;
    }


    /**@brief An interface function for Horton & Isometric cycle construction*/
    inline ShortestPaths::TyCptEdge & getEdge (int n_compact_edge_index)
    {
        return m_p_graph->getEdge(n_compact_edge_index);
    }

    /**@brief An interface function for Horton & Isometric cycle construction*/
    inline const ShortestPaths::TyCptEdge & getEdge (int n_compact_edge_index) const
    {
        return m_p_graph->getEdge(n_compact_edge_index);
    }


    /**@brief interface function for other class to get the isometric cycle set */
    inline TyCycleContentContainer & getIsometricCycles()
    {
        return m_v_isometric_cycle_buffer;
    }


    /**@brief interface function for other class to get the isometric cycle set */
    inline const TyCycleContentContainer & getIsometricCycles() const
    {
        return m_v_isometric_cycle_buffer;
    }


    /**@brief Index mapping.
     * from <node_id, edge_id> pair to a unique index */
    inline int indexMapping (int t_n_node_id, int t_n_edge_id)
    {
        return (t_n_node_id * m_n_num_edges + t_n_edge_id);
    }


    /**@brief Inverse Index mapping.
     * from a unique index to recover unique <node_id, edge_id> pair */
    inline std::pair<int, int> indexInvMapping (int t_n_index)
    {
        // below 2 lines will be optimized by compiler, as % is a by-product of /
        int n_node_id = t_n_index / m_n_num_edges;
        int n_edge_id = t_n_index % m_n_num_edges;
        return std::make_pair(n_node_id, n_edge_id);
    };


    inline void computeIsometricCycles ()
    {
        this->linkHortonCycleRepresentations();
        this->computeConnectedComponents();
       // std::cout << m_time_info;
    }


    /**@brief first node on the path x -> y
     * @remarks the name of the function "S" is in accordance with the paper of Amaldi et. al
     * @attention "Breaking the O(m^2n) Barrier for Minimum Cycle Bases"
     * @attention "Improved minimum cycle bases algorithm by restriction to isometric cycles"*/
    inline int S (int x, int y)
    {
        // path(x, y) == path(y, x)
        // use shortest-path-tree rooted at y, rather than x
        const ShortestPaths::TySptIndex & spt = m_p_graph->getSptIndex(y, x);
        // the shortest-path from x to y, can be easily obtained by tracing-back along the spt_tree.
        return spt.m_n_parrent_node_id;
    }



    /**@brief return an equivalent cycle representation
     * @param[in]node_id index of node
     * @param[in]edge_id index of edge
     * @param[in]source_id the source node of the edge
     * @param[in]target_id the target node of the edge
     * @return the node id in dgraph, namely a mapped <node_id, edge_id> pair to a unique index */
    inline int findRepresentation (int node_id, int edge_id, int source_id, int target_id)
    {
        /**@brief find another other representation for C(x, uv) */


        /// uv is on P(u,x) OR vu is on P(v,x)
        if ( m_p_graph->getSptIndex(node_id, source_id).m_n_edge_id == edge_id ||
                m_p_graph->getSptIndex(node_id, target_id).m_n_edge_id == edge_id )
        {
            return (-1); // labeled as Bad (-1).
        }


        /**@brief Path(node_id, edge_source_id) */
        ShortestPaths::TySptIndex & path_ns = m_p_graph->getSptIndex(source_id, node_id);  // path x->u
        // First node on Path(node_id, edge_source_id)
        int ns_node = path_ns.m_n_parrent_node_id;   /// x'
        int ns_edge = path_ns.m_n_edge_id;    /// xx'

        
        /**@brief Exists a shared node on path(x,u) and path(x,v) */
        if ( ns_node != -1 && ns_node == this->S(node_id, target_id) )
        {
            return  (-1); // labeled as Bad (-1).
        }


        /**@brief As a special case, this case can tackle (node_id == source_id == target_id), i.e., self-loop */
        /// x = u
        if (node_id == source_id)
        {
            return (target_id * m_n_num_edges + edge_id);  /// C(x, uv) --> C(v, uv)
        }

        /// x = S(x', v)
        if (node_id == this->S(ns_node, target_id) )
        {
            return (ns_node * m_n_num_edges + edge_id);  /// C(x, uv) --> C(x', uv)
        }

        /// u = S(v, x')
        if (source_id == this->S(target_id, ns_node))
        {
            return (target_id * m_n_num_edges + ns_edge);  /// C(x, uv) --> C(v, xx')
        }

        /**@brief the cycle is non-isometric */
        {
            return (-1); // labeled as Bad (-1).
        }

    }




    /**@brief connect different representations of isometric cycles on dgraph */
    void linkHortonCycleRepresentations ()
    {
        Timer T;

        /**@remarks All combinations of <node_id, edge_id> denote the set of Horton cycles */
#ifdef MCB_USE_OMP_PARALLELIZED_DIJKSTRA
        #pragma omp parallel for
#endif
        for (int node_id = 0; node_id < m_n_num_nodes; ++node_id)
        {
            for (int edge_id = 0; edge_id < m_n_num_edges; ++edge_id)
            {

                const ShortestPaths::TyCptEdge & r_edge = m_p_graph->getEdge(edge_id);

                /**@brief calculate index directly for parallelism. */
                int * ptr_pos_horton = m_v_dgraph_adjacentNodes_buffer + ( node_id * m_n_num_edges + edge_id) * 2;


                // slot 2k for index k
                *ptr_pos_horton = this->findRepresentation(node_id, edge_id, r_edge.m_n_source, r_edge.m_n_target);
		

#ifdef MCB_ISOMETRIC_DEBUG_DOUBLE_LINKED_CYCLE
		if (*ptr_pos_horton > 0) /// used to debug
		{
		  int tmp_node = indexInvMapping(*ptr_pos_horton).first;
		  int tmp_edge = indexInvMapping(*ptr_pos_horton).second;
		  printf("link pair: C(%d, <%d, %d>) ---> C(%d, <%d, %d>)\n", node_id, r_edge.m_n_source, r_edge.m_n_target, tmp_node, m_p_graph->getEdge(tmp_edge).m_n_source, m_p_graph->getEdge(tmp_edge).m_n_target);
		}
#endif					
		
                // slot 2k+1 for index k
                *(++ptr_pos_horton) = this->findRepresentation(node_id, edge_id, r_edge.m_n_target, r_edge.m_n_source);


#ifdef MCB_ISOMETRIC_DEBUG_DOUBLE_LINKED_CYCLE
		if (*ptr_pos_horton > 0) /// used to debug
		{
		  int tmp_node = indexInvMapping(*ptr_pos_horton).first;
		  int tmp_edge = indexInvMapping(*ptr_pos_horton).second;
		  printf("link pair: C(%d, <%d, %d>) ---> C(%d, <%d, %d>)\n", node_id, r_edge.m_n_source, r_edge.m_n_target, tmp_node, m_p_graph->getEdge(tmp_edge).m_n_source, m_p_graph->getEdge(tmp_edge).m_n_target);
		}
#endif					
            }
        }


        m_time_info.f_time_link_representation = T.toc();
    }



    /**@brief Compute connected-components of dgraph */
    void computeConnectedComponents ()
    {
        Timer T;

        /**@brief each connect component is a double-linked circuit */
        int * ptr_pos_isometric = m_v_isometric_index_buffer;  // isometric cycle iterator

        int * ptr_pos_horton = m_v_dgraph_adjacentNodes_buffer;  // horton cycle iterator

        for (int id = 0, n_max_id = m_n_num_nodes*m_n_num_edges; id < n_max_id; ++id)
        {
            if( *ptr_pos_horton != -1 && *(ptr_pos_horton+1) != -1 ) // current horton node is not a Bad node
            {
                /**@brief traverse the connect component of this node, and count the number of representations */
                /**@note For an isometric cycle, the number of representation should equal to the number of edges/nodes. */
                int n_num_representations = 1;
                int n_prev_id = id;
                int n_next_id = *ptr_pos_horton; // chose arbitrary traverse direction.

                /** next_node_id, with adjacent nodes {n_first, n_second} */
                int * ptr_next = m_v_dgraph_adjacentNodes_buffer + 2 * n_next_id;
                int n_first = *ptr_next; // content of 2k
                int n_second = *(ptr_next+1); // content of 2k+1


                while (n_first != -1 && n_second != -1 && n_next_id != id)
                {
                    ++n_num_representations;  // detect a new representation.

                    if(n_first == n_prev_id)
                    {
                        n_prev_id = n_next_id; n_next_id = n_second;
                    }
                    else if (n_second == n_prev_id)
                    {
                        n_prev_id = n_next_id; n_next_id = n_first;
                    }
                    else
                    {
                        break; // no link for next-node to current node. i.e., not double linked.
                    }

                    // Disable the adjacent link at next_node_id;
                    *ptr_next = -1;
                    ++ptr_next;
                    *ptr_next = -1;

                    // find next ptr by id
                    ptr_next = m_v_dgraph_adjacentNodes_buffer + 2 * n_next_id;
                    n_first = *ptr_next; // content of 2k
                    n_second = *(ptr_next+1); // content of 2k+1
                }


                /**@brief count the number of representations. */
                /**@brief the cycle is isometric if the number of representations equals the number of edges */

                // seems we should recover the infomation of cycle i, and save it directly if it is isometric
                int node_id = (id) / m_n_num_edges;
                int edge_id = (id) % m_n_num_edges;
                ShortestPaths::TyCptEdge & r_edge = m_p_graph->getEdge(edge_id);

                int n_source_id = r_edge.m_n_source;
                int n_target_id = r_edge.m_n_target;
                int n_weight = r_edge.m_n_weight;

                ShortestPaths::TySptIndex * spt_tree_at_node_id = m_p_graph->getSptIndex(node_id);


                /**@brief below two lines use length and weight */
                ShortestPaths::TyComplexWeight & spt_weight_source = m_p_graph->getSptWeight(node_id, n_source_id);
                ShortestPaths::TyComplexWeight & spt_weight_target = m_p_graph->getSptWeight(node_id, n_target_id);


                int cycle_length = spt_weight_source.m_n_length + spt_weight_target.m_n_length + 1;


                if(cycle_length == n_num_representations ) {

                    int cycle_weight = spt_weight_source.m_n_distance + spt_weight_target.m_n_distance + n_weight;

                    std::vector<int> v_cycle_edges;
                    v_cycle_edges.reserve(cycle_length);


                    ShortestPaths::TySptIndex *spt_tree_at_source = m_p_graph->getSptIndex(n_source_id);

                    // Path: node_id -> edge_source_id
                    ShortestPaths::TySptIndex *spt_pos = spt_tree_at_source + node_id;

                    while (spt_pos->m_n_parrent_node_id != -1) {

                        v_cycle_edges.emplace_back(spt_pos->m_n_edge_id);

                        spt_pos = spt_tree_at_source + spt_pos->m_n_parrent_node_id;
                    }


                    v_cycle_edges.emplace_back(edge_id); /// edge id;

                    // Path: edge_target_id -> node_id;
                    spt_pos = spt_tree_at_node_id + n_target_id;

                    while (spt_pos->m_n_parrent_node_id != -1) {

                        v_cycle_edges.emplace_back(spt_pos->m_n_edge_id);

                        spt_pos = spt_tree_at_node_id + spt_pos->m_n_parrent_node_id;
                    }



                    /**@brief put isometric cycle into container */
                    m_v_isometric_cycle_buffer.emplace_back(TyCycleContent{{cycle_weight, cycle_length}, v_cycle_edges});



                    // ---
                    *ptr_pos_isometric = id;   // save isometric cycle by dgraph id.
                    ++ptr_pos_isometric;
                    // ---

                    ++m_n_num_isometric_cycles;
                }

            }

            /**@brief disable this node's adjacent list */
            *ptr_pos_horton = -1; ++ptr_pos_horton;
            *ptr_pos_horton = -1; ++ptr_pos_horton;

        }



        m_time_info.f_time_find_connected_component += T.toc(); T.tic();

        /**@brief sort the buffer according to the weight. */
        m_v_isometric_cycle_buffer.sort();
//        std::sort(m_v_isometric_cycle_buffer.begin(), m_v_isometric_cycle_buffer.end());
        /// please use std::sort if we use vector based buffer.
        m_time_info.f_time_sort_cycles_ascending = T.toc();
    }


    void printIsometricCycles (std::ostream & os)
    {
        assert( (size_t) m_n_num_isometric_cycles == m_v_isometric_cycle_buffer.size() );

        os << "\n\nIsometric Cycles.\n";
        os << "\tDim. of Cycle Bases: " << (m_n_num_edges - m_n_num_nodes + 1) << "\n";
        os << "\tNum. of Isometric Cycles: " << m_n_num_isometric_cycles << "\n";

        this->printCycles(os);
    }


    void printCycles (std::ostream & os)
    {
        for (auto it = m_v_isometric_cycle_buffer.begin(), end = m_v_isometric_cycle_buffer.end(); it != end; ++it)
        {
            std::vector<int> & ref_edges = (*it).m_edges;
            os << "\n\tDist: " << (*it).m_info.m_n_weight << ".\tLength: " << (*it).m_info.m_n_length << ":\n\t";
            for (int i = 0, n = ref_edges.size(); i < n; ++i)
            {
                os << ref_edges[i] << " ";
            }
            os << "\n";
        }
    }



    ~IsometricCycles()
    {
        free(m_v_dgraph_adjacentNodes_buffer);
        free(m_v_isometric_index_buffer);
    }



};

#endif //MINIMUMCYCLEBASIS_ISOMETRICCYCLES_H
