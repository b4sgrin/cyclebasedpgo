//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_ADAPTIVEISOMETRICCYCLESEXTRACTION_H
#define MINIMUMCYCLEBASIS_ADAPTIVEISOMETRICCYCLESEXTRACTION_H


#include <list>  ///STL list.//

#include <vector>

#include <algorithm>

#include "IsometricCycles.h"

#include "AdaptiveSpanningTree.h"   /// implementation for adaptive spanning tree

#include "SupportVector.h"   /// implementation for support vectors.


/**@brief Using
 * template < class TyWitness >
 * class AdaptiveIsometricCyclesExtraction
 */



class AdaptiveIsometricCyclesExtraction
{

public:

    /**@brief using vector-based support-vector (i.e., witness) implementation. */
    typedef SupportVector TyWitness;


private:


    IsometricCycles * m_p_isometric_cycles;

    int m_n_num_nodes;

    int m_n_num_edges;

    int m_n_num_cycle_bases;

    int m_n_num_implicit_update;

    int m_n_num_explicit_update;

    int m_n_num_implicit_no_witness_update;

    int m_n_num_no_witness_found;

    int m_n_num_useless_edges;



    /**@brief Data used for cycle selection. */
    /**@brief Spanning tree and co-tree edges must support fast edge lookup (use vector lookup)
     * set operation (union, intersection, difference) is not really need.
     * @note vector based implementation can be slow for set operations (union, intersection, difference).
     * But, as long as one of the set in the operation is given using sparse representation,
     * it can be done fast through vector lookup. */

    AdaptiveSpanningTree m_spanning_tree; /// spanning tree

    bool * m_b_cotree_edges; /// co-tree edges

    
    /**@brief a collection of support vectors.
     * @note this data-structure needs frequently insert/delete
     * @note random access - not required.
     * @note ordering - not required. */
    std::list<TyWitness> m_witnesses; /// explicit witnesses, i.e., support vectors
    /// consider using std::vector for this as well. thanks to cache compactness, vector is faster even for insert-delete


    int * m_n_cnt_witness_edges; /// edges used by witnesses. Basically, this counts the times of occurrence of each edge.






public:

    AdaptiveIsometricCyclesExtraction(IsometricCycles * t_p_isometric_cycles)
        : m_p_isometric_cycles ( t_p_isometric_cycles ),
          m_n_num_nodes( t_p_isometric_cycles->n_num_nodes() ),
          m_n_num_edges( t_p_isometric_cycles->n_num_edges() ),
          m_n_num_cycle_bases(0),
          m_n_num_implicit_update(0),
          m_n_num_explicit_update(0),
          m_n_num_implicit_no_witness_update(0),
          m_n_num_no_witness_found(0),
          m_n_num_useless_edges(0),
          m_spanning_tree( m_n_num_nodes, m_n_num_edges )
    {
        m_b_cotree_edges = new bool[m_n_num_edges](); /// initialized to false
        m_n_cnt_witness_edges = new int[m_n_num_edges](); /// initialized to 0
    }



    inline int n_num_cycle_basis ()
    {
        return m_n_num_cycle_bases;
    }

    inline int n_num_implicit_update ()
    {
        return m_n_num_implicit_update;
    }

    inline int n_num_explicit_update ()
    {
        return m_n_num_explicit_update;
    }

    inline int n_num_implicit_no_witness_update ()
    {
        return m_n_num_implicit_no_witness_update;
    }

    inline int n_num_no_witness_found ()
    {
        return m_n_num_no_witness_found;
    }

    inline int n_num_useless_edges ()
    {
        return m_n_num_useless_edges;
    }




    inline IsometricCycles::TyCycleContentContainer & getCycleBasis ()
    {
        return m_p_isometric_cycles->getIsometricCycles();
    }


    /**@brief read sequential IDs on a cycle, and convert it to Es En (ordered) */
    void extractCycleInfo (const std::vector<int> & isometric_cycle, std::vector<int> & E_s, std::vector<int> & E_n)
    {
        for (auto id : isometric_cycle)
        {
            if (!m_spanning_tree.IsTreeEdge(id) && !m_b_cotree_edges[id] )
            {
                CompactGraph::TyCptEdge & r_edge = m_p_isometric_cycles->getEdge(id);

                if ( !m_spanning_tree.InsertEdge(id, r_edge.m_n_source, r_edge.m_n_target) )
                {
                    if ( m_n_cnt_witness_edges[id] ) // an edge used by witnesses
                    {
                        E_s.emplace_back(id);
                    }
                    else // new edge, not in any witness
                    {
                        E_n.emplace_back(id);
                    }
                }
            }
        }
        /**@brief sort E_s and E_n in ascending order. */
        std::sort(E_s.begin(), E_s.end());
        std::sort(E_n.begin(), E_n.end());  // it is not necessary to sort E_n.
    }



    /**@brief process one isometric cycle. line 5-18.
     * @return true = independent, false = Not independent */
    bool processIsometricCycle (const std::vector<int> & isometric_cycle)
    {
        /**@brief reserve space, or use a global one */
        std::vector<int> E_s;
        std::vector<int> E_n;

        this->extractCycleInfo(isometric_cycle, E_s, E_n); // line 5-7

        // process E_s, E_n
        if (E_n.size() != 0) // line 8-11
        {
//            printf("\nImplicit Update, E_n.size() = %d. \n", E_n.size());

            ++m_n_num_implicit_update;

            bool b_flag_witness_updated = false;

            // update all existing witnesses. line 10
            if (E_s.size() != 0)
            {
                for(auto it = m_witnesses.begin(); it != m_witnesses.end(); ++it)
                {
                    if (it->IsOddIntersectionNum(E_s))
                    {
                        // update using implicit witness {E_n[0]}
                        it->UpdateByInsert( E_n[0], m_n_cnt_witness_edges );
                        // set witness update flag here.
                        b_flag_witness_updated = true;
                    }
                }
            }



            int E_n_size_diff = E_n.size() - 1;

            if (E_n_size_diff > 0)  // aka E_n.size > 1
            {
                // new witnesses, i.e., witnesses that become explict
                for (int i = 0; i < E_n_size_diff; ++i)
                {
                    // add new witnesses to the support vector stack
                    m_witnesses.emplace_back( TyWitness(E_n[i], E_n[i+1]) );
                    // initialize counters for new edges used by support vectors.
                    m_n_cnt_witness_edges[ E_n[i] ] += 1;
                    m_n_cnt_witness_edges[ E_n[i+1] ] += 1;
                }
            }
            else // E_n_size_diff = 0; aka E_n.size() = 1
            {
                if(!b_flag_witness_updated) // no witness is updated by E_n[0].
                {
                    ++m_n_num_implicit_no_witness_update;
                    m_b_cotree_edges[ E_n[0] ] = true;  // line 11
                }
            }

            return true; // accepted
        }
        else if (E_s.size() != 0) // line 12-15
        {
//            printf("\nExplicit Update, E_s.size() = %d. \n", E_s.size());

            ++m_n_num_explicit_update;

            auto witness_it = m_witnesses.begin();
            auto end = m_witnesses.end();

            // find an explicit witness to validate the cycle. line 12.
            while(witness_it != end)
            {
                if (witness_it->IsOddIntersectionNum(E_s))
                {
                    break;
                }
                ++witness_it;
            }

            // witness found. line 13-15
            if(witness_it != end)
            {
                auto it = witness_it; ++it;

                // below while loop update witnesses. line 14
                while(it != end)
                {
                    if (it->IsOddIntersectionNum(E_s))
                    {
                        it->UpdateBySymmetricDifference(*witness_it, m_n_cnt_witness_edges); // line 14
                    }
                    ++it;
                }

                // decrease the edge counter and delete the witness.
                witness_it->DecreaseEdgeCounter(m_n_cnt_witness_edges, m_b_cotree_edges);
                m_witnesses.erase(witness_it);

                return true; // accepted
            }
            else
            {
                ++m_n_num_no_witness_found;
               // printf("Rejected: not explicit witness found.\n");
                return false; // rejected. no witness found.
            }
        }
        else
        {
            ++ m_n_num_useless_edges;
     //       printf("Rejected: all useless edges.\n");
            return false; // rejected. all useless edges.
        }
    }




    int main_AICE ()
    {
        IsometricCycles::TyCycleContentContainer & cycle_container = m_p_isometric_cycles->getIsometricCycles();

        auto it = cycle_container.begin(), end = cycle_container.end();
        while (it != end )
        {
            if ( this->processIsometricCycle( (*it).m_edges ) )
            {
                ++m_n_num_cycle_bases;
                ++it;
            }
            else
            {
                it = cycle_container.erase(it);
            }
        }
        return 0;
    }


    void printCycleBases (std::ostream & os)
    {
        assert(m_n_num_cycle_bases == m_n_num_edges - m_n_num_nodes + 1);

        os << "\n\nCycles Bases in Compact-Graph.\n";
        os << "\tNum. of Cycles in Cycle Bases: " << (m_n_num_edges - m_n_num_nodes + 1) << "\n";

        this->m_p_isometric_cycles->printCycles(os);
        os << "\nEND#\n\n";
    }


    ~AdaptiveIsometricCyclesExtraction()
    {
        delete []m_b_cotree_edges;
        delete []m_n_cnt_witness_edges;
    }



};

#endif //MINIMUMCYCLEBASIS_ADAPTIVEISOMETRICCYCLESEXTRACTION_H
