//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_PRIORITYQUEUE_H
#define MINIMUMCYCLEBASIS_PRIORITYQUEUE_H

#include <queue> // std::priority_queue
//#include <boost/heap/priority_queue.hpp>
//#include <boost/heap/fibonacci_heap.hpp>

#include <set>


namespace pq
{
    typedef struct Node
    {
        int id;
        int val;
        /**@brief add length */
       // int len;
       // bool operator < (const struct Node & n) const { (val < n.val || (val == n.val && id < n.id) ); }
    } TNode;

    struct GreaterFunctor
    {
        bool operator()(const TNode & n1, const TNode & n2) const
        {
            return n1.val >= n2.val;
        }
    };

    struct LessFunctor
    {
        bool operator()(const TNode & n1, const TNode & n2) const
        {
            return (n1.val < n2.val || (n1.val == n2.val && n1.id < n2.id) );
        }
    };


    template <class TyNode>
    class PriorityQueueVectorBased
    {
    private:
        std::vector<TyNode> m_pq;

        typedef typename std::vector<TyNode>::iterator _TyIterator;

        _TyIterator m_iter_top;

    public:

        PriorityQueueVectorBased ()
        {
            m_iter_top = m_pq.begin();
        }

        PriorityQueueVectorBased (int size)
        {
            m_pq.reserve(size);
            m_iter_top = m_pq.begin();
        }


        inline void push (const TyNode & node)
        {
            // lower_bound: return less or equal
            // upper_bound: return greater, cannot be equal
            _TyIterator iter = std::lower_bound<_TyIterator, TyNode, LessFunctor> (m_iter_top, m_pq.end(), node);
            m_pq.insert(iter, node);
        }


        inline TyNode & top ()
        {
            return *m_iter_top;
        }


        inline void pop ()
        {
            ++m_iter_top;
        }

    };





#ifdef MCB_PRIORITY_QUEUE_BASED_IMPLEMENTATION

    //    typedef boost::heap::priority_queue<TNode, boost::heap::compare<CompareFunctor>> TyHeap;

    typedef std::priority_queue<TNode, std::vector<TNode>, GreaterFunctor > TyHeap;
  //  typedef std::priority_queue<TNode, std::deque<TNode>, CompareFunctor > TyHeap;

   // typedef PriorityQueueVectorBased<TNode> TyHeap;

#endif



#ifdef MCB_FIBINACCI_HEAP_BASED_IMPLEMENTATION

    typedef boost::heap::fibonacci_heap<TNode, boost::heap::compare<GreaterFunctor>> TyHeap;

    typedef boost::heap::fibonacci_heap<TNode, boost::heap::compare<GreaterFunctor>>::handle_type TyHeapHandle;

#endif



#ifdef MCB_BINARY_SEARCH_TREE_BASED_IMPLEMENTATION

    typedef std::set<TNode, LessFunctor > TyHeap;

#endif


}






#endif //MINIMUMCYCLEBASIS_PRIORITYQUEUE_H
