//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_TIMER_H
#define MINIMUMCYCLEBASIS_TIMER_H


#include <chrono>


/**@brief Use tic() to reset start time point
 * @brief Use toc() to return elapsed time duration in seconds */

class Timer
{

private:

    std::chrono::time_point<std::chrono::high_resolution_clock> m_time_point_now;

public:

    Timer()
            :m_time_point_now ( std::chrono::high_resolution_clock::now() )
    {}


    void tic ()
    {
        m_time_point_now = std::chrono::high_resolution_clock::now();
    }


    double toc ()
    {
        std::chrono::duration<double> t_elapsed = std::chrono::high_resolution_clock::now() - m_time_point_now;
        return t_elapsed.count();
    }
};


#endif //MINIMUMCYCLEBASIS_TIMER_H
