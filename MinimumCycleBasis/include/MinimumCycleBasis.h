//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_MINIMUMCYCLEBASIS_H
#define MINIMUMCYCLEBASIS_MINIMUMCYCLEBASIS_H


#include <iostream>

#include <vector>

#include <list>

#include <unordered_map>   /// used for node ID mapping from Graph->CompactGraph

#include <set>   /// used for Dijkstra search frontier

#include <algorithm>

#include <limits.h>   /// INT_MAX

#include <assert.h>

#include "Timer.h"

#include "Graph.h"

#include "APSP.h"

#include "IsometricCycles.h"

#include "AdaptiveSpanningTree.h"   /// implementation for adaptive spanning tree

#include "SupportVector.h"   /// implementation for support vectors.

#include "AdaptiveIsometricCyclesExtraction.h"


//#define MCB_USE_COMPRESSED_GRAPH_BY_ELIMINATING_DEGREE_TWO_VERTICES



struct TimingStatistics
{
    double m_graph_compression;

    double m_compact_graph_construction;

    double m_shortest_paths;

    double m_isometric_cycle_set;

    double m_adaptive_isometric_cycle_extraction;

    double m_edge_recovery_and_lifting;

    friend std::ostream & operator << (std::ostream & os, const struct TimingStatistics & obj)
    {
        os << "\n";
        os << "Time MCB overall: " <<   obj.m_graph_compression
                                      + obj.m_compact_graph_construction
                                      + obj.m_shortest_paths
                                      + obj.m_isometric_cycle_set
                                      + obj.m_adaptive_isometric_cycle_extraction
                                      + obj.m_edge_recovery_and_lifting
           << " sec\n";
        os << "Time graph compression: " << obj.m_graph_compression << " sec\n";
        os << "Time compact-graph-construction: " << obj.m_compact_graph_construction << " sec\n";
        os << "Time all-pairs-shortest-paths computation: " << obj.m_shortest_paths << " sec\n";
        os << "Time isometric-set-construction: " << obj.m_isometric_cycle_set << " sec\n";
        os << "Time adaptive-isometric-cycle-extraction: " << obj.m_adaptive_isometric_cycle_extraction << " sec\n";
        os << "Time edge-recovery-and-lifting: " << obj.m_edge_recovery_and_lifting << " sec\n";
        os << "\n";
	return os;
    }

};



struct AlgorithmStatistics
{
    int m_n_num_edges_original;

    int m_n_num_vertices_original;

    int m_n_num_edges_reduced;

    int m_n_num_vertices_reduced;

    int m_n_dim_cycle_basis_original;

    int m_n_dim_cycle_basis_reduced;

    int m_n_isometric_circuits;

    int m_n_num_calculated_cycles;

    int m_n_implicit_update;

    int m_n_explicit_update;

    int m_n_num_implicit_no_witness_update;

    int m_n_num_no_witness_found;

    int m_n_num_useless_edges;


    friend std::ostream & operator << (std::ostream & os, const struct AlgorithmStatistics & obj)
    {
        os << "\n";
        os << "Num. of edges - original: " << obj.m_n_num_edges_original << "\n";
        os << "Num. of vertices - original: " << obj.m_n_num_vertices_original << "\n";
        os << "Num. of edges - reduced: " << obj.m_n_num_edges_reduced << "\n";
        os << "Num. of vertices - reduced: " << obj.m_n_num_vertices_reduced << "\n";
        os << "Dim. of cycle basis - original: " << obj.m_n_dim_cycle_basis_original << "\n";
        os << "Dim. of cycle basis - reduced: " << obj.m_n_dim_cycle_basis_reduced << "\n";
        os << "Num. of calculated cycles: " << obj.m_n_num_calculated_cycles << "\n";
        os << "Num. of isometric circuits: " << obj.m_n_isometric_circuits << "\n";
        os << "Num. of implicit update: " << obj.m_n_implicit_update << "\n";
        os << "Num. of explicit update: " << obj.m_n_explicit_update << "\n";
        os << "Num. of implicit update with no witness update: " << obj.m_n_num_implicit_no_witness_update << "\n";
        os << "Num. of rejection by no witness found: " << obj.m_n_num_no_witness_found << "\n";
        os << "Num. of rejection by useless edges: " << obj.m_n_num_useless_edges << "\n";
        os << "\nMCB " << (obj.m_n_dim_cycle_basis_original == obj.m_n_num_calculated_cycles ? "Succeed. \n" : "Failed. \n");
        os << "\n";
	return os;
    }

};



class MinimumCycleBasis

{

private:

    Graph m_graph;

    std::vector< std::vector< std::pair<int, bool> > > m_v_cycle_basis;

    TimingStatistics m_time;

    AlgorithmStatistics m_alg_statistics;

    bool m_b_verbose_print;

public:


    MinimumCycleBasis()
            :m_b_verbose_print(false)
    {}


    MinimumCycleBasis(int n_num_nodes, int n_num_edges)
            :m_graph(n_num_nodes, n_num_edges), m_b_verbose_print(false)
    {}


    inline bool addEdge (int t_n_id, int t_n_source, int t_n_target, int t_n_weight = 1)
    {
        return m_graph.addEdge(t_n_id, t_n_source, t_n_target, t_n_weight);
    }


    inline bool addNode(int t_n_id)
    {
        return m_graph.addNode(t_n_id);
    }


    inline const std::vector< std::vector< std::pair<int, bool> > > & getCycleBasis () const
    {
        return m_v_cycle_basis;
    }


    void setVerbosePrint (bool b_flag_enable_print)
    {
        m_b_verbose_print = b_flag_enable_print;
    }



    int run ()
    {
        Timer T;

//        printf("\nOriginal Graph Size: Nodes %d, Edges %d\n", m_graph.n_num_nodes(), m_graph.n_num_edges());

        m_alg_statistics.m_n_num_edges_original = m_graph.n_num_edges();
        m_alg_statistics.m_n_num_vertices_original = m_graph.n_num_nodes();
        m_alg_statistics.m_n_dim_cycle_basis_original = m_graph.n_num_edges() - m_graph.n_num_nodes() + 1;


        T.tic();

#ifdef MCB_USE_COMPRESSED_GRAPH_BY_ELIMINATING_DEGREE_TWO_VERTICES

        /**@brief compress degree-2 nodes in graph */
        int status = m_graph.reduceGraph();
        assert(status == 0 && "Reducing nodes of degree two failed. @Graph::reduceGraph()");
	if (status != 0) return -1;

#else

        m_graph.activateAllNodes();

#endif

        if (m_b_verbose_print)
            m_graph.printGraph(std::cout);


        m_time.m_graph_compression = T.toc();


        T.tic();
        /**@brief compact graph class for shortest-paths */
        ShortestPaths c_compact_graph (&m_graph);

        m_time.m_compact_graph_construction = T.toc();

        if (m_b_verbose_print)
            c_compact_graph.printGraph(std::cout);

//        printf("\nReduced Graph Size: Nodes %d, Edges %d.\n\n\n", c_compact_graph.n_num_nodes(), c_compact_graph.n_num_edges());

        m_alg_statistics.m_n_num_edges_reduced = c_compact_graph.n_num_edges();
        m_alg_statistics.m_n_num_vertices_reduced = c_compact_graph.n_num_nodes();
        m_alg_statistics.m_n_dim_cycle_basis_reduced = c_compact_graph.n_num_edges() - c_compact_graph.n_num_nodes() + 1;


        T.tic();

        c_compact_graph.computeAllPairsShortestPaths();

        m_time.m_shortest_paths = T.toc();

        if (m_b_verbose_print)
            c_compact_graph.printShortestPathTree(std::cout);//


        T.tic();
        /**@brief generate isometric cycles */
        IsometricCycles c_isometric_cycle (&c_compact_graph);

        c_isometric_cycle.computeIsometricCycles(); /// passing m_v_cycle_basis in here?

        m_time.m_isometric_cycle_set = T.toc();

        if (m_b_verbose_print)
            c_isometric_cycle.printIsometricCycles(std::cout);

        m_alg_statistics.m_n_isometric_circuits = c_isometric_cycle.getIsometricCycles().size();


        T.tic();


        /**@brief AICE class */
        AdaptiveIsometricCyclesExtraction c_AICE (&c_isometric_cycle); /// m_v_cycle_basis, and c_compact_graph ?

        c_AICE.main_AICE();

        m_time.m_adaptive_isometric_cycle_extraction = T.toc();


        if (m_b_verbose_print)
           c_AICE.printCycleBases(std::cout);





        m_alg_statistics.m_n_num_calculated_cycles = c_AICE.getCycleBasis().size();
        m_alg_statistics.m_n_implicit_update = c_AICE.n_num_implicit_update();
        m_alg_statistics.m_n_explicit_update = c_AICE.n_num_explicit_update();
        m_alg_statistics.m_n_num_implicit_no_witness_update = c_AICE.n_num_implicit_no_witness_update();
        m_alg_statistics.m_n_num_no_witness_found = c_AICE.n_num_no_witness_found();
        m_alg_statistics.m_n_num_useless_edges = c_AICE.n_num_useless_edges();



        T.tic();

        /**@brief recover the edge ids, and lift the cycles with orientations. */
        m_v_cycle_basis.resize( c_AICE.getCycleBasis().size() );

        IsometricCycles::TyCycleContentContainer & ref_cycle_basis = c_AICE.getCycleBasis();

        auto mcb_it = m_v_cycle_basis.begin();

        for (auto it = ref_cycle_basis.begin(), end = ref_cycle_basis.end(); it != end; ++it)
        {
            std::vector< std::pair<int, bool> > v_cycle;

            std::vector<int> & ref_edges = (*it).m_edges;


            /** @brief initialize start node for the traversal */
            Graph::Edge & e0 = m_graph.getEdge( c_compact_graph.mapEdgeIndex( ref_edges[0] ) );

            int cur_node_id = e0.m_n_end_node_id[0]; // initialize cur_node_id;

            if (ref_edges.size() > 2) // 1 and 2 edges, are self-loop and multiple-edges. They can be explained in any direction.
            {
                Graph::Edge & e1 = m_graph.getEdge( c_compact_graph.mapEdgeIndex( ref_edges[1] ) );
                if (cur_node_id == e1.m_n_end_node_id[0] || cur_node_id == e1.m_n_end_node_id[1])
                    cur_node_id = e0.m_n_end_node_id[1];
            }

            /**@remark Maybe we should do this at IsometricCycles, collecting all cycles with an orientation. */

            /**@brief traverse the cycle, recover the ear-edge with the corresponding path, and lift the cycle with edge orientations */
            for (int idx = 0, ref_edges_size = ref_edges.size(); idx < ref_edges_size; ++idx)
            {
//                printf("\ncopy edge %d in %d\n", idx, ref_edges_size);

                int edge_id = c_compact_graph.mapEdgeIndex( ref_edges[idx] );

                Graph::Edge & edge = m_graph.getEdge(edge_id);


                // if this is not a ear-edge
                if(edge.m_n_ear_edge_id[0] == -1 && edge.m_n_ear_edge_id[1] == -1)
                {
//                    printf("Not ear edge\t");
                    bool traverse_dir = (cur_node_id == edge.m_n_end_node_id[0]);

                    cur_node_id = traverse_dir ? edge.m_n_end_node_id[1] : edge.m_n_end_node_id[0];

                    v_cycle.emplace_back(std::make_pair(edge_id, traverse_dir)); /// check speed.
                }
                else  // this is an ear-edge.
                {
//                    printf("\ndetect ear edge [%d -> %d] by <%d, %d> {%d} \t",
//                           edge.m_n_end_node_id[0], edge.m_n_end_node_id[1], edge.m_n_ear_edge_id[0], edge.m_n_ear_edge_id[1], edge.m_n_weight);

                    bool traverse_dir = (cur_node_id == edge.m_n_end_node_id[0]); // source -> target

                    if (!traverse_dir) cur_node_id = edge.m_n_end_node_id[1];

                    int cur_edge_id = traverse_dir ? edge.m_n_ear_edge_id[0] : edge.m_n_ear_edge_id[1];

                    int dest_node_id = traverse_dir ? edge.m_n_end_node_id[1] : edge.m_n_end_node_id[0];

//                    printf("\tdest_node_id: %d\n", dest_node_id);

                    /**@brief find all edges on the path. */
                    Graph::Edge & ee = m_graph.getEdge(cur_edge_id);

                    bool orientation = ( ee.m_n_end_node_id[0] == cur_node_id );

                    v_cycle.emplace_back(std::make_pair(cur_edge_id, orientation)); /// check speed


//                    printf("[%d]\t", cur_node_id);
//                    if(orientation)
//                        printf(" (%d)[%d => %d] \n", ee.m_n_id, ee.m_n_end_node_id[0], ee.m_n_end_node_id[1]);
//                    else
//                        printf(" (%d)[%d => %d] \n", ee.m_n_id, ee.m_n_end_node_id[1], ee.m_n_end_node_id[0]);


                    cur_node_id = orientation ? ee.m_n_end_node_id[1] : ee.m_n_end_node_id[0];


                    while (cur_node_id != dest_node_id)
                    {
                        Graph::Node & n = m_graph.getNode(cur_node_id);

                        cur_edge_id = ( cur_edge_id == n.m_v_p_adjacent_lists[0] ) ? n.m_v_p_adjacent_lists[1] : n.m_v_p_adjacent_lists[0];

                        Graph::Edge & e = m_graph.getEdge(cur_edge_id);

                        orientation = (e.m_n_end_node_id[0] == cur_node_id);


//                        printf("[%d]\t", cur_node_id);
//                        if(orientation)
//                            printf(" (%d)[%d => %d] \n", e.m_n_id, e.m_n_end_node_id[0], e.m_n_end_node_id[1]);
//                        else
//                            printf(" (%d)[%d => %d] \n", e.m_n_id, e.m_n_end_node_id[1], e.m_n_end_node_id[0]);



                        v_cycle.emplace_back(std::make_pair(cur_edge_id, orientation)); /// check speed

                        cur_node_id = orientation ? e.m_n_end_node_id[1] : e.m_n_end_node_id[0];
                    }

//                    printf("ear edge traversed#\n");
                }
            }

            // assign content of cycle to container.
            (*mcb_it).swap(v_cycle); ++mcb_it;
        }

        m_time.m_edge_recovery_and_lifting = T.toc();


	    return 0;
	
    }


    void print (std::ostream & os = std::cout) const
    {
        os << "\n\n---- Minimum Cycle Basis ----\n\n";
        for(auto it = this->m_v_cycle_basis.begin(), end = this->m_v_cycle_basis.end(); it != end; ++it)
        {
            const std::vector<std::pair<int, bool>> & r_cycle = *it;
            for (int i = 0, n = r_cycle.size(); i < n; ++i)
                os << r_cycle[i].first << "<" <<  m_graph.getEdge(r_cycle[i].first).m_n_end_node_id[0] << ", "
		  << m_graph.getEdge(r_cycle[i].first).m_n_end_node_id[1] << ">(" << ( r_cycle[i].second ? "+1" : "-1" ) << ") ";
            os << "\n";
        }
    }


    friend std::ostream & operator << (std::ostream & os, const MinimumCycleBasis & mcb)
    {

        os << "\n\n---- Minimum Cycle Basis Statistics ----\n";
        os << mcb.m_time;
        os << mcb.m_alg_statistics;

        return os;
    }


};






#endif //MINIMUMCYCLEBASIS_MINIMUMCYCLEBASIS_H
