//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_APSP_H
#define MINIMUMCYCLEBASIS_APSP_H


/**@brief for a compact adjacent list representation */
//#define MCB_BFS_BASED_COMPRESSION


/**@brief Rouhgly Timing: Fibinacci_Heap = BST = 2 * Priority_queue
 * So it would be better to stick to stl::priority_queue, namely the binary heap with contiguous memory.*/

#ifndef MCB_PRIORITY_QUEUE_BASED_IMPLEMENTATION
#define MCB_PRIORITY_QUEUE_BASED_IMPLEMENTATION
#endif

// below options are not recommended.
//#define MCB_FIBINACCI_HEAP_BASED_IMPLEMENTATION
//#define MCB_BINARY_SEARCH_TREE_BASED_IMPLEMENTATION


/**@brief Flag to enable LexDijkstra.
 * Otherwise: The code implements the method in the classical paper:
 * "The All-Pairs Min Cut Problem and the Minimum Cycle Basis Problem on Planar Graphs", by David Hartvigsen and Russell Mardon 1993
 * Read More: https://epubs.siam.org/doi/abs/10.1137/S0895480190177042
 * @note LexDijkstra performs better since it avoids the sorting
 * and the heavy random access in post-processing */
//#define MCB_LEX_DIJKSTRA_IMPLEMENTATION



/**@brief It seems no timing improvement by reusing previous calculated shortest path trees.
 * Actually it is slightly slower because of the additional traversal and mememroy access. */
//#define MCB_APSP_REUSE_SHORTEST_PATHS


#include <unordered_map>   /// used for node ID mapping from Graph->CompactGraph
#include <set>

#include <limits.h>   /// INT_MAX

#include <assert.h>

#include "PriorityQueue.h"

#include "Graph.h"

#include <fstream>



/** @brief forward declarationof a fixed size block memory allocator */

/** @brief A simple implementation of a static fixed size block memory allocator */
template <class TAdjItem, class TAdjList, size_t N >
class MemAllocator
{

private:

    int m_n_capacity;

    char * m_buffer;

    char * m_top;


public:

    MemAllocator ()
        :m_n_capacity(0), m_buffer(nullptr), m_top(nullptr)
    {}


    /**@brief initialize a block of memory of n_size bytes*/
    MemAllocator (int n_size)
    {
        m_n_capacity = n_size;
        m_buffer = (char *) malloc (m_n_capacity);
        m_top = m_buffer;
        if(m_buffer == nullptr)
        {
            std::cerr << "\nBlock Memory allocation failure!\n";
            exit(1);
        }
    }


    /**@brief a specialized constructor for CompactGraph and ShortestPahts */
    MemAllocator (int n_num_nodes, int n_num_edges)
    {
        int adj_list_size = 2 * n_num_edges * sizeof(TAdjItem) + n_num_nodes * sizeof(TAdjList);
        int spt_size = n_num_nodes * n_num_nodes * sizeof(int) * N + n_num_nodes * n_num_nodes * sizeof(bool);
        m_n_capacity = adj_list_size + spt_size;
        m_buffer = (char *) malloc (m_n_capacity);
        m_top = m_buffer;
        if(m_buffer == nullptr)
        {
            std::cerr << "\nBlock Memory allocation failure!\n";
            exit(1);
        }
    }



    /**@brief initialize a block of memory of n_size bytes*/
    char * InitMemSize (int n_size)
    {
        m_n_capacity = n_size;
        m_buffer = (char *) malloc (m_n_capacity);
        m_top = m_buffer;
        // check nullptr for allocation failure.
        return m_top;
    }


    /**@brief template function to allocate space for TyItem * n_size.
     * @return pointer to the beginning of the allocated memory block. */

    template <class TyItem>
    TyItem * MemAlloc (int n_size)
    {
        // if there is no sufficient memory available
        if (m_top - m_buffer + sizeof(TyItem) * n_size > (size_t) m_n_capacity)
            return nullptr;
        // allocate memory from buffer
        TyItem * pos = (TyItem*) m_top;
        m_top += sizeof(TyItem) * n_size;
        return pos;
    }


    /**@return full capacity of the reserved memory in bytes */
    inline int Capacity () { return m_n_capacity; }


    /**@return size of used memory in bytes */
    inline int Size () { return m_top - m_buffer; }


    ~ MemAllocator() { free(m_buffer); }
};



/**@brief A container to collect two dimensional data */

template < class TyEntry >
class TwoDimBlockContainer
{
private:

    const int m_n_dimension;

    TyEntry * m_buffer;   /// the buffer for links of N shortest path tree;

private:
    /**@brief disable copy constructor and assignment operator. avoid shallow copy*/
    TwoDimBlockContainer(const TwoDimBlockContainer &) = delete;

    TwoDimBlockContainer & operator= (const TwoDimBlockContainer &);


public:

    template <class T1, class T2, size_t N >
    TwoDimBlockContainer(int n_dimension, MemAllocator<T1, T2, N> & mem_allocator)
            :m_n_dimension(n_dimension)
    {
        m_buffer = mem_allocator. template MemAlloc<TyEntry> (m_n_dimension * m_n_dimension);

        if(m_buffer == nullptr)
        {
            std::cerr << "\nMemory allocation error for TwoDimBlockContainer!\n";
            exit(1);
        }
    }


    inline int n_dimension ()
    {
        return m_n_dimension;
    }


    inline TyEntry & operator () (int n_row_id, int n_col_id)
    {
        return m_buffer[m_n_dimension * n_row_id + n_col_id];
    }


    inline const TyEntry & operator () (int n_row_id, int n_col_id) const
    {
        return m_buffer[m_n_dimension * n_row_id + n_col_id];
    }


    inline TyEntry * operator[] (int n_row_id)
    {
        return (m_buffer + m_n_dimension * n_row_id);
    }


    inline const TyEntry * operator[] (int n_row_id) const
    {
        return (m_buffer + m_n_dimension * n_row_id);
    }


    ~TwoDimBlockContainer()
    {
    //    free( m_buffer );
    }
};





/**@brief Adjacent List Container Implementation of the Compact Graph */


template < class TAdjItem >
class AdjListContainer
{

public:

    class AdjList
    {

    public:

        typedef const TAdjItem * iterator;

        AdjList(const TAdjItem * ptr_begin, const TAdjItem * ptr_end)
                : m_begin(ptr_begin), m_end (ptr_end)
        {}

        const AdjList & operator = (const AdjList & rhs)
        {
            m_begin = rhs.m_begin;  m_end = rhs.m_end;
	    return *this;
        }

        inline iterator begin() const { return  m_begin; };

        inline iterator end() const { return  m_end; };

        inline int size () const { return m_end - m_begin; };


    private:

        const TAdjItem *  m_begin;

        const TAdjItem *  m_end;

    };


    typedef AdjList TyAdjList;

public:

    template <class T1, class T2, size_t N >
    AdjListContainer(int n_num_nodes, int n_num_edges, MemAllocator<T1, T2, N> & mem_allocator)
            : m_n_num_nodes(n_num_nodes),
              m_n_num_edges(n_num_edges)
    {
//        m_pool_buffer = (char*) malloc ( 2 * m_n_num_edges * sizeof(TAdjItem) + m_n_num_nodes * sizeof(TyAdjList) );
//
//        m_AdjListPool = (TyAdjList*) ( m_pool_buffer );

//        m_AdjListItemPool = (TAdjItem*) ( m_pool_buffer + m_n_num_nodes * sizeof(TyAdjList) );


        m_AdjListPool = mem_allocator. template MemAlloc<TyAdjList> ( m_n_num_nodes );

        m_AdjListItemPool = mem_allocator. template MemAlloc<TAdjItem> ( 2 * m_n_num_edges );

//        printf("\nmem_allocator.size() = %d, mem_allocator.Capacity() = %d\n", mem_allocator.Size(), mem_allocator.Capacity());


        if(m_AdjListPool == nullptr || m_AdjListItemPool == nullptr)
        {
            std::cerr << "\nMemory allocation error for AdjListContainer!\n";
            exit(1);
        }


        m_item_pool_top = m_AdjListItemPool;

        m_list_pool_top = m_AdjListPool;

    }


    inline int size () const
    {
        return m_n_num_nodes;
    }


    inline void push (const TAdjItem & item)
    {
        *m_item_pool_top = item;
        ++m_item_pool_top;
    }


    inline void push (const TyAdjList & list)
    {
        *m_list_pool_top = list;
        ++m_list_pool_top;
    }

    inline const TyAdjList & operator[] (int node_id) const
    {
        return m_AdjListPool[node_id];
    }


    inline const TAdjItem * top_item () const
    {
        return m_item_pool_top;
    }


    inline const TAdjItem * top_list () const
    {
        return m_list_pool_top;
    }


    ~AdjListContainer()
    {
//        free(m_pool_buffer);
    }


private:

    int m_n_num_nodes;

    int m_n_num_edges;

//    char * m_pool_buffer;

    TAdjItem * m_AdjListItemPool;

    TyAdjList * m_AdjListPool;

    TAdjItem * m_item_pool_top;

    TyAdjList * m_list_pool_top;


};






/**@brief A compact graph class built from a given graph */
class CompactGraph
{


public:


    typedef struct AdjListItem
    {
        int m_n_target_node_id;

        int m_n_edge_id;

        int m_n_weight;

    } TyAdjListItem;


    typedef struct CptEdge
    {
        int m_n_source;

        int m_n_target;

        int m_n_weight;

    } TyCptEdge;



    typedef AdjListContainer< TyAdjListItem > TyAdjListContainer;

    typedef std::vector<TyCptEdge> TyCompactEdgeContainer;

    typedef TyAdjListContainer::TyAdjList TyAdjList;


#ifdef MCB_LEX_DIJKSTRA_IMPLEMENTATION

    typedef MemAllocator<TyAdjListItem, TyAdjList, 4> TyMemAllocator;

#else

    typedef MemAllocator<TyAdjListItem, TyAdjList, 6> TyMemAllocator;

#endif



protected:

    Graph * m_p_graph;

    int m_n_num_nodes;

    int m_n_num_edges;


    TyMemAllocator m_mem_allocator;

    TyAdjListContainer m_v_adj_list_container;

    TyCompactEdgeContainer m_v_compact_edge_container;


    std::vector<int> m_v_active_nodes_id_mapping;   /// node id container, act as mapping
    std::vector<int> m_v_active_nodes_id_invmapping;

    std::vector<int> m_v_active_edges_id_mapping;
    std::vector<int> m_v_active_edges_id_invmapping;



private:  // disable copy constructor and assignment operator

    CompactGraph (const CompactGraph &);

    CompactGraph & operator= (const CompactGraph &);


public:

    // initialize a compact graph representation from a graph after running compression algorithms.
    CompactGraph (Graph * t_graph)
            : m_p_graph(t_graph),
              m_n_num_nodes(t_graph->n_num_active_nodes()),
              m_n_num_edges(t_graph->n_num_active_edges()),
              m_mem_allocator(m_n_num_nodes, m_n_num_edges),
              m_v_adj_list_container(m_n_num_nodes, m_n_num_edges, m_mem_allocator),
              m_v_compact_edge_container(m_n_num_edges),
              m_v_active_nodes_id_mapping(m_n_num_nodes),
              m_v_active_nodes_id_invmapping(t_graph->n_num_nodes(), -1),
              m_v_active_edges_id_mapping(m_n_num_edges),
              m_v_active_edges_id_invmapping(t_graph->n_num_edges(), -1)
    {



#ifdef MCB_BFS_BASED_COMPRESSION



        /** USE BFS based vertices sequence. */

        int n_node_cnt = 0;
        int n_edge_cnt = 0;
        int n_buffer_item_cnt = 0;


        bool *visited_node = new bool[m_p_graph->n_num_nodes()]();
        bool *visited_edge = new bool[m_p_graph->n_num_edges()]();


        std::list<int> queue;

        int vst_id = m_p_graph->n_dfs_root_node_id();


        assert(vst_id >=0 && "starting node id for BFS/DFS is negative.");


        visited_node[vst_id] = true;
        m_v_active_nodes_id_mapping[n_node_cnt] = vst_id;
        m_v_active_nodes_id_invmapping[vst_id] = n_node_cnt;
        ++n_node_cnt;


        queue.push_back(vst_id);

        while(!queue.empty())
        {
            vst_id = queue.front();
            queue.pop_front();

            Graph::Node & node = m_p_graph->getNode(vst_id);

            const TyAdjListItem * ptr_start_pos = m_v_adj_list_container.top_item();

            for (auto t_edge_id : node.m_v_p_adjacent_lists)
            {
                Graph::Edge & edge = m_p_graph->getEdge(t_edge_id);

                if (edge.m_b_active)
                {
                    int target_node_id = m_p_graph->otherNode(edge, vst_id);  /// this node is active definitely.

                    if( !visited_node[target_node_id])
                    {
                        m_v_active_nodes_id_mapping[n_node_cnt] = target_node_id;
                        m_v_active_nodes_id_invmapping[target_node_id] = n_node_cnt;
                        ++n_node_cnt;

                        visited_node[target_node_id] = true;
                        queue.push_back(target_node_id);
                    }

                    if ( !visited_edge[edge.m_n_id])
                    {
                        m_v_active_edges_id_mapping[n_edge_cnt] = edge.m_n_id;
                        m_v_active_edges_id_invmapping[edge.m_n_id] = n_edge_cnt;
                        /// no need to change edge ID, as far as I see.
                        /// But it is useful to save a collection of active edges for building Horton set.
                        TyCptEdge & active_edge = m_v_compact_edge_container[n_edge_cnt];
                        active_edge.m_n_source = m_v_active_nodes_id_invmapping[ edge.m_n_end_node_id[0] ];
                        active_edge.m_n_target = m_v_active_nodes_id_invmapping[ edge.m_n_end_node_id[1] ];
                        active_edge.m_n_weight = edge.m_n_weight;
                        ++n_edge_cnt;

                        visited_edge[edge.m_n_id] = true;

                        if(active_edge.m_n_source == active_edge.m_n_target)
                            printf("\ndetect a self-loop\n");
                    }


                    int tmp_node_id = m_v_active_nodes_id_invmapping[target_node_id];
                    int tmp_edge_id = m_v_active_edges_id_invmapping[ edge.m_n_id ];

                    m_v_adj_list_container.push( TyAdjListItem{tmp_node_id, tmp_edge_id, edge.m_n_weight} );

                    ++n_buffer_item_cnt;

                }
            }

            m_v_adj_list_container.push(TyAdjList{ptr_start_pos, m_v_adj_list_container.top_item()});

        }

        delete []visited_node;
        delete []visited_edge;


        if(n_node_cnt != m_n_num_nodes)
        {
            printf("\nError: Counted active node number(%d) != saved active num_nodes (%d) \n", n_node_cnt, m_n_num_nodes);
            exit(1);
        }
        if(n_edge_cnt != m_n_num_edges)
        {
            printf("\nNote: Counted active node number(%d) && saved active num_nodes (%d) \n", n_node_cnt, m_n_num_nodes);
            printf("\nError: Counted active edge number (%d) != saved active num_edges (%d)\n", n_edge_cnt, m_n_num_edges);
            exit(1);
        }
        if (n_buffer_item_cnt != 2 * m_n_num_edges)
        {
            printf("\nError: size of m_AdjListPool (%d) != 2 * num_edges (%d) \n", n_buffer_item_cnt, m_n_num_edges);
            exit(1);
        }




#else



        /**@brief compression by simply seraching container sequentially */


        int n_node_cnt = 0;


        for (int i = 0, m = t_graph->n_num_nodes(); i < m; ++i)
        {
            Graph::Node & node = t_graph->getNode(i);
            if (node.m_b_active)
            {
                m_v_active_nodes_id_mapping[n_node_cnt] = node.m_n_id;
                m_v_active_nodes_id_invmapping[node.m_n_id] = n_node_cnt;
                ++n_node_cnt;
            }
        }

        if(n_node_cnt != m_n_num_nodes)
        {
            printf("\nError: Counted active node number(%d) != saved active num_nodes (%d) \n", n_node_cnt, m_n_num_nodes);
            exit(1);
        }


        int n_edge_cnt = 0;

        for (int i = 0, m = t_graph->n_num_edges(); i < m; ++i) {
            Graph::Edge &edge = t_graph->getEdge(i);
            if (edge.m_b_active) {
                m_v_active_edges_id_mapping[n_edge_cnt] = edge.m_n_id;
                m_v_active_edges_id_invmapping[edge.m_n_id] = n_edge_cnt;
                /// no need to change edge ID, as far as I see.
                /// But it is useful to save a collection of active edges for building Horton set.
                TyCptEdge & active_edge = m_v_compact_edge_container[n_edge_cnt];
                active_edge.m_n_source = m_v_active_nodes_id_invmapping[ edge.m_n_end_node_id[0] ];
                active_edge.m_n_target = m_v_active_nodes_id_invmapping[ edge.m_n_end_node_id[1] ];
                active_edge.m_n_weight = edge.m_n_weight;
                ++n_edge_cnt;

                if(active_edge.m_n_source == active_edge.m_n_target)
                    printf("\ndetect a self-loop\n");
            }
        }


        if(n_edge_cnt != m_n_num_edges)
        {
            printf("\nNote: Counted active node number(%d) && saved active num_nodes (%d) \n", n_node_cnt, m_n_num_nodes);
            printf("\nError: Counted active edge number (%d) != saved active num_edges (%d)\n", n_edge_cnt, m_n_num_edges);
            exit(1);
        }



        int n_buffer_item_cnt = 0;


        // traverse all active nodes to build adjacent list.
        for (int i = 0; i < m_n_num_nodes; ++i)
        {
            int t_node_id = m_v_active_nodes_id_mapping[i];

            Graph::Node & node = t_graph->getNode( t_node_id );

            const TyAdjListItem * ptr_start_pos = m_v_adj_list_container.top_item();

            for (auto t_edge_id : node.m_v_p_adjacent_lists)
            {
                int compact_edge_id = m_v_active_edges_id_invmapping[ t_edge_id ];

                if (compact_edge_id != -1)  /// an acitve edge
                {
                    TyCptEdge & compact_edge = m_v_compact_edge_container[ compact_edge_id ];

                    int tmp_node_id = (compact_edge.m_n_source == i ? compact_edge.m_n_target : compact_edge.m_n_source);

                    m_v_adj_list_container.push( TyAdjListItem{tmp_node_id, compact_edge_id, compact_edge.m_n_weight} );

                    ++n_buffer_item_cnt;
                }
            }

            m_v_adj_list_container.push(TyAdjList{ptr_start_pos, m_v_adj_list_container.top_item()});

        }

        if (n_buffer_item_cnt != 2 * m_n_num_edges)
        {
            printf("\nError: size of m_AdjListPool (%d) != 2 * num_edges (%d) \n", n_buffer_item_cnt, m_n_num_edges);
            exit(1);
        }



#endif


#ifdef MCB_DEBUG_GRAPH_COMPRESSION

        int n_cnt_bridge_edges = 0;
        int n_cnt_self_loops = 0;

        printf("\nTest Num. of bridges: \n");
        for (size_t i = 0; i < m_v_compact_edge_container.size(); ++i)
        {
            const TyCptEdge & e = m_v_compact_edge_container[i];
            if(e.m_n_source == e.m_n_target)
                ++n_cnt_self_loops;

            const TyAdjList & adj_s = m_v_adj_list_container[e.m_n_source];
            const TyAdjList & adj_t = m_v_adj_list_container[e.m_n_target];
            if(adj_s.size() == 1 || adj_t.size() == 1)
                ++n_cnt_bridge_edges;

        }
        printf("\n\tNum. of bridges by edge traversal: %d \n", n_cnt_bridge_edges);

        int n_cnt_bridge_nodes = 0;
        for (int i= 0; i < m_v_adj_list_container.size(); ++i)
        {
            if (m_v_adj_list_container[i].size() == 1)
                ++n_cnt_bridge_nodes;
        }
        printf("\n\tNum. of bridges by node traversal: %d \n", n_cnt_bridge_nodes);


        printf("\nTest Num. of self-loops: %d \n", n_cnt_self_loops);

#endif

    }






    inline int n_num_nodes ()
    {
        return m_n_num_nodes;
    }


    inline int n_num_edges ()
    {
        return m_n_num_edges;
    }


    inline const TyAdjList & getAdjList (int node_id) const
    {
        return  m_v_adj_list_container[node_id];
    }


    /**@brief An interface function for Horton & Isometric cycle construction*/
    inline TyCptEdge & getEdge (int n_compact_edge_index)
    {
        return m_v_compact_edge_container[ n_compact_edge_index ];
    }

    /**@brief An interface function for Horton & Isometric cycle construction*/
    inline const TyCptEdge & getEdge (int n_compact_edge_index) const
    {
        return m_v_compact_edge_container[ n_compact_edge_index ];
    }


    inline int mapEdgeIndex (int n_edge_id) const
    {
        return m_v_active_edges_id_mapping[n_edge_id];
    }

    inline int mapNodeIndex (int n_node_id) const
    {
        return m_v_active_nodes_id_mapping[n_node_id];
    }


    /**@brief a test function used to reorganize the edges */
    void sortCptEdges ()
    {
        class CptEdgeLessFunctor
        {
        public:
            bool operator() (const TyCptEdge & a, const TyCptEdge & b) const
            {
                return (a.m_n_source + a.m_n_target) < (b.m_n_source + b.m_n_target);
            }
        }CptEdgeLess;

        std::sort(m_v_compact_edge_container.begin(), m_v_compact_edge_container.end(), CptEdgeLess);
    }


    void printGraph(std::ostream & os)
    {
        os << "\nCompactGraph Info.\n"
           << "\tNum. of Nodes: " << m_v_active_nodes_id_mapping.size() << "\n"
           << "\tNum. of Edges: " << m_v_active_edges_id_mapping.size() << "\n";

        for (int n_id = 0; n_id < m_n_num_nodes; ++n_id)
        {
            const TyAdjList & adj = this->getAdjList(n_id);

            os << "\nNode [" << n_id << "] " << "\n";
            os << "\tAdjacent Edges: \n";

            for (TyAdjList::iterator it = adj.begin(); it != adj.end(); ++it)
            {
                os << "\t\tedgeID: " << it->m_n_edge_id
                   << "\tweight " << it->m_n_weight
                   << "\ttarget " << it->m_n_target_node_id
                   << "\n";
            }
        }
        os << "\nEND#\n\n";
    }



    ~CompactGraph()
    {}


};




/**@brief A compact graph class, simplified for faster shortest path algorithms */
class ShortestPaths : public CompactGraph
{

public:

    static const int m_n_max_int = INT_MAX;


    typedef struct ComplexWeight
    {
        int m_n_distance;   /// distance used for cycle weight

        int m_n_length;    /// length of the path, used for cycle length.

    } TyComplexWeight;


    typedef struct SptIndex
    {
        int m_n_edge_id;   /// edge id, used to distinguish multiple-edges

        int m_n_parrent_node_id;   /// link to parrent node

    } TySptIndex;




    typedef TwoDimBlockContainer <TyComplexWeight> TySptWeightContainer;

    typedef TwoDimBlockContainer <TySptIndex> TySptIndexContainer;

    typedef TwoDimBlockContainer <bool> TySptFlagContainer;


#ifndef MCB_LEX_DIJKSTRA_IMPLEMENTATION

    typedef struct LexInfo
    {
        int m_n_min_index;

        int m_n_root_edge;

    } TyLexInfo;


    typedef TwoDimBlockContainer <TyLexInfo> TyLexInfoContainer;


    /**@brief simple data-structure used for sorting nodes paris by weights */

    typedef struct LexDistLink
    {
        int m_n_distance;
        int m_n_length;
        int m_n_source;
        int m_n_target;

        inline bool operator < (const LexDistLink & other) const
        {
            if (m_n_distance < other.m_n_distance)
                return true;
            else if (m_n_distance == other.m_n_distance)
            {
                if(m_n_length < other.m_n_length)
                    return true;
                return false;
            }
            return false;
        }

    } TyLexDistLink;

#endif



private:


    TySptWeightContainer m_v_spt_weight_container;

    TySptIndexContainer m_v_spt_index_container;

    TySptFlagContainer m_v_spt_flag_container;

#ifndef MCB_LEX_DIJKSTRA_IMPLEMENTATION
    TyLexInfoContainer m_v_lex_info_container;
#endif

    std::vector <int> m_v_apsp_sequence;


#ifdef DEBUG_APSP

    private:

    std::ofstream m_ofs;

    int m_n_dijkstra_cmp_weight;

    int m_n_dijkstra_cmp_length;

    int m_n_dijkstra_cmp_min_id;

#endif

public:

    ShortestPaths(Graph * t_graph)
            : CompactGraph (t_graph),
              m_v_spt_weight_container(this->n_num_nodes(), this->m_mem_allocator),
              m_v_spt_index_container(this->n_num_nodes(), this->m_mem_allocator),
              m_v_spt_flag_container(this->n_num_nodes(), this->m_mem_allocator),
#ifndef MCB_LEX_DIJKSTRA_IMPLEMENTATION
              m_v_lex_info_container(this->n_num_nodes(), this->m_mem_allocator),
#endif
              m_v_apsp_sequence(0)
    {

#ifdef DEBUG_APSP
        m_ofs.open ("terminal.txt", std::ofstream::out | std::ofstream::app);
        m_n_dijkstra_cmp_weight = 0;
        m_n_dijkstra_cmp_length = 0;
        m_n_dijkstra_cmp_min_id = 0;
#endif

    }


#ifdef DEBUG_APSP
    ~ShortestPaths()
    {
        m_ofs.close();
    }
#endif


public:





    void LexDijkstra (int n_node_id)
    {

        int * m_path_1 = (int*) malloc(sizeof(int) * this->n_num_nodes());
        int * m_path_2 = (int*) malloc(sizeof(int) * this->n_num_nodes());


#ifdef MCB_APSP_REUSE_SHORTEST_PATHS
        bool * m_flag = m_v_spt_flag_container[n_node_id];
#endif


        TySptIndex * m_spt = m_v_spt_index_container[n_node_id];

        m_spt[n_node_id] = TySptIndex{-1, -1};


        TyComplexWeight * cur_spt_weight = m_v_spt_weight_container[n_node_id];

        cur_spt_weight[n_node_id] = TyComplexWeight{0, 0};


        pq::TyHeap m_pq_search_frontier; /// define search frontier for each thread, for parallelism

        m_pq_search_frontier.push( pq::Node{n_node_id, 0} );



        while (! m_pq_search_frontier.empty())
        {

            pq::Node cur_node = m_pq_search_frontier.top();
            m_pq_search_frontier.pop();

            // The shortest path to cur_node has been found.


            TyComplexWeight & ref_cur_weight = cur_spt_weight[cur_node.id];


            // this is required because STL::priority_queue does not have decrease_key operation.
            if (cur_node.val > ref_cur_weight.m_n_distance)
                continue;


            /**@biref intialize weight from <target, source> as well to speed up, does not operate on priority queue*/
           //// m_v_spt_weight_container[cur_node.id][n_node_id] = ref_cur_weight;
            /// This is buggy, since priority queque may become empty if no nodes added.


            const TyAdjList & adj = this->getAdjList(cur_node.id);


            for (TyAdjList::iterator it = adj.begin(); it != adj.end(); ++it) {

                int target_node_id = it->m_n_target_node_id;



#ifdef MCB_APSP_REUSE_SHORTEST_PATHS
                /**@brief Dijkstra will searcha all edges exactly once!
                 * There is only one case that the edge_id has been assigned correctly
                 * that is, the shortest-path to this node has been found before!
                 */
                if (m_flag[target_node_id])  /// found = true; These are special nodes.
                {
                    if (m_spt[target_node_id].m_n_edge_id == it->m_n_edge_id) // correct edge, means found.
                    {
                        int t_dist = ref_cur_weight.m_n_distance + it->m_n_weight;
                        int t_len = ref_cur_weight.m_n_length + 1;
                        cur_spt_weight[target_node_id] = TyComplexWeight{t_dist, t_len};
                        m_pq_search_frontier.push(pq::Node{target_node_id, t_dist});
                    }
                    continue;
                }
#endif


                int n_new_distance = ref_cur_weight.m_n_distance + it->m_n_weight;
                int n_new_length = ref_cur_weight.m_n_length + 1;


                TyComplexWeight & ref_target_weight = cur_spt_weight[target_node_id];


                LexComparePaths(target_node_id, n_new_distance, n_new_length,
                                ref_target_weight, m_spt, m_path_1, m_path_2,
                                cur_node.id, it->m_n_edge_id, m_pq_search_frontier);

            }

        }



        /**@brief  Reuse shortest path tree  */

#ifdef MCB_APSP_REUSE_SHORTEST_PATHS
        this->ReuseShortestPathTree ( n_node_id, m_spt );
#endif

        free(m_path_1);
        free(m_path_2);

    }



    inline void ReuseShortestPathTree (int n_node_id, TySptIndex * m_spt)
    {
        // [m_v_deg_cnt]: pos0, deg0; pos1, deg1; pos2, deg2
        std::vector<int> m_v_deg_cnt( 2 * this->n_num_nodes(), 0 );


        // initialize node degree
        for (int i = 0; i < this->n_num_nodes(); ++i)
        {
            int t_p_id = m_spt[i].m_n_parrent_node_id;
            if ( t_p_id != -1 )
                ++m_v_deg_cnt[ 2 * t_p_id + 1];
        }


        // initialize start position
        for (auto iter = m_v_deg_cnt.begin() + 2; iter != m_v_deg_cnt.end(); ++iter, ++iter)
        {
            *iter = *(iter-1) + *(iter-2);
        }


        // the content will be extracted form index structure of shortest-path-tree
        struct ForwardTreeLink
        {
            int m_n_child_id;
            int m_n_edge_id;
        };


        // add data into adjacent list (i.e. a vector)
        ForwardTreeLink* v_adj_list = (ForwardTreeLink*) malloc(sizeof(ForwardTreeLink) * this->n_num_nodes());
        for (int i = 0; i < this->n_num_nodes(); ++i)
        {
            int t_p_id = m_spt[i].m_n_parrent_node_id;
            int t_p_edge = m_spt[i].m_n_edge_id;
            if (t_p_id != -1)
            {
                // forward edge:  [ t_p_id ] ---> [ i ]
                int n_pos = m_v_deg_cnt [2*t_p_id];  // start_pos for node [t_p_id]
                m_v_deg_cnt [2*t_p_id] ++ ;
                v_adj_list[n_pos].m_n_child_id = i;
                v_adj_list[n_pos].m_n_edge_id = t_p_edge;
            }
        }


        for (auto iter = m_v_deg_cnt.begin(); iter != m_v_deg_cnt.end(); ++iter, ++iter)
        {
            *iter -= *(iter+1);
        }


        int t_root_pos = m_v_deg_cnt[2*n_node_id];
        int t_root_end = t_root_pos + m_v_deg_cnt[2*n_node_id+1];


        std::vector<int> v_stack; /// maintain the node id to expand
        while (t_root_pos != t_root_end)
        {
            int n_next_root = v_adj_list[t_root_pos].m_n_child_id;

            // fill in branch
            TySptIndex * next_spt = m_v_spt_index_container[n_next_root];
            bool * next_flag = m_v_spt_flag_container[n_next_root];

            v_stack.push_back(n_next_root);
            next_spt[n_next_root] =TySptIndex{-1, -1};  // edge_id, parrent_id
            next_flag[n_next_root] = true;

            while (!v_stack.empty())
            {
                int n_expand_id = v_stack.back();
                v_stack.pop_back();
                int t_expand_pos = m_v_deg_cnt[2*n_expand_id];
                int t_expand_end = t_expand_pos + m_v_deg_cnt[2*n_expand_id+1];
                while (t_expand_pos != t_expand_end)
                {
                    const ForwardTreeLink & ref_expand_info = v_adj_list[t_expand_pos];
                    next_spt[ref_expand_info.m_n_child_id]= TySptIndex{ref_expand_info.m_n_edge_id, n_expand_id};
                    next_flag[ref_expand_info.m_n_child_id] = true;
                    v_stack.push_back(ref_expand_info.m_n_child_id);
                    ++ t_expand_pos;
                }
            }
            ++t_root_pos;

        }


        if (n_node_id == 0)
        {
            std::list<int> bfs_queue; /// maintain the node id to expand
            bfs_queue.push_back(n_node_id);

            while (!bfs_queue.empty())
            {
                int n_expand_id = bfs_queue.front();
                bfs_queue.pop_front();

                m_v_apsp_sequence.push_back(n_expand_id);

                int t_expand_pos = m_v_deg_cnt[2*n_expand_id];
                int t_expand_end = t_expand_pos + m_v_deg_cnt[2*n_expand_id+1];
                while (t_expand_pos != t_expand_end)
                {
                    int n_child_id = v_adj_list[t_expand_pos].m_n_child_id;
                    bfs_queue.push_back(n_child_id);
                    ++ t_expand_pos;
                }
            }

        }

    }




    template<class T>
    void print_vector (const std::vector<T> & vec)
    {
        for (int i = 0; i < vec.size(); ++i)
        {
            std::cout << vec[i] << " ";
        }
    }


    // 2 - update prioprity queue, and weight matrix
    // 1 - update weight matrix
    // 0 - do nothing
    inline void LexComparePaths(int target_node_id, int n_new_distance, int n_new_length,
                               TyComplexWeight & ref_target_weight, TySptIndex * m_spt,
                               int * m_path_1, int * m_path_2, int cur_node_id, int next_edge_id, pq::TyHeap & pq_heap)
    {

        bool b_update_path = false;

        if ( ref_target_weight.m_n_distance != m_n_max_int )
        {
            if(n_new_distance < ref_target_weight.m_n_distance)
            {
#ifdef DEBUG_APSP
                ++ m_n_dijkstra_cmp_weight; /// compare weight
#endif
                pq_heap.push(pq::Node{target_node_id, n_new_distance});
                b_update_path = true;
            }
            else if (n_new_distance == ref_target_weight.m_n_distance )
            {
                if (n_new_length < ref_target_weight.m_n_length)
                {
#ifdef DEBUG_APSP
                    ++ m_n_dijkstra_cmp_length; /// compare length
#endif
                    b_update_path = true;
                }
#ifdef MCB_LEX_DIJKSTRA_IMPLEMENTATION
                else if (n_new_length == ref_target_weight.m_n_length)
                {
#ifdef DEBUG_APSP
                    ++ m_n_dijkstra_cmp_min_id; /// compare min_id
#endif
                    int * m_pos_path_1 = m_path_1;
                    int * m_pos_path_2 = m_path_2;

                    *m_pos_path_1 = m_spt[target_node_id].m_n_edge_id;  ++m_pos_path_1;
                    int t_parrent_id_1 = m_spt[target_node_id].m_n_parrent_node_id;

                    *m_pos_path_2 = next_edge_id;  ++m_pos_path_2;
                    int t_parrent_id_2 = cur_node_id;

                    // we only need to back-traverse to a common node.
                    while (t_parrent_id_1 != t_parrent_id_2)
                    {

                        *m_pos_path_1 = m_spt[t_parrent_id_1].m_n_edge_id;  ++m_pos_path_1;
                        t_parrent_id_1 = m_spt[t_parrent_id_1].m_n_parrent_node_id;

                        *m_pos_path_2 = m_spt[t_parrent_id_2].m_n_edge_id;  ++m_pos_path_2;
                        t_parrent_id_2 = m_spt[t_parrent_id_2].m_n_parrent_node_id;

                    }

                    std::sort(m_path_1, m_pos_path_1);
                    std::sort(m_path_2, m_pos_path_2);

                    auto it1 = m_path_1;
                    auto it2 = m_path_2;

                    while (it1 != m_pos_path_1 && it2 != m_pos_path_2)
                    {
                        if (*it2 < *it1)
                        {
                            b_update_path = true;
                            break;
                        }
                        else if (*it2 > *it1)
                        {
                            b_update_path = false;
                            break;
                        }
                        else if (*it2 == *it1)
                        {
                            ++it1; ++it2;
                        }
                    }
                }
#endif
            }
        }
        else
        {
            pq_heap.push(pq::Node{target_node_id, n_new_distance});
            b_update_path = true;
        }


        if (b_update_path)
        {
            ref_target_weight = TyComplexWeight{n_new_distance, n_new_length};
            m_spt[target_node_id] = TySptIndex{next_edge_id, cur_node_id};
        }
    }







    void computeAllPairsShortestPaths ()
    {

        Timer T;


        // intialize complex weight matrix.
        TyComplexWeight * tmp_weight_pos = m_v_spt_weight_container[0];
        TyComplexWeight * tmp_end = tmp_weight_pos + m_n_num_nodes * m_n_num_nodes;
        while (tmp_weight_pos != tmp_end)
        {
            (*tmp_weight_pos).m_n_distance = m_n_max_int;
            ++tmp_weight_pos;
        }


//        double f_time_intialize_complex_weight = T.toc();
//
//        printf("Time used by initializing weights in spt tree: %f\n", f_time_intialize_complex_weight);
//
        T.tic();



#ifdef MCB_APSP_REUSE_SHORTEST_PATHS

        this->LexDijkstra(0);

//        print_vector(m_v_apsp_sequence);

#ifdef MCB_USE_OMP_PARALLELIZED_DIJKSTRA
        #pragma omp parallel for
#endif
        for (int i = 1; i < m_n_num_nodes; ++i)
        {
            int n_expand_id = m_v_apsp_sequence[i];
            this->LexDijkstra( n_expand_id );
        }

#else

#ifdef MCB_USE_OMP_PARALLELIZED_DIJKSTRA
        #pragma omp parallel for
#endif
        for (int i = 0; i < m_n_num_nodes; ++i)
        {
            this->LexDijkstra(i);
        }

#endif


#ifdef DEBUG_APSP
        double f_time_dijkstra = T.toc();
        int m_n_dijkstra_cmp_totoal = m_n_dijkstra_cmp_weight + m_n_dijkstra_cmp_length + m_n_dijkstra_cmp_min_id;
        m_ofs << "Time used by APSP (Dijkstra): " << f_time_dijkstra << "\n";
        m_ofs << "\t Dijkstra Update Comparison: " << m_n_dijkstra_cmp_totoal << "\n";
        m_ofs << "\t Dijkstra compare weight: " << m_n_dijkstra_cmp_weight
              << "\tRatio: " << ((double)m_n_dijkstra_cmp_weight / m_n_dijkstra_cmp_totoal) << "\n";
        m_ofs << "\t Dijkstra compare length: " << m_n_dijkstra_cmp_length
               << "\tRatio: " << ((double)m_n_dijkstra_cmp_length / m_n_dijkstra_cmp_totoal) << "\n";
        m_ofs << "\t Dijkstra compare min_id (NOT intersected): " << m_n_dijkstra_cmp_min_id
               << "\tRatio: " << ((double)m_n_dijkstra_cmp_min_id / m_n_dijkstra_cmp_totoal) << "\n";
#endif




#ifndef MCB_LEX_DIJKSTRA_IMPLEMENTATION

        this->setLexicographicOrdering();

#endif

//        printf("\n");
#ifdef DEBUG_APSP
        m_ofs << "\n";
#endif


    }





#ifndef MCB_LEX_DIJKSTRA_IMPLEMENTATION


    void setLexicographicOrdering () {

        Timer T;

        std::vector<TyLexDistLink> buffer;

        buffer.reserve( m_n_num_nodes * m_n_num_nodes );

        for (int i = 0; i < m_n_num_nodes; ++i)
        {
            for (int j = i; j < m_n_num_nodes; ++j)
            {
                TyComplexWeight & ref_spt_weight = m_v_spt_weight_container(i, j);
                buffer.emplace_back(TyLexDistLink{ref_spt_weight.m_n_distance, ref_spt_weight.m_n_length, i, j});
            }
        }

        std::sort(buffer.begin(), buffer.end());


        double used_time = T.toc();

        printf("Time used by sorting node pairs by complex-weights: %f\n", used_time);

#ifdef DEBUG_APSP
        m_ofs << "Time used by sorting node pairs by complex-weights: " << used_time << "\n";
#endif

        T.tic();

        for (auto iter = buffer.begin(), end = buffer.end(); iter != end; ++iter)
        {
            this->lexiProcessNodePair((*iter).m_n_source, (*iter).m_n_target);
        }

        used_time = T.toc();

        printf("Time used by processing lexicographic ordering: %f\n", used_time);

#ifdef DEBUG_APSP
        m_ofs << "Time used by processing lexicographic ordering: " << used_time << "\n";
#endif

    }




    inline void lexiProcessNodePair (int n_source, int n_target)
    {

        TyComplexWeight * path_s = m_v_spt_weight_container[n_source];
        TyComplexWeight * path_t = m_v_spt_weight_container[n_target];

        int dist = path_s[n_target].m_n_distance;
        int len = path_s[n_target].m_n_length;


        if (len == 0)
        {
            m_v_spt_index_container(n_source, n_target) = TySptIndex{-1, -1};
            m_v_spt_index_container(n_target, n_source) = TySptIndex{-1, -1};

            m_v_lex_info_container(n_source, n_target) = TyLexInfo{m_n_max_int, -1};
            m_v_lex_info_container(n_target, n_source) = TyLexInfo{m_n_max_int, -1};

            return;
        }



        // avoid the case when it is a self-loop, this definitely won't be a loop!! dist ==0 even for loop
        if(len == 1)
        {

            int min_id_tracker = m_n_max_int;


            const TyAdjList & s_adj = this->getAdjList(n_source);

            for (TyAdjList::iterator s_it = s_adj.begin(); s_it != s_adj.end(); ++s_it)
            {
                if ((*s_it).m_n_target_node_id == n_target)
                {
                    int tmp_min_id = (*s_it).m_n_edge_id;

                    if (tmp_min_id < min_id_tracker)
                    {
                        min_id_tracker = tmp_min_id;
                    }
                }
            }


            m_v_spt_index_container(n_source, n_target) = TySptIndex{min_id_tracker, n_source};
            m_v_spt_index_container(n_target, n_source) = TySptIndex{min_id_tracker, n_target};

            m_v_lex_info_container(n_source, n_target) = TyLexInfo{min_id_tracker, min_id_tracker};
            m_v_lex_info_container(n_target, n_source) = TyLexInfo{min_id_tracker, min_id_tracker};

            return;
        }



        int min_id_tracker = m_n_max_int;

        TyAdjList::iterator tmp_s_it, tmp_t_it;

        const TyAdjList & s_adj = this->getAdjList(n_source);


        for (TyAdjList::iterator s_it = s_adj.begin(); s_it != s_adj.end(); ++s_it)
        {
            TyComplexWeight & ref_ts_weight = path_t[s_it->m_n_target_node_id];

            if (ref_ts_weight.m_n_distance + s_it->m_n_weight == dist &&
                ref_ts_weight.m_n_length + 1 == len)
            {
                const TyAdjList & t_adj = this->getAdjList(n_target);

                for (TyAdjList::iterator t_it = t_adj.begin(); t_it != t_adj.end(); ++t_it)
                {
                    TyComplexWeight & ref_st_weight = path_s[t_it->m_n_target_node_id];

                    if (ref_st_weight.m_n_distance + t_it->m_n_weight == dist &&
                        ref_st_weight.m_n_length + 1 == len)
                    {
                        // pair

                        TyLexInfo & lexinfo_st = m_v_lex_info_container(n_source, t_it->m_n_target_node_id);
                        TyLexInfo & lexInfo_ts = m_v_lex_info_container(n_target, s_it->m_n_target_node_id);

                        if(lexinfo_st.m_n_root_edge == s_it->m_n_edge_id && lexInfo_ts.m_n_root_edge == t_it->m_n_edge_id)
                        {
                            int tmp_min_id = std::min(lexinfo_st.m_n_min_index, lexInfo_ts.m_n_min_index);

                            if (tmp_min_id < min_id_tracker)
                            {
                                min_id_tracker = tmp_min_id;
                                tmp_s_it = s_it;
                                tmp_t_it = t_it;
                            }
                        }

                    }


                }
            }
        }


        m_v_spt_index_container(n_source, n_target) = TySptIndex{(*tmp_t_it).m_n_edge_id, (*tmp_t_it).m_n_target_node_id};
        m_v_spt_index_container(n_target, n_source) = TySptIndex{(*tmp_s_it).m_n_edge_id, (*tmp_s_it).m_n_target_node_id};


        m_v_lex_info_container(n_source, n_target) = TyLexInfo{min_id_tracker, tmp_s_it->m_n_edge_id};
        m_v_lex_info_container(n_target, n_source) = TyLexInfo{min_id_tracker, tmp_t_it->m_n_edge_id};

    }


#endif




    inline TyComplexWeight & getSptWeight (int n_source_id, int n_target_id)
    {
        return m_v_spt_weight_container(n_source_id, n_target_id);
    }


    inline const TyComplexWeight & getSptWeight (int n_source_id, int n_target_id) const
    {
        return m_v_spt_weight_container(n_source_id, n_target_id);
    }


    inline TySptIndex & getSptIndex (int n_root_id, int n_leaf_id)
    {
        return m_v_spt_index_container(n_root_id, n_leaf_id);
    }


    inline const TySptIndex & getSptIndex (int n_root_id, int n_leaf_id) const
    {
        return m_v_spt_index_container(n_root_id, n_leaf_id);
    }


    inline TySptIndex * getSptIndex (int n_root_id)
    {
        return m_v_spt_index_container[n_root_id];
    }


    inline const TySptIndex * getSptIndex (int n_root_id) const
    {
        return m_v_spt_index_container[n_root_id];
    }






    void printShortestPathTree (std::ostream & os)
    {
        os << "\nShortest-Path-Tree (SPT).\n";
        os << "\tNum. of Nodes: " << m_n_num_nodes << "\n";
        os << "\tNum. of Edges: " << m_n_num_edges << "\n";

        for (int end_node = 0; end_node < m_n_num_nodes; ++end_node)
        {
            TySptIndex * spt_tree = m_v_spt_index_container[end_node];

            os << "\nSPT @ node [" << end_node << "]\n";

            for (int start_node = 0; start_node < m_n_num_nodes; ++start_node)
            {
                TySptIndex * p_start_node = spt_tree + start_node;

                os << "\tFrom [" << start_node << "] To [" << end_node << "]: ";

                while (p_start_node->m_n_parrent_node_id != -1)
                {
                    os << "[" << p_start_node->m_n_edge_id << "]";
                    TyCptEdge & edge = m_v_compact_edge_container[ p_start_node->m_n_edge_id ];
                    os << "(<" << edge.m_n_source
                       << ", " << edge.m_n_target
                       << ">, " << edge.m_n_weight << ")";

                    p_start_node = spt_tree + p_start_node->m_n_parrent_node_id;

                    if (p_start_node->m_n_parrent_node_id != -1)
                        os << " => ";
                }
                os << "\n";
            }
        }
        os << "\nEND#\n\n";
    }




};












#endif //MINIMUMCYCLEBASIS_APSP_H
