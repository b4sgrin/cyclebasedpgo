//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef MINIMUMCYCLEBASIS_SUPPORTVECTOR_H
#define MINIMUMCYCLEBASIS_SUPPORTVECTOR_H

#include <vector>
#include <algorithm>

#include <assert.h>


/**@brief See the link for comparison between vector and list.
 * <a href="https://baptiste-wicht.com/posts/2012/11/cpp-benchmark-vector-vs-list.html">link text</a>
 *
 * To conclude, we can get some facts about each data structure:
 *
std::vector is insanely faster than std::list to find an element
std::vector performs always faster than std::list with very small data
std::vector is always faster to push elements at the back than std::list
std::list handles very well large elements, especially for sorting or inserting in the front
This draw simple conclusions on usage of each data structure:

Number crunching: use std::vector
Linear search: use std::vector
Random Insert/Remove: use std::list (if data size very small (< 64B on my computer), use std::vector)
Big data size: use std::list (not if intended for searching)

 */

class SupportVector
{

private:

    std::vector<int> m_v_support_vector; /// using std::vector for number crunching.


public:

    /**@brief constructor. An explicit witness is added by line 9 */
    SupportVector(int e_i, int e_j)
    {
        assert(e_i != e_j);

        if(e_i < e_j)
        {
            m_v_support_vector.emplace_back(e_i);
            m_v_support_vector.emplace_back(e_j);
        }
        else
        {
            m_v_support_vector.emplace_back(e_j);
            m_v_support_vector.emplace_back(e_i);
        }
    };


    int Size () const
    {
        return m_v_support_vector.size();
    }

    int operator [](int n_id) const
    {
        return m_v_support_vector[n_id];
    }

    /**@brief decrease edge counter before delete this witness */
    void DecreaseEdgeCounter (int * n_edge_counter_buffer,  bool * b_cotree_edges_buffer)
    {
        for (auto item : m_v_support_vector)
            if (/* n_edge_counter_buffer[item] && */ !(--n_edge_counter_buffer[item]) )
                b_cotree_edges_buffer[item] = true;
    }


    /**@brief update support vector by inserting a new edge */
    /**@brief for vector, we can do binary search, but need to shift elements for insertion*/
    void UpdateByInsert (int n_id, int * edge_counter_buffer)
    {
        // using binary search
        auto it = std::lower_bound(m_v_support_vector.begin(), m_v_support_vector.end(), n_id);
        // insert and shift elements.
        m_v_support_vector.insert(it, n_id);
        ++edge_counter_buffer[n_id];
    }


    /**@brief update support vector by symmetric difference with another support-vector */
    void UpdateBySymmetricDifference (const SupportVector & other, int * edge_counter_buffer)
    {
        std::vector<int> result;

   //     result.reserve(this->m_v_support_vector.size() + other.m_v_support_vector.size());

        auto it1 = this->m_v_support_vector.begin();
        auto it2 = other.m_v_support_vector.begin();
        auto end1 = this->m_v_support_vector.end();
        auto end2 = other.m_v_support_vector.end();

        while(true)
        {
            if(it1 == end1)
            {
                while (it2 != end2)
                {
                    result.emplace_back(*it2);
                    ++edge_counter_buffer[*it2]; /// new elements from other
                    ++it2;
                }
                break;
            }

            if(it2 == end2)
            {
                while (it1 != end1)
                {
                    result.emplace_back(*it1);
                    ++it1;
                }
                break;
            }

            if(*it1 < *it2)
            {
                result.emplace_back(*it1);
                ++it1;
            }
            else if (*it2 < *it1)
            {
                result.emplace_back(*it2);
                ++edge_counter_buffer[*it2]; /// new elements from other
                ++it2;
            }
            else // *it1 == *it2
            {
                --edge_counter_buffer[*it2]; /// delete shared elements
                ++it1;
                ++it2;
            }
        }
        this->m_v_support_vector.swap(result);
    }


    /**@remark make sure v is sorted properly */
    bool IsOddIntersectionNum (const std::vector<int> & v_sorted)
    {
        bool b_cnt = false; /// initialized as even.

        auto it1 = this->m_v_support_vector.begin();
        auto end1 = this->m_v_support_vector.end();

        auto it2 = v_sorted.begin();
        auto end2 = v_sorted.end();

        while(it1 != end1 && it2 != end2)
        {
            if(*it1 < *it2)
            {
                ++it1;
            }
            else if (*it2 < *it1)
            {
                ++it2;
            }
            else // equal;
            {
                b_cnt = !b_cnt;
                ++it1;
                ++it2;
            }
        }

        return b_cnt;
    }



    void Print(std::ostream & os)
    {
        os << "\n";
        for (int i = 0, n = m_v_support_vector.size(); i < n; ++i)
        {
            os << m_v_support_vector[i] << " ";
        }
        os << "\n";
    }

};




#endif //MINIMUMCYCLEBASIS_SUPPORTVECTOR_H
