//
// Created by fang on 30/12/18.
//

#include "MinimumCycleBasis.h"



//#define USING_CIRCLE_GRAPH

//#define USING_BIGLOOP_GRAPH

//#define USING_TOPOLOGICAL1_GRAPH

#define USING_TOPOLOGICAL2_GRAPH

//#define USING_TOPOLOGICAL3_GRAPH


int main()
{
    MinimumCycleBasis G;


#if defined USING_CIRCLE_GRAPH


    for (int i = 0; i < 9; ++ i)
        G.addNode(i);

    G.addEdge(0, 0, 1);
    G.addEdge(1, 1, 2);
    G.addEdge(2, 1, 3);
    G.addEdge(3, 3, 5);
    G.addEdge(4, 2, 4);
    G.addEdge(5, 4, 6);
    G.addEdge(6, 5, 7);
    G.addEdge(7, 7, 8);
    G.addEdge(8, 6, 8);


#elif defined USING_BIGLOOP_GRAPH

    const int V = 30;

    int e_cnt = -1;

    for (int i = 0; i < V; ++i)
    {
        G.addNode(i);
        if (i)
        {
            ++e_cnt;
            G.addEdge(e_cnt, i-1, i);
        }
    }
    G.addEdge(++e_cnt, 0, V-1);

    /**@brief add loop-closure like edges here */
   // G.addEdge(++e_cnt, 0, 2);



#elif defined USING_TOPOLOGICAL1_GRAPH

    for (int i = 0; i < 9; ++i)
    {
        G.addNode(i);
    }

    // add edges
    int n_e_cnt = 0;

    G.addEdge(n_e_cnt, 0, 1, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 2, 8);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 2, 3, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 4, 9);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 4, 5, 10);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 5, 6, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 6, 7, 1);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 0, 8);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 7, 8, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 1, 11);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 8, 2, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 8, 6, 6);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 2, 5, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 5, 14);  ++n_e_cnt;

#elif defined USING_TOPOLOGICAL2_GRAPH


    for (int i = 0; i < 10; ++i)
    {
        G.addNode(i);
    }

    // add edges
    int n_e_cnt = 0;

    G.addEdge(n_e_cnt, 0, 1);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 2, 3);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 4, 5);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 5, 6);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 4, 6);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 6);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 6, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 8);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 8, 9);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 4, 9);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 9);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 0, 9);  ++n_e_cnt;


#elif defined USING_TOPOLOGICAL3_GRAPH

        for (int i = 0; i < 4; ++i)
        {
            G.addNode(i);
        }


        int n_e_cnt = 0;

        G.addEdge(n_e_cnt, 1, 2);  ++n_e_cnt;
        G.addEdge(n_e_cnt, 0, 2);  ++n_e_cnt;
        G.addEdge(n_e_cnt, 1, 3);  ++n_e_cnt;
        G.addEdge(n_e_cnt, 0, 3);  ++n_e_cnt;
        G.addEdge(n_e_cnt, 0, 3);  ++n_e_cnt;
        G.addEdge(n_e_cnt, 3, 2);  ++n_e_cnt;
        G.addEdge(n_e_cnt, 2, 1);  ++n_e_cnt;
        G.addEdge(n_e_cnt, 1, 0);  ++n_e_cnt;

#endif



    G.runMCB();

    std::cout<<G;










        return 0;
}

