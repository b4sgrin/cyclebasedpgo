//
// Created by fang on 18/12/18.
//

#include <iostream>

#include <unordered_map>

#include <vector>

#include "Timer.h"

using namespace std;

int main()
{
    int N = 43;

    int index = 3782;


    /// the compiler would optimize the code, and would use remainder from division computation
    int h = index / N;
    int l = index % N;

    printf("\nh = %d, n = %d\n", h, l);
    printf("\nindex = %d\n", h * N + l);


    int BigNum = 10000000;

    unordered_map<int, int> intmap;
    vector<int> intvec(BigNum);


    Timer T;

    T.tic();

    for (int i = 0; i < BigNum; ++i)
    {
        intmap.insert(std::make_pair(i, -i));
    }

    double map_cons = T.toc(); T.tic();

    for (int i = 0; i < BigNum; ++i)
    {
        intvec[i] = -i;
    }

    double vec_cons = T.toc(); T.tic();

    for (int i = 0; i < BigNum; i = i + 9)
    {
        int a = intmap[i];
    }

    double map_lookup = T.toc(); T.tic();

    for (int i = 0; i < BigNum; ++i, i = i + 9)
    {
        int a = intvec[i];
    }

    double vec_lookup = T.toc(); T.tic();



    printf("\ntime_map_construction = %f\n", map_cons);
    printf("\ntime_vec_construction = %f\n", vec_cons);
    printf("\ntime_map_lookup = %f\n", map_lookup);
    printf("\ntime_vec_lookup = %f\n", vec_lookup);


    T.tic();

    std::vector<int> vint1(10000000);

    double vec_initialization_1 = T.toc(); T.tic();

    std::vector<int> vint2(10000000, -1);

    double vec_initialization_2 = T.toc(); T.tic();

    for(int i = 0, n = vint1.size(); i < n; ++ i)
    {
        vint1[i] = 100;
    }

    double vec_initialization_3 = T.toc(); T.tic();

    for(auto item : vint1)
    {
        item = 100;
    }

    double vec_initialization_4 = T.toc(); T.tic();

    for(auto iter = vint1.begin(), end = vint1.end(); iter != end; ++iter)
    {
        *iter = 100;
    }

    double vec_initialization_5 = T.toc(); T.tic();


    int * ptr = &vint1[0];
    for(int i = 0, n = vint1.size(); i < n; ++i)
    {
        *ptr = 100; ++ptr;
    }

    double vec_initialization_6 = T.toc(); T.tic();


    int * int_ptr = &vint1[0];
    int * end_ptr = &vint1[vint1.size()];
    while (int_ptr != end_ptr)
    {
        *int_ptr = -100000; ++int_ptr;
    }

    double vec_initialization_7 = T.toc(); T.tic();



    printf("\ntime vector initialziation default = %f\n", vec_initialization_1);
    printf("\ntime vector initialziation to -1 = %f\n", vec_initialization_2);
    printf("\ntime vector initialziation for loop = %f\n", vec_initialization_3);
    printf("\ntime vector initialziation ranged based for loop = %f\n", vec_initialization_4);
    printf("\ntime vector initialziation by iterator = %f\n", vec_initialization_5);
    printf("\ntime vector initialziation by for loop & pointer = %f\n", vec_initialization_6);
    printf("\ntime vector initialziation by while loop & pointer = %f\n", vec_initialization_7);



    BigNum = 1000000;

    T.tic();
    std::vector<int> v(BigNum,10);

    double time_using_vector = T.toc(); T.tic();

    int * buf = new int[BigNum]();


    double time_using_new = T.toc(); T.tic();

    int * buffer = (int*) malloc(BigNum * sizeof(int));


    int *pos = buffer;
    int * end = buffer + BigNum;
    while(pos != end)
    {
        *pos = 10;
        ++pos;
    }

    double time_using_malloc_and_initialization = T.toc();

    printf("\ntime using vector = %f\n", time_using_vector);
    printf("\ntime using new = %f\n", time_using_new);
    printf("\ntime using malloc and initialization = %f\n", time_using_malloc_and_initialization);

    printf("\n sizeof(short) = %d\n", sizeof(short));
    printf("\n sizeof(int) = %d\n", sizeof(int));
    printf("\n sizeof(long) = %d\n", sizeof(long));
    printf("\n sizeof(int8_t) = %d\n", sizeof(std::int8_t));
    printf("\n sizeof(int16_t) = %d\n", sizeof(std::int16_t));
    printf("\n sizeof(int32_t) = %d\n", sizeof(std::int32_t));
    printf("\n sizeof(int64_T) = %d\n", sizeof(std::int64_t));



}