//
// Created by fang on 14/12/18.
//

#include <iostream>

#include "Timer.h"

#include "ShortestPaths.h"

int main()
{

    const int V = 1000000;

    int e_cnt = -1;


    Timer timer;

    //Graph G(V, V+10);

    Graph G;

    for (int i = 0; i < V; ++i)
    {
        G.addNode(i);
        if (i)
        {
            ++e_cnt;
            G.addEdge(e_cnt, i-1, i);
        }
    }
    G.addEdge(++e_cnt, 0, V-1);

    /**@brief add loop-closure like edges here */
    G.addEdge(++e_cnt, 0, 2);


    double time_construct_graph = timer.toc(); timer.tic();

    // G.printGraph(std::cout);

    int status = G.reduceGraph();


    double time_reduce_graph = timer.toc(); timer.tic();

    if (! status)
        G.printGraph(std::cout);


    double time_print_graph = timer.toc(); timer.tic();




    /**@brief Compact Graph */



    CompactGraph CG (&G);  ///

    double time_build_compact_graph = timer.toc(); timer.tic();

    CG.printGraph(std::cout);  ///

    double time_print_compact_graph = timer.toc(); timer.tic();

    CG.computeAllPairsShortestPaths();  ///

    double time_all_paris_shortest_paths = timer.toc(); timer.tic();

    CG.printShortestPathTree(std::cout);  ///

    double time_print_spt_tree = timer.toc();




    std::cout << "\nTime for graph construction : " << time_construct_graph << " s\n";
    std::cout << "\nTime for graph compression : " << time_reduce_graph << " s\n";
    std::cout << "\nTime for graph printing (container iterating) : " << time_print_graph << " s\n";

    std::cout << "\nTime for building compact graph : " << time_build_compact_graph << " s\n";
    std::cout << "\nTime for printing compact graph : " << time_print_compact_graph << " s\n";
    std::cout << "\nTime for computing all-pairs-shortest-paths : " << time_all_paris_shortest_paths << " s\n";
    std::cout << "\nTime for printing SPT tree : " << time_print_spt_tree << " s\n";



    return 0;
}