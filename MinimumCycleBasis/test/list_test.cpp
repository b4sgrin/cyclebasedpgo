//
// Created by fang on 11/02/19.
//

// erasing from list
#include <iostream>
#include <list>

int main ()
{
    std::list<int> mylist;
    std::list<int>::iterator it1,it2;

    // set some values:
    for (int i=1; i<10; ++i) mylist.push_back(i*10);

    // 10 20 30 40 50 60 70 80 90
    it1 = it2 = mylist.begin(); // ^^

    while (it1 != mylist.end())
    {
        if (*it1 ==50)
        {
            it1 = mylist.erase(it1);
            //++it1;
        }
        else
        {
            *it1 = (*it1) * 10;
            ++it1;
        }
    }


    std::cout << "mylist contains:";
    for (it1=mylist.begin(); it1!=mylist.end(); ++it1)
        std::cout << ' ' << *it1;
    std::cout << '\n';

    return 0;
}