//
// Created by fang on 18/12/18.
//

#include <iostream>

#include "Timer.h"

#include "ShortestPaths.h"

#include "IsometricCycles.h"

//#define USING_CIRCLE_GRAPH

//#define USING_BIGLOOP_GRAPH

//#define USING_TOPOLOGICAL1_GRAPH

//#define USING_TOPOLOGICAL2_GRAPH

#define USING_TOPOLOGICAL3_GRAPH

int main()
{

    Timer timer;


#if defined USING_CIRCLE_GRAPH


    /**@brief an example graph to test lexicographic ordering, and unique shortest-path, i.e., best path */
    /**@brief contain a bridge (edge 0), and big circle. */
    /**@brief Used to test switch representations, and connected component of dgraph. */
    Graph G;

    for (int i = 0; i < 9; ++ i)
        G.addNode(i);

    G.addEdge(0, 0, 1);
    G.addEdge(1, 1, 2);
    G.addEdge(2, 1, 3);
    G.addEdge(3, 3, 5);
    G.addEdge(4, 2, 4);
    G.addEdge(5, 4, 6);
    G.addEdge(6, 5, 7);
    G.addEdge(7, 7, 8);
    G.addEdge(8, 6, 8);


    double time_construct_graph = timer.toc(); timer.tic();

    G.activateAllNodes();

    double time_reduce_graph = timer.toc(); timer.tic();

    G.printGraph(std::cout);

    double time_print_graph = timer.toc(); timer.tic();


#elif defined USING_BIGLOOP_GRAPH



    const int V = 1000000;

    int e_cnt = -1;

    //Graph G(V, V+10);

    Graph G;

    for (int i = 0; i < V; ++i)
    {
        G.addNode(i);
        if (i)
        {
            ++e_cnt;
            G.addEdge(e_cnt, i-1, i);
        }
    }
    G.addEdge(++e_cnt, 0, V-1);

    /**@brief add loop-closure like edges here */
    G.addEdge(++e_cnt, 0, 2);



#elif defined USING_TOPOLOGICAL1_GRAPH

    Graph G;

        //add nodes
    for (int i = 0; i < 9; ++i)
    {
        G.addNode(i);
    }

    // add edges
    int n_e_cnt = 0;

    G.addEdge(n_e_cnt, 0, 1, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 2, 8);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 2, 3, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 4, 9);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 4, 5, 10);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 5, 6, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 6, 7, 1);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 0, 8);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 7, 8, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 1, 11);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 8, 2, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 8, 6, 6);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 2, 5, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 5, 14);  ++n_e_cnt;

#elif defined USING_TOPOLOGICAL2_GRAPH

    Graph G;

        //add nodes
    for (int i = 0; i < 10; ++i)
    {
        G.addNode(i);
    }

    // add edges
    int n_e_cnt = 0;

    G.addEdge(n_e_cnt, 0, 1);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 2, 3);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 3, 4);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 4, 5);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 5, 6);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 4, 6);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 6);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 6, 7);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 7, 8);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 8, 9);  ++n_e_cnt;

    G.addEdge(n_e_cnt, 4, 9);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 9);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 0, 9);  ++n_e_cnt;


#elif defined USING_TOPOLOGICAL3_GRAPH


    Graph G;

    for (int i = 0; i < 4; ++i)
    {
        G.addNode(i);
    }


    int n_e_cnt = 0;

    G.addEdge(n_e_cnt, 1, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 0, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 3);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 0, 3);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 0, 3);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 2, 3);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 1, 2);  ++n_e_cnt;
    G.addEdge(n_e_cnt, 0, 1);  ++n_e_cnt;

#endif




    ///-------------------------------------------------------------------
#ifndef USING_CIRCLE_GRAPH

    double time_construct_graph = timer.toc(); timer.tic();

    // G.printGraph(std::cout);

    int status = G.reduceGraph();


    double time_reduce_graph = timer.toc(); timer.tic();

    if (! status)
        G.printGraph(std::cout);


    double time_print_graph = timer.toc(); timer.tic();

#endif


    /**@brief Compact Graph */



    CompactGraph CG (&G);  ///

    double time_build_compact_graph = timer.toc(); timer.tic();

    CG.printGraph(std::cout);  ///

    double time_print_compact_graph = timer.toc(); timer.tic();

    CG.computeAllPairsShortestPaths();  ///

    double time_all_paris_shortest_paths = timer.toc(); timer.tic();

    CG.printShortestPathTree(std::cout);  ///

    double time_print_spt_tree = timer.toc(); timer.tic();

    IsometricCycles isoc (&CG);

    double time_construct_isometric_cycle_class = timer.toc(); timer.tic();

    isoc.linkHortonCycleRepresentations();

    double time_link_horton_cycle_representation = timer.toc(); timer.tic();

    isoc.computeConnectedComponents();

    double time_find_dgraph_connected_component = timer.toc(); timer.tic();

    isoc.printIsometricCycles(std::cout);

    double time_print_isometric_cycles = timer.toc(); timer.tic();


    std::cout << "\n\t ----class Graph test ----\n";
    std::cout << "\nTime for graph construction : " << time_construct_graph << " s\n";
    std::cout << "\nTime for graph compression : " << time_reduce_graph << " s\n";
    std::cout << "\nTime for graph printing (container iterating) : " << time_print_graph << " s\n";

    std::cout << "\n\t ----class CompactGraph test ----\n";
    std::cout << "\nTime for building compact graph : " << time_build_compact_graph << " s\n";
    std::cout << "\nTime for printing compact graph : " << time_print_compact_graph << " s\n";
    std::cout << "\nTime for computing all-pairs-shortest-paths : " << time_all_paris_shortest_paths << " s\n";
    std::cout << "\nTime for printing SPT tree : " << time_print_spt_tree << " s\n";

    std::cout << "\n\t ----class IsometricCycle test ----\n";
    std::cout << "\nTime to build class IsometricCycle : " << time_construct_isometric_cycle_class << " s\n";
    std::cout << "\nTime to link Horton Cycle Representations : " << time_link_horton_cycle_representation << " s\n";
    std::cout << "\nTime to compute connected component on dgraph : " << time_find_dgraph_connected_component << " s\n";
    std::cout << "\nTime to print Isometric cycles : " << time_print_isometric_cycles << " s\n";



    return 0;
}