#include <iostream>

#include "Timer.h"

#include "Graph.h"


int main() {


    const int V = 1000000;

    int e_cnt = -1;


    Timer timer;

    //Graph G(V, V+10);

    Graph G;

    for (int i = 0; i < V; ++i)
    {
        G.addNode(i);
        if (i)
        {
            ++e_cnt;
            G.addEdge(e_cnt, i-1, i);
        }
    }
    G.addEdge(++e_cnt, 0, V-1);

    /**@brief add loop-closure like edges here */
    G.addEdge(++e_cnt, 0, 2);


    double time_construct_graph = timer.toc(); timer.tic();

    // G.printGraph(std::cout);

    int status = G.reduceGraph();


    double time_reduce_graph = timer.toc(); timer.tic();

    if (! status)
        G.printGraph(std::cout);


    double time_print_graph = timer.toc();


    std::cout << "\nTime for graph construction : " << time_construct_graph << " s\n";
    std::cout << "\nTime for graph compression : " << time_reduce_graph << " s\n";
    std::cout << "\nTime for graph printing (container iterating) : " << time_print_graph << " s\n";


    return 0;
}

