//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#include <unistd.h>

#include <string>

#include <sstream> //

#include <fstream>

#include "MinimumCycleBasis.h"




bool FuncReadPoseGraphFile (const char * p_s_file_name, MinimumCycleBasis & G)
{

    std::ifstream ifs;

    ifs.open(p_s_file_name, std::ios::in);

    if (!ifs.is_open())
    {
        std::cerr << "\nFailed to Open File! Aborting..\n";
        return false;
    }

    // maximum existing ID for nodes
    int max_vidx = -1;
    int edge_id_cnt = -1;

    std::string line;
    std::string token;

    if( ifs.good() )
    {
        while (std::getline(ifs, line))
        {
            std::istringstream iss(line);

            if( std::getline(iss, token, ' ') )
                if( token.compare(0, 4, "Edge") == 0 || token.compare(0, 4, "EDGE") == 0 || token.compare("ODOMETRY") == 0 )
                {
                    // get node id1
                    std::getline(iss, token, ' ');
                    int v_id1 = atoi(token.c_str());

                    // get node id2
                    std::getline(iss, token, ' ');
                    int v_id2 = atoi(token.c_str());

                    // add nodes
                    while (v_id1 > max_vidx)
                    {
                        ++max_vidx;
                        G.addNode(max_vidx);
                    }
                    while (v_id2 > max_vidx)
                    {
                        ++max_vidx;
                        G.addNode(max_vidx);
                    }

                    // add edge
                    ++edge_id_cnt;
                    G.addEdge(edge_id_cnt, v_id1, v_id2);
                }
        }
        ifs.close();
    } else{
        std::cerr << "\nRead graph data failed! Aborting..\n";
        return false;
    }

    return true;

}



int main (int argc, char** argv)
{
    Timer T;

//    const char * p_s_file_name = "/home/fang/Code/MinimumCycleBasis/data/MITb.txt";

    const char * p_s_file_name = argv[1];

    MinimumCycleBasis G;

    T.tic();
    if( !FuncReadPoseGraphFile (p_s_file_name, G) )
        return 1;
    double time_add_data_from_file = T.toc();

    G.run();

    G.print();

    // Maybe I should add empty graph as a special case.
    std::cout<<G;

    std::cout << "\ntime_add_data_from_file = " << time_add_data_from_file << "\n";

    return 0;

}
