#!/bin/bash

function run_data ()
{
    mkdir build

    echo $1 >> terminal.txt
    echo "" >> terminal.txt

    
    echo "    a. ClassicalMethod, with Parallelism" >> terminal.txt

    rm build/CMakeCache.txt
    cd build
    cmake -DDebug_APSP=ON -DUse_LexDijkstra=OFF -DUse_Omp_Parallelism=ON ..
    make
    cd ../
    
    ./build/src/mcb $1


    echo "    b. LexDijkstra, with Parallelism" >> terminal.txt

    rm build/CMakeCache.txt    
    cd build
    cmake -DDebug_APSP=ON -DUse_LexDijkstra=ON -DUse_Omp_Parallelism=ON ..
    make
    cd ../

    ./build/src/mcb $1

    
    echo "    c. ClassicalMethod, without Parallelism" >> terminal.txt

    rm build/CMakeCache.txt    
    cd build
    cmake -DDebug_APSP=ON -DUse_LexDijkstra=OFF -DUse_Omp_Parallelism=OFF ..
    make
    cd ../

    ./build/src/mcb $1


    echo "    d. LexDijkstra, without Parallelism" >> terminal.txt
    
    rm build/CMakeCache.txt
    cd build
    cmake -DDebug_APSP=ON -DUse_LexDijkstra=ON -DUse_Omp_Parallelism=OFF ..
    make
    cd ../

    ./build/src/mcb $1


    echo "- - - -" >> terminal.txt    
}






rm terminal.txt

run_data "../data/intel.txt"
run_data "../data/M3500.txt"
run_data "../data/sphere2500.txt"
run_data "../data/INTEL.txt"
run_data "../data/MITb.txt"
run_data "../data/city10k.txt"
run_data "../data/torus10000.txt"



rm build -rf
mkdir build
cd build
cmake -DDebug_APSP=OFF -DUse_LexDijkstra=ON -DUse_Omp_Parallelism=ON ..
make
cd ../


echo " " >> terminal.txt

echo "------------intel------------------" >> terminal.txt
./build/src/mcb ../data/intel.txt >> terminal.txt
echo " " >> terminal.txt

echo "------------M3500------------------" >> terminal.txt
./build/src/mcb ../data/M3500.txt >> terminal.txt
echo " " >> terminal.txt

echo "------------sphere2500------------------" >> terminal.txt
./build/src/mcb ../data/sphere2500.txt >> terminal.txt
echo " " >> terminal.txt

echo "------------INTEL------------------" >> terminal.txt
./build/src/mcb ../data/INTEL.txt >> terminal.txt
echo " " >> terminal.txt

echo "------------MITb------------------" >> terminal.txt
./build/src/mcb ../data/MITb.txt >> terminal.txt
echo " " >> terminal.txt

echo "------------city10k------------------" >> terminal.txt
./build/src/mcb ../data/city10k.txt >> terminal.txt
echo " " >> terminal.txt

echo "------------torus10000------------------" >> terminal.txt
./build/src/mcb ../data/torus10000.txt >> terminal.txt
echo " " >> terminal.txt
