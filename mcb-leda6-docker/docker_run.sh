#!/bin/bash

ContainerName=mcbcontainer
MCBROOT=mcb-main

make


# run the container
docker stop ${ContainerName}
docker container rm ${ContainerName}
docker run -it -d --name $ContainerName mcb:1.0 /bin/bash


# copy the directory/file in
docker cp ${MCBROOT}/main/ ${ContainerName}:/opt/mcb-mcb-0.8/main/
# copy in the new Makefile
docker cp ${MCBROOT}/Makefile ${ContainerName}:/opt/mcb-mcb-0.8/Makefile


# compile inside container
docker exec -i ${ContainerName} bash -c "cd /opt/mcb-mcb-0.8 && CXX=g++-4.1 ./configure && make "

