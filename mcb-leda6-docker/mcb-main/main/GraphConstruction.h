//
// Created by fang on 27/07/18.
// fang.bai@student.uts.edu.au
// different methods to create a graph for MCB
// misc_rw

#ifndef MCB_MAIN_GRAPHCONSTRUCTION_H
#define MCB_MAIN_GRAPHCONSTRUCTION_H


#include <vector>
#include <string>
#include <sstream>


int createPoseGraphByEdgeData ( leda::graph & G, std::istream& ifs ){

    // all sequence of nodes are saved, just for indexing
    std::vector<leda::node> node_lst;

    // maximum index for nodes
    int max_vidx = -1;

    int v_id1 = 0;
    int v_id2 = 0;

    std::string line;
    std::string token;

    if( ifs.good() )
    {
        while (std::getline(ifs, line))
        {
            std::istringstream iss(line);

            if( std::getline(iss, token, ' ') )
                if( token.compare(0, 4, "Edge") == 0 || token.compare(0, 4, "EDGE") == 0 || token.compare("ODOMETRY") == 0 )
                {
                    // get node ids
                    if( std::getline(iss, token, ' ') )
                        v_id1 = atoi(token.c_str());
                    else
                        continue;

                    if( std::getline(iss, token, ' ') )
                        v_id2 = atoi(token.c_str());
                    else
                        continue;

                    // add node
                    while (v_id2 > max_vidx || v_id1 > max_vidx){
                        leda::node v = G.new_node();
                        node_lst.push_back( v );
                        ++max_vidx;
                    }
                    // add edge
                    G.new_edge (node_lst[v_id1], node_lst[v_id2]);
                }
        }
    } else{
        std::cerr << "Read graph data failed! Aborting.." << std::endl;
        return 1;
    }

    return 0;
}




#endif //MCB_MAIN_GRAPHCONSTRUCTION_H
