
// high resolution time of 1 micro second


#include <time.h>
#include <sys/time.h>
#include <math.h>


using namespace std;

class Timer
{
private:

    struct timespec start, end;

public:

    Timer ()
    {
	clock_gettime(CLOCK_MONOTONIC, &start);
    }



    void tic ()
    {
	clock_gettime(CLOCK_MONOTONIC, &start);
    }


    double toc ()
    { 
	clock_gettime(CLOCK_MONOTONIC, &end);

	double time_taken;

	time_taken = (end.tv_sec - start.tv_sec) * 1e9;
	time_taken = (time_taken + (end.tv_nsec - start.tv_nsec)) * 1e-9;
        return  time_taken;
    }

};
