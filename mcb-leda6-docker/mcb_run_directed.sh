#!/bin/bash

# firstly run docker_run.sh to compile the image and exe

ContainerName=mcbcontainer
MCBROOT=mcb-main


InputFile=$1

OutputFile=$2

SummaryFile=$3


# copy input file in to container
docker cp $InputFile ${ContainerName}:/tmp/graph.txt

docker exec -i ${ContainerName} bash -c "LD_LIBRARY_PATH=/opt/mcb-mcb-0.8:$LEDAROOT /opt/mcb-mcb-0.8/main/MCB -i /tmp/graph.txt -o /tmp/cycle.txt -a /tmp/summary.txt -d"


# copy output file out to local
docker cp ${ContainerName}:/tmp/cycle.txt $OutputFile

# copy summary file out to local
docker cp ${ContainerName}:/tmp/summary.txt $SummaryFile


