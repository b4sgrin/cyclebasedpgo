    CB-PGO vs VB-PGO C++ Software Package
Maintainer: Fang Bai (fang.bai@yahoo.com; Fang.Bai@student.uts.edu.au)

** The repository includes the C++ software for minimum cycle basis, cycle-based pose graph optimization (CB-PGO), and vertex-based pose graph optimization (VB-PGO). **

  The software is released with the publication, "sparse pose graph optimization in cycle space", by bai et al., T-RO.

---
### License ###

  The software is published under GNU General Public License (version 3). See GNU_GENERAL_PUBLIC_LICENSE.txt

  The software is free to the research communties. However if you use the software for commerical purposes, please contact Fang Bai <fang.bai@yahoo.com>

  For academic use, please cite our publication.
  
---
### What's inside each folder ###

 - **MinimumCycleBasis**: Implementation of minimum cycle basis

 - **EC-PGO**: Implementation of cycle-based PGO (** CB-PGO ** in the paper), where EC standards for edge-cycle incidence
 
 - **NE-PGO**: Implementation of vertex-based PGO (** VB-PGO ** in the paper), where NE standards for node-edge incidence
 
 - **ChordalInitialization**: Implementation of chordal initialization. Both the rotation and translation are initialized to achieve better performance.

 - **AddNoise**: Create noisy PGO dataset with groundtruth, using additive Gaussian noise
 
 - **slam_plus_plus**: External library, SLAM++ (https://sourceforge.net/p/slam-plus-plus/wiki/Home/)
 
 - **mcb-leda6-docker**: External library, Dimitrios Michail's MCB implementation based on LEDA.
 
 - **scripts**: All the scripts needed to reproduce the result of the paper "Sparse Pose Graph Optimization in Cycle Space", Bai et al., T-RO.


---
### Installation requirement ###

 - **MinimumCycleBasis**: Implemented by C++ STL only. No other requirements.
 
 - **EC-PGO**, **NE-PGO**, **ChordalInitialization**: Dependent on SLAM++.


---
### How to run the code ###

 - Download the code

        git clone git@bitbucket.org:FangBai/slam_pgo_cycle_vs_vertex.git

 - Compile the code
 
        mkdir build  
        cd build  
        cmake ..  
        make  
           
 - Run cycle-based PGO by
 
        ./bin/EC-PGO -i input_dataset_file -v output_vertex_estimate -e output_edge_estimate  
	
    or simply
   
        ./bin/EC-PGO
	
    for more options  
   	
 - Run vertex-based PGO by
 
        ./bin/NE-PGO -i input_dataset_file -v output_vertex_estimate
	
    or simply
           
        ./bin/NE-PGO
	
    for more options  	


---
### Format of the PGO input file ###

 - g2o format (https://github.com/RainerKuemmerle/g2o/wiki/File-Format)
 
 - 2D PGO format:

        EDGE2 vID1 vID2 px py o Info11 Info 12 Info13 Info 22 Info 23 Info33
       
        EDGE_SE2 vID1 vID2 px py o Info11 Info 12 Info13 Info 22 Info 23 Info33  
       
 - 3D PGO format:
 
        EDGE3 vID1 vID2 px py pz o1 o2 o3 Info11 Info12 Info13 Info14 Info15 Info16 Info22 Info23 Info24 Info25 Info26 Info33 Info34 Info35 Info36 Info44 Info45 Info46 Info55 Info56 Info66
	
        EDGE_SE3 vID1 vID2 px py pz o1 o2 o3 Info11 Info12 Info13 Info14 Info15 Info16 Info22 Info23 Info24 Info25 Info26 Info33 Info34 Info35 Info36 Info44 Info45 Info46 Info55 Info56 Info66         


---
### How to use the code as a library on your research project ###

   The implementations are all header based. It is easy to use by simply incoporating the corresponding header file, and looking at the interface functions provided in that class. Then it's done!
 
  - Minimum cycle basis: Include the header file *** MinimumCycleBasis/include/MinimumCycleBasis.h ***

  - Cycle based PGO: Include the header file *** EC-PGO/ECPGO_NonlinearSolver.h ***

  - Vertex based PGO: Include the header file *** NE-PGO/NEPGO_NonlinearSolver.h ***
  
  - ChordalInitialization: Include the header file *** ChordalInitialization/ChordalInitialization.h ***

  - Lie Group: Include the header file ** LieGroup.h ** inside the ** EC-PGO ** or ** NE-PGO ** directory.
