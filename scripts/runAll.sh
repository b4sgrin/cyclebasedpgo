#!/bin/bash

cd mcb
bash runMehlhornMichailMCB.sh
bash runProposedMCB.sh
cd ../


cd benchmarks
bash runBenchmarks.sh
cd ..

cd simulation
bash runSimData.sh
cd ..

tar -czvf result.tar.gz ../result

