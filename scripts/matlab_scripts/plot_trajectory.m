function plot_trajectory ( file_name, fig_saved_name, caption_name )

fid = fopen (file_name, 'r');

ecline = fgetl (fid);


while ischar(ecline)
    
    if (size(ecline, 2) > 0)
        
        linestr = textscan (ecline, '%s');
        
        if strcmp (linestr{1}{1}, 'Vertex') || strcmp (linestr{1}{1}, 'VERTEX')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ec_pgo_obj = [ec_pgo_obj,  str2double( linestr{1}{idx} )];
            end
        end
        
    end
   
    ecline = fgetl (fid_ecpgo);
end

fclose(fid_ecpgo);


