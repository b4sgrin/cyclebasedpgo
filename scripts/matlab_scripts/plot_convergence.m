function plot_convergence ( file_dir, fig_saved_name, caption_name )

ec_pgo_obj = [];
ec_pgo_cons = [];
ec_pgo_pert= [];
ec_pgo_odom = [];
ec_pgo_obj_vertex = [];

%file_dir = '../single/sphere2500'

startpos = 1;

fid_ecpgo = fopen ([file_dir, '/s_ecpgo.txt'], 'r');

ecline = fgetl (fid_ecpgo);


while ischar(ecline)
    
    if (size(ecline, 2) > 0)
        
        linestr = textscan (ecline, '%s');
        
        if strcmp (linestr{1}{1}, 'obj_func') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ec_pgo_obj = [ec_pgo_obj,  str2double( linestr{1}{idx} )];
            end
        end
        
        
        if strcmp (linestr{1}{1}, 'obj_func_vertex_based') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ec_pgo_obj_vertex = [ec_pgo_obj_vertex,  str2double( linestr{1}{idx} )];
            end
        end
        
        
        if strcmp (linestr{1}{1}, 'cycle_error') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ec_pgo_cons = [ec_pgo_cons,  str2double( linestr{1}{idx} )];
            end
        end
        
        
        if strcmp (linestr{1}{1}, 'perturbation') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ec_pgo_pert = [ec_pgo_pert,  str2double( linestr{1}{idx} )];
            end
        end
        
        if strcmp (linestr{1}{1}, 'odometry_error') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ec_pgo_odom = [ec_pgo_odom,  str2double( linestr{1}{idx} )];
            end
        end
        
        
    end
   
    ecline = fgetl (fid_ecpgo);
end

fclose(fid_ecpgo);



%-------%


ne_pgo_obj = [];
ne_pgo_cons = [];
ne_pgo_pert = [];
ne_pgo_odom = [];


fid_nepgo = fopen ([file_dir, '/s_nepgo.txt'], 'r');

neline = fgetl (fid_nepgo);

while ischar(neline)
    
    if (size(neline, 2) > 0)
        
        linestr = textscan (neline, '%s');
        
        if strcmp (linestr{1}{1}, 'obj_func') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ne_pgo_obj = [ne_pgo_obj,  str2double( linestr{1}{idx} )];
            end
        end
        
        if strcmp (linestr{1}{1}, 'cycle_error') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ne_pgo_cons = [ne_pgo_cons,  str2double( linestr{1}{idx} )];
            end
        end
        
        
        if strcmp (linestr{1}{1}, 'perturbation') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ne_pgo_pert = [ne_pgo_pert,  str2double( linestr{1}{idx} )];
            end
        end
        
        if strcmp (linestr{1}{1}, 'odometry_error') %&& strcmp (linestr{1}{2}, '(')
            strdim = size(linestr{1}, 1) - 1;
            for idx = 3 : strdim
                ne_pgo_odom = [ne_pgo_odom,  str2double( linestr{1}{idx} )];
            end
        end
        
    end
    
    neline = fgetl (fid_nepgo);
end

fclose(fid_nepgo);


%------------------------------------%

ec_pgo_pert = ec_pgo_pert(2:end);
ne_pgo_pert = ne_pgo_pert(2:end);


ne_pgo_odom = ne_pgo_odom + 1e-8;
ec_pgo_odom = ec_pgo_odom + 1e-8;

%------------------------------------%
%--------- plot figures ----------%
%------------------------------------%


n_data_dim = size(ne_pgo_obj, 2) - 1;

x_tick_lim = [0, n_data_dim];


ec_pgo_obj = ec_pgo_obj + 1e-2
ne_pgo_obj = ne_pgo_obj + 1e-2


p_line_wdith = 1.2

p_tick_fontsize = 12

p_label_fontsize = 15

p_legend_fontsize = 10

p_color_orange = [0.9100    0.4100    0.1700];


hFig = figure(100);


subplot(1,2,1);

hold on

    yyaxis left

    h1 = plot([startpos-1 : 1:  size(ec_pgo_obj, 2)-1], ec_pgo_obj(startpos:end),  'go-', 'LineWidth', p_line_wdith);

    h2 = plot([startpos-1 : 1 : size(ne_pgo_obj, 2)-1], ne_pgo_obj(startpos:end), 'bs--', 'LineWidth', p_line_wdith);

    hhh = plot([startpos-1 : 1:  size(ec_pgo_obj, 2)-1], ec_pgo_obj_vertex(startpos:end),  'rd-', 'LineWidth', p_line_wdith);
    
    ycolor_left = get(gca, 'YColor')
    
    yyaxis right

    h3 = plot([startpos-1 : 1 : size(ec_pgo_cons, 2)-1], ec_pgo_cons(startpos:end), 'm*-', 'LineWidth', p_line_wdith);
    
    ycolor_right = get(gca, 'YColor')

    
    legend ( [h1, h3, hhh, h2], {['\color[rgb]{', num2str(ycolor_left), '}Cycle Based PGO - ObjFunc'], ...
                                            ['\color[rgb]{', num2str(ycolor_right), '}Cycle Based PGO - ConsNorm'], ...
                                            ['\color[rgb]{', num2str(ycolor_left), '}Cycle Based PGO - ObjFuncVertex '],  ...
                                            ['\color[rgb]{', num2str(ycolor_left), '}Vertex Based PGO - ObjFunc ']}, ...
        'FontSize', p_legend_fontsize, 'Location', 'northeast' );     
    
hold off


xlabel ('Iterations', 'FontSize', p_label_fontsize);

yyaxis left
ylabel ('ObjFunc', 'FontSize', p_label_fontsize);
set(gca, 'YScale', 'log')



yyaxis right
ylabel ('ConsNorm', 'FontSize', p_label_fontsize);
set(gca, 'YScale', 'log')


ax = gca;
ax.FontSize = p_tick_fontsize; 

title([caption_name, ': ObjFunc and ConsVal'], 'Interpreter','none');

xlim (x_tick_lim);

box on
grid on


pause(0.2)
%-----------------------------------------%


subplot(1,2,2);

hold on 

    yyaxis left
    
    h4 = plot([1 : 1 : size(ec_pgo_pert, 2)], ec_pgo_pert, 'rs:', 'LineWidth', p_line_wdith);

    h5 = plot([1 : 1 : size(ne_pgo_pert, 2)], ne_pgo_pert, 'bx:', 'LineWidth', p_line_wdith);

    ycolor_left = get(gca, 'YColor')
    
    yyaxis right
    
    h6 = plot([startpos-1 : 1 :  size(ec_pgo_odom, 2)-1], ec_pgo_odom, 'ro-', 'LineWidth', p_line_wdith);

    h7 = plot([startpos-1 : 1 : size(ne_pgo_odom, 2)-1], ne_pgo_odom, 'b*-', 'LineWidth', p_line_wdith);
    
    ycolor_right = get(gca, 'YColor')
    
    legend ( [h4, h6, h5, h7], {['\color[rgb]{', num2str(ycolor_left), '}Cycle Based PGO - PertNorm'], ...
                                            ['\color[rgb]{', num2str(ycolor_right), '}Cycle Based PGO - OdomError'], ...
                                            ['\color[rgb]{', num2str(ycolor_left), '}Vertex Based PGO - PertNorm'], ...
                                            ['\color[rgb]{', num2str(ycolor_right), '}Vertex Based PGO - OdomError']}, ...
        'FontSize', p_legend_fontsize, 'Location', 'northeast' );     
    
hold off


yyaxis left
ylabel ('PertNorm', 'FontSize', p_label_fontsize);
set(gca, 'YScale', 'log')

yyaxis right
ylabel ('OdomError', 'FontSize', p_label_fontsize);
set(gca, 'YScale', 'log')


xlabel ('Iterations', 'FontSize', p_label_fontsize);

ax = gca;
ax.FontSize = p_tick_fontsize; 

xlim (x_tick_lim);

title([caption_name, ': PertNorm and OdomError'], 'Interpreter','none')

box on
grid on


pause(0.5)

%--------------------%



pos = get (hFig, 'Position');
set(hFig, 'Position', [100, 50, 1800, 300]);

pause(0.5)

hFig = tightfig(hFig);

saveas (hFig, [fig_saved_name], 'pdf');

pause(0.2)

