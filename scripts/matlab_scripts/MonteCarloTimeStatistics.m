clear all
close all
clc


file = "../../result/simulation/results.txt";

% 500 1000 4000 7000 10000 30000 50000 70000 100000
%for idx = [1, 2, 3, 4, 5, 6, 7, 8, 9]
for idx = [1, 2, 3, 4]
%for idx = [2, 3, 5, 6]
[M_mean, M_stdev, M_iter, numPoses ]= arrangeBySparsity (file, idx);

Bar (M_mean, M_stdev, M_iter, numPoses);
end




function [ne, ec, ne_chord, ec_chord ] =  readData (file, request_sparsity)

ne.numPoses = [];
ne.mean = [];
ne.stdev = [];
ne.iter = [];

ne_chord.numPoses = [];
ne_chord.mean = [];
ne_chord.stdev = [];
ne_chord.iter = [];

ec.numPoses = [];
ec.mean = [];
ec.stdev = [];
ec.iter = [];

ec_chord.numPoses = [];
ec_chord.mean = [];
ec_chord.stdev = [];
ec_chord.iter = [];


found = false;

fid= fopen (file, 'r');

line = fgetl (fid);

while ischar(line)
    
    if (size(line, 2) > 0)  % in case empity line
        
        linestr = textscan (line, '%s');
        linestr = linestr{1};
        strdim = size(linestr, 1);
        
        if strdim == 2 && strcmp(linestr{1}, 'Sparsity:')
            sparsity = str2double (linestr{2});
            found = (sparsity == request_sparsity);
            line = fgetl (fid);
            linestr = textscan (line, '%s');
            linestr = linestr{1};
            strdim = size(linestr, 1);
        end
        
        if (found && size(line, 2) > 0 && strdim > 2)
            if strcmp (linestr{3}, 'NE-PGO')
                for i = 1 : 2 : strdim
                    if strcmp (linestr{i}, 'numPoses:')
                        v = str2double (linestr{i+1});
                        ne.numPoses = [ne.numPoses, v ];
                    end
                    if strcmp (linestr{i}, 'mean')
                        v = str2double (linestr{i+1});
                        ne.mean = [ne.mean,  v];
                    end
                    if strcmp (linestr{i}, 'stdev')
                        v = str2double (linestr{i+1});
                        ne.stdev = [ne.stdev, v ];
                    end
                    if strcmp (linestr{i}, 'iter:')
                        v = str2double (linestr{i+1});
                        ne.iter = [ne.iter,  v];
                    end
                end
            end
            if strcmp (linestr{3}, 'NE-PGO-Chord')
                for i = 1 : 2 : strdim
                    if strcmp (linestr{i}, 'numPoses:')
                        v = str2double (linestr{i+1});
                        ne_chord.numPoses = [ne_chord.numPoses, v ];
                    end
                    if strcmp (linestr{i}, 'mean')
                        v = str2double (linestr{i+1});
                        ne_chord.mean = [ne_chord.mean,  v];
                    end
                    if strcmp (linestr{i}, 'stdev')
                        v = str2double (linestr{i+1});
                        ne_chord.stdev = [ne_chord.stdev, v ];
                    end
                    if strcmp (linestr{i}, 'iter:')
                        v = str2double (linestr{i+1});
                        ne_chord.iter = [ne_chord.iter,  v];
                    end
                end
            end
            if strcmp (linestr{3}, 'EC-PGO')
                for i = 1 : 2 : strdim
                    if strcmp (linestr{i}, 'numPoses:')
                        v = str2double (linestr{i+1});
                        ec.numPoses = [ec.numPoses, v ];
                    end
                    if strcmp (linestr{i}, 'mean')
                        v = str2double (linestr{i+1});
                        ec.mean = [ec.mean, v ];
                    end
                    if strcmp (linestr{i}, 'stdev')
                        v = str2double (linestr{i+1});
                        ec.stdev = [ec.stdev, v ];
                    end
                    if strcmp (linestr{i}, 'iter:')
                        v = str2double (linestr{i+1});
                        ec.iter = [ec.iter, v ];
                    end
                end
            end
            if strcmp (linestr{3}, 'EC-PGO-Chord')
                for i = 1 : 2 : strdim
                    if strcmp (linestr{i}, 'numPoses:')
                        v = str2double (linestr{i+1});
                        ec_chord.numPoses = [ec_chord.numPoses, v ];
                    end
                    if strcmp (linestr{i}, 'mean')
                        v = str2double (linestr{i+1});
                        ec_chord.mean = [ec_chord.mean,  v];
                    end
                    if strcmp (linestr{i}, 'stdev')
                        v = str2double (linestr{i+1});
                        ec_chord.stdev = [ec_chord.stdev, v ];
                    end
                    if strcmp (linestr{i}, 'iter:')
                        v = str2double (linestr{i+1});
                        ec_chord.iter = [ec_chord.iter,  v];
                    end
                end
            end
        end % found
    end % empty line
    
    line = fgetl (fid);
end

fclose(fid);

end



function [M_mean, M_stdev, M_iter, numPoses ]= arrangeBySparsity (file, idx)

[ne_001, ec_001, ne_chord_001, ec_chord_001 ] =  readData (file, 0.01);
[ne_005, ec_005, ne_chord_005, ec_chord_005 ] =  readData (file, 0.05);
[ne_010, ec_010, ne_chord_010, ec_chord_010 ] =  readData (file, 0.1);
[ne_015, ec_015, ne_chord_015, ec_chord_015 ] =  readData (file, 0.15);
[ne_020, ec_020, ne_chord_020, ec_chord_020 ] =  readData (file, 0.2);
[ne_025, ec_025, ne_chord_025, ec_chord_025 ] =  readData (file, 0.25);


ne_001.numPoses
numPoses = ne_001.numPoses(idx)

M_mean = [];
M_stdev = [];
M_iter = [];

M_mean = [M_mean;
                  ne_001.mean(idx), ne_chord_001.mean(idx), ec_001.mean(idx), ec_chord_001.mean(idx);
                  ne_005.mean(idx), ne_chord_005.mean(idx), ec_005.mean(idx), ec_chord_005.mean(idx);
                  ne_010.mean(idx), ne_chord_010.mean(idx), ec_010.mean(idx), ec_chord_010.mean(idx);
                  ne_015.mean(idx), ne_chord_015.mean(idx), ec_015.mean(idx), ec_chord_015.mean(idx);
                  ne_020.mean(idx), ne_chord_020.mean(idx), ec_020.mean(idx), ec_chord_020.mean(idx);
                  ne_025.mean(idx), ne_chord_025.mean(idx), ec_025.mean(idx), ec_chord_025.mean(idx);
                  ];
M_stdev = [M_stdev;
                  ne_001.stdev(idx), ne_chord_001.stdev(idx), ec_001.stdev(idx), ec_chord_001.stdev(idx);
                  ne_005.stdev(idx), ne_chord_005.stdev(idx), ec_005.stdev(idx), ec_chord_005.stdev(idx);
                  ne_010.stdev(idx), ne_chord_010.stdev(idx), ec_010.stdev(idx), ec_chord_010.stdev(idx);
                  ne_015.stdev(idx), ne_chord_015.stdev(idx), ec_015.stdev(idx), ec_chord_015.stdev(idx);
                  ne_020.stdev(idx), ne_chord_020.stdev(idx), ec_020.stdev(idx), ec_chord_020.stdev(idx);
                  ne_025.stdev(idx), ne_chord_025.stdev(idx), ec_025.stdev(idx), ec_chord_025.stdev(idx);
                  ];
M_iter = [M_iter;
               ne_001.iter(idx), ne_chord_001.iter(idx), ec_001.iter(idx), ec_chord_001.iter(idx); 
               ne_005.iter(idx), ne_chord_005.iter(idx), ec_005.iter(idx), ec_chord_005.iter(idx);
               ne_010.iter(idx), ne_chord_010.iter(idx), ec_010.iter(idx), ec_chord_010.iter(idx);
               ne_015.iter(idx), ne_chord_015.iter(idx), ec_015.iter(idx), ec_chord_015.iter(idx);
               ne_020.iter(idx), ne_chord_020.iter(idx), ec_020.iter(idx), ec_chord_020.iter(idx);
               ne_025.iter(idx), ne_chord_025.iter(idx), ec_025.iter(idx), ec_chord_025.iter(idx);
               ];
end



function hfig = Bar (M_mean, M_stdev, M_iter, numPoses)

hfig = figure(numPoses)

M_sum = M_mean + M_stdev;
h2 = bar(M_sum, 'FaceColor', 'w');

for i=1:length(h2)  % iterate over number of bar objects
  text(h2(i).XData +h2(i).XOffset, ...
         h2(i).YData, ...
         num2str(M_iter(:,i), '%.0f'), ...
                          'VerticalAlignment','bottom','horizontalalign','center');
end

hold on
h1 = bar(M_mean);
hold off

r1 = [1  0  0];
r2 = [0.6350  0.0780  0.1840 ];
b1 = [0  0  1];
b2 = [0  0.4470  0.7410];



% h1(1).FaceColor = r1;
% h1(2).FaceColor = r2;
% h1(3).FaceColor = b1;
% h1(4).FaceColor = b2;

if(numPoses == 10000)
legend(h1, 'VB', 'VB-Chord', 'CB-MCB', 'CB-MCB-Chord', 'Location', 'northwest', 'Box', 'off') 
end


xticks([1, 2, 3, 4, 5, 6]);
xticklabels({'1%', '5%', '10%', '15%', '20%', '25%'})

title (['Number of Poses = ', num2str(numPoses) ]);
xlabel('Graph sparsity by cycle ratio', 'FontSize', 10);
ylabel('Time (seconds)', 'FontSize', 10);

set(gca, 'FontSize', 12);

%box on
box off



%set(hfig, 'Position', [100, 50, 500, 350]);
set(hfig, 'Position', [100, 50, 450, 225]);

hfig = tightfig(hfig);

dir = '/home/fang/WORKSPACE/PHDWork/TRO2020/TRO-FinalVersion/Journal/figure/';
fig_saved_name = [dir, 'simulation_pose_num_', num2str(numPoses)];
saveas (hfig, fig_saved_name, 'pdf');


end




