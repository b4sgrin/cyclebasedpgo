clear all
close all
clc

fileDir = '../../result/benchmarks/';


filename = 'INTEL';
plotData (fileDir, filename)

filename = 'MITb';
plotData (fileDir, filename)

filename = 'sphere2500';
plotData (fileDir, filename)

filename = 'sphere2500_0.1';
plotData (fileDir, filename)

filename = 'kitti';
plotData(fileDir, filename)



function plotData (fileDir, filename)



chord_x_avg = []; % average chordal time
mcb_x_avg = []; % average mcb time


file = [fileDir, filename, '_NEPGO', '.txt']
[x_coord1, y_coord1 ] =  getFileStatistics ( file )



file = [fileDir, filename, '_NEPGO_Chord', '.txt']
[x_coord2, y_coord2 ] =  getFileStatistics ( file )

chord_x_avg = [chord_x_avg, x_coord2(1)];



file = [fileDir, filename, '_ECPGO_MCB', '.txt']
[x_coord3, y_coord3 ] =  getFileStatistics ( file )

mcb_x_avg = [mcb_x_avg, x_coord3(2) - x_coord3(1) ];



file = [fileDir, filename, '_ECPGO_MCB_Chord', '.txt']
[x_coord4, y_coord4 ] =  getFileStatistics ( file )

chord_x_avg = [chord_x_avg, x_coord4(1)];

mcb_x_avg = [mcb_x_avg,  x_coord4(2) - x_coord4(1) ];




x_coord5 = [];
y_coord5 = [];
x_coord6 = [];
y_coord6 = [];

if strcmp(filename,'MITb')
    
file = [fileDir, filename, '_ECPGO_FCB', '.txt']
[x_coord5, y_coord5 ] =  getFileStatistics ( file )



file = [fileDir, filename, '_ECPGO_FCB_Chord', '.txt']
[x_coord6, y_coord6 ] =  getFileStatistics ( file )

chord_x_avg = [chord_x_avg, x_coord6(1)];

end

chord_x_avg = mean (chord_x_avg);  % take the average chordal time

mcb_x_avg(1) 
mcb_x_avg(2) 
mcb_x_avg = mean (mcb_x_avg); % take the average mcb time



% ---- process x_coord with chord_x_avg, if chordal initialization is used
x_coord2 = x_coord2 + (chord_x_avg - x_coord2(1));
x_coord4 = x_coord4 + (chord_x_avg - x_coord4(1));
if strcmp(filename,'MITb')
 x_coord6 = x_coord6 + (chord_x_avg - x_coord6(1));   
end


%----- process x_coord with mcb_x_avg, if mcb is used
x_coord3(2:end) = x_coord3(2:end) + ( (x_coord3(1) + mcb_x_avg)  -  x_coord3(2) );
x_coord4(2:end) = x_coord4(2:end) + ( (x_coord4(1) + mcb_x_avg)  -  x_coord4(2) ) ;


%----- plot figures

hfig = figure


%-- used colors
r1 = [1  0  0];
b1 = [0  0  1];
g1 = [0  1  0];
r2 = [0.6350  0.0780  0.1840 ];
b2 = [0  0.4470  0.7410];
g2 = [0  0.5  0]; 

linewidth = 1.5;
markersize = 6;


hold on;

h1 = plot(x_coord1, y_coord1, '*-',  'Color', r1, 'LineWidth', linewidth, 'MarkerSize', markersize);
h2 = plot(x_coord2, y_coord2, 'o:',  'Color', r2, 'LineWidth', linewidth, 'MarkerSize', markersize);
h3= plot(x_coord3, y_coord3, '*-',  'Color', b1, 'LineWidth', linewidth, 'MarkerSize', markersize);
h4 = plot(x_coord4, y_coord4, 'o:',  'Color', b2, 'LineWidth', linewidth, 'MarkerSize', markersize);


if strcmp(filename,'MITb')
h5 = plot(x_coord5, y_coord5, '*-',  'Color', g1, 'LineWidth', linewidth, 'MarkerSize', markersize);
h6 = plot(x_coord6, y_coord6, 'o:',  'Color', g2, 'LineWidth', linewidth, 'MarkerSize', markersize);
end


% plot chordal, mcb point as marker.
h_chord = plot(chord_x_avg, y_coord2(1), 'kd', 'MarkerSize', 6,  'MarkerFaceColor', 'k' );
h_mcb = plot ([x_coord3(2),  x_coord4(2)],  [y_coord3(2), y_coord4(2)],  'ks', 'MarkerSize', 6, 'MarkerFaceColor', 'k' );


hold off;


set(gca, 'YScale', 'log')

if strcmp(filename, 'MITb')
    legend ([h5, h6], {'CB-FCB', 'CB-FCB-Chord'}, 'Location', 'northeast', 'Box', 'off');
end
    
if strcmp(filename, 'kitti')
    legend ([h1, h2, h3, h4, h_chord, h_mcb], {'VB', 'VB-Chord', 'CB-MCB', 'CB-MCB-Chord', 'Chord', 'MCB'}, 'Location', 'northeast', 'Box', 'off');
end


file_name = filename;
if strcmp (filename, 'MITb')
    filename = 'MITb';
    file_name = filename;
end
if strcmp (filename, 'INTEL')
    filename = 'INTEL\_P';
    file_name = 'INTEL';
end
if strcmp (filename, 'sphere2500')
    filename = 'Sphere2500';
    file_name = 'Sphere2500';
end
if strcmp (filename, 'sphere2500_0.1')
    filename = 'Sphere2500\_rot\_0.1';
    file_name = 'Sphere2500_noisy';
end
if strcmp (filename, 'sphere2500_0.15')
    filename = 'Sphere2500\_rot\_0.15';
    file_name = 'Sphere2500_noisy';
end
if strcmp (filename, 'kitti')
    filename = 'KITTI';
    file_name = 'kitti';
end


title ([filename]);
xlabel('Computational time (seconds)');
ylabel('Objective value');

box off

set(gca, 'FontSize', 12);


%set(hfig, 'Position', [100, 50, 500, 350]);
set(hfig, 'Position', [100, 50, 450, 225]);

hfig = tightfig(hfig);

dir = '/home/fang/WORKSPACE/PHDWork/TRO2020/TRO-FinalVersion/Journal/figure/';
fig_saved_name = [dir, 'benchmarks_', file_name];
saveas (hfig, fig_saved_name, 'pdf');




end







function [ x_coord, y_coord ] =  getFileStatistics ( file )

fid= fopen (file, 'r');

line = fgetl (fid);

objFunc = [];
total_time = 0;
CB_time = 0;
Chord_time = 0;
n_iteration = 0;

while ischar(line)
    
    if (size(line, 2) > 0)  % in case empity line
        
        linestr = textscan (line, '%s');
        linestr = linestr{1};
        strdim = size(linestr, 1);
        
        if strcmp (linestr{1}, 'time_chord_init')
            Chord_time = str2double(linestr{2});
        end
        
        if strcmp (linestr{1}, 'time_overall')
            for i = 2 : length(linestr)
                if strcmp (linestr{i}, 'cycle_basis:')
                    CB_time = str2double (linestr{i+1});
                end
                if strcmp (linestr{i}, 'sum:')
                    total_time = str2double (linestr{i+1});
                end
            end
        end
        
        if strcmp (linestr{1}, 'time_used')
            for i = 2 : length(linestr)
                if strcmp (linestr{i}, 'Overall:')
                    total_time = str2double (linestr{i+1});
                end
            end
        end
        
        if strcmp (linestr{1}, 'obj_func')
            objFunc = [];
            for i = 3 : length(linestr) - 1
                v = str2double (linestr{i});
                objFunc = [ objFunc,  v];
            end
        end
        
        if strcmp (linestr{1}, 'obj_func_vertex_based')
            objFunc = [];
            for i = 3 : length(linestr) - 1
                v = str2double (linestr{i});
                objFunc = [ objFunc, v ];
            end
        end
        
        if strcmp (linestr{1}, 'n_iterations:')
            n_iteration = str2double (linestr{2});
        end
        
    end
    
    line = fgetl (fid);
    
end

fclose(fid);


%  process data to x-y coordinates.
% y - cost function
% x - spent time

time_per_iter = (total_time - CB_time )/n_iteration;
y_coord = objFunc;
x_coord = time_per_iter * (1 : n_iteration);


if (CB_time > 0)
    y_coord = [objFunc(1), y_coord];
    x_coord =  CB_time + [0,  x_coord];
end
if (Chord_time > 0)
    y_coord = [objFunc(1), y_coord];
    x_coord = Chord_time + [0, x_coord];
end

x_coord = [0, x_coord];

aa = total_time  - x_coord(end);
bb = total_time + Chord_time - x_coord(end);

if( abs(aa) < 1e-10 || abs(bb) < 1e-10)
    disp (['time statistic checked ', num2str(aa), ' ' num2str(bb)]);
end

if (Chord_time > 0)
    x_coord = x_coord(2:end);
    y_coord = y_coord(2:end);
end

end


