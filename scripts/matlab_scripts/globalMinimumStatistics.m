clear all
close all
clc


dataset = 'manhattanOlson3500';
readData (dataset)

dataset = 'sphere2500';
readData (dataset)





function [ ]= readData (dataset)

dir = '../../result/global_minimum/';

file = [dir, dataset, '_', 'statistics.txt'];


fid = fopen (file, 'r');

line = fgetl (fid);

NE = [];
EC_MCB = [];
EC_FCB = [];
NE_Chord = [];
EC_MCB_Chord = [];
EC_FCB_Chord = [];

while ischar(line)
    if (size(line, 2) > 0)  % in case empity line
        
        linestr = textscan (line, '%s');
        linestr = linestr{1};
        strdim = size(linestr, 1);
        
        for i = 1 : strdim
            if strcmp (linestr{i}, 'NE:')  % NE
                v = str2double (linestr{i+1});
                NE = [NE,  v];
            end
            if strcmp (linestr{i}, 'EC_MCB:')  % EC_MCB
                v = str2double (linestr{i+1});
                EC_MCB = [EC_MCB,  v];
            end
            if strcmp (linestr{i}, 'EC_FCB:')  % EC_FCB
                v = str2double (linestr{i+1});
                EC_FCB = [EC_FCB,  v];
            end            
            if strcmp (linestr{i}, 'NE_Chord:')  % NE_Chord
                v = str2double (linestr{i+1});
                NE_Chord = [NE_Chord,  v];
            end            
            if strcmp (linestr{i}, 'EC_MCB_Chord:')  % EC_MCB_Chord
                v = str2double (linestr{i+1});
                EC_MCB_Chord = [EC_MCB_Chord,  v];
            end
            if strcmp (linestr{i}, 'EC_FCB_Chord:') % EC_FCB_Chord
                v = str2double (linestr{i+1});
                EC_FCB_Chord = [EC_FCB_Chord,  v];
            end            
        end
        
    end % empty line
    line = fgetl (fid);
end

fclose (fid);


% Ploy statistics


hfig = figure;

Dim = length (NE);


%-- used colors
r1 = [1  0  0];
b1 = [0  0  1];
g1 = [0  1  0];
r2 = [0.6350  0.0780  0.1840 ];
b2 = [0  0.4470  0.7410];
g2 = [0  0.5  0]; 

linewidth = 1.5;
markersize = 6;


hold on;

h1 = plot ([1:Dim], 100*NE, '*-',  'Color', r1, 'LineWidth', linewidth, 'MarkerSize', markersize);
h3 = plot ([1:Dim], 100*EC_MCB, '*-', 'Color', b1, 'LineWidth', linewidth, 'MarkerSize', markersize);
h5 = plot ([1:Dim], 100*EC_FCB, '*-', 'Color', g1, 'LineWidth', linewidth, 'MarkerSize', markersize);


h2 = plot ([1:Dim], 100*NE_Chord, 'o:', 'Color', r2, 'LineWidth', linewidth, 'MarkerSize', markersize);
h4 = plot ([1:Dim], 100*EC_MCB_Chord, 'o-.', 'Color', b2, 'LineWidth', linewidth, 'MarkerSize', markersize);
h6 = plot ([1:Dim], 100*EC_FCB_Chord, 'o--', 'Color', g2, 'LineWidth', linewidth, 'MarkerSize', markersize);

hold off

if strcmp(dataset,'sphere2500')
legend ([h1, h2, h3, h4, h5, h6], {'VB', 'VB-Chord', 'CB-MCB', 'CB-MCB-Chord', 'CB-FCB', 'CB-FCB-Chord'}, 'Location', 'southwest', 'Orientation','vertical', 'Box','off');
end



xticks([1:Dim]);
xticklabels({'0.01', '0.05', '0.10', '0.15', '0.20'})


dataset_name = dataset;
if strcmp (dataset, 'manhattanOlson3500')
    dataset_name = 'Manhattan3500';
    fig_title_name = 'Manhattan';
end
if strcmp (dataset, 'sphere2500')
    dataset_name = 'Sphere2500';
    fig_title_name = 'Sphere2500';
end


ytickformat('percentage');

set(gca, 'FontSize', 12);


title ([ fig_title_name ]);
xlabel('Rotational noise (rad)');
ylabel('Success rate');

if strcmp (dataset, 'sphere2500')
axes('position',[.70 .65 .15 .10])
hold on;
interest_area = [4, 5];
hs3 = plot (interest_area, 100*EC_MCB(interest_area), '*-', 'Color', b1, 'LineWidth', linewidth, 'MarkerSize', markersize);
hs2 = plot (interest_area, 100*NE_Chord(interest_area), 'o:', 'Color', r2, 'LineWidth', linewidth, 'MarkerSize', markersize);
hs4 = plot (interest_area, 100*EC_MCB_Chord(interest_area), 'o-.', 'Color', b2, 'LineWidth', linewidth, 'MarkerSize', markersize);
hs6 = plot (interest_area, 100*EC_FCB_Chord(interest_area), 'o--', 'Color', g2, 'LineWidth', linewidth, 'MarkerSize', markersize);
hold off
ytickformat('percentage');
xticks(interest_area);
xticklabels({'0.15', '0.20', 'FontSize', 12})
end


if strcmp (dataset, 'manhattanOlson3500')
axes('position',[.75 .65 .15 .10])
hold on;
interest_area = [2, 3];
hs3 = plot (interest_area, 100*EC_MCB(interest_area), '*-', 'Color', b1, 'LineWidth', linewidth, 'MarkerSize', markersize);
hs2 = plot (interest_area, 100*NE_Chord(interest_area), 'o:', 'Color', r2, 'LineWidth', linewidth, 'MarkerSize', markersize);
hs4 = plot (interest_area, 100*EC_MCB_Chord(interest_area), 'o-.', 'Color', b2, 'LineWidth', linewidth, 'MarkerSize', markersize);
hs6 = plot (interest_area, 100*EC_FCB_Chord(interest_area), 'o--', 'Color', g2, 'LineWidth', linewidth, 'MarkerSize', markersize);
hold off
ytickformat('percentage');
xticks(interest_area);
xticklabels({'0.05', '0.10', 'FontSize', 12})
end



set(hfig, 'Position', [100, 50, 450, 225]);

hfig = tightfig(hfig);

dir = '/home/fang/WORKSPACE/PHDWork/TRO2020/TRO-FinalVersion/Journal/figure/';
fig_saved_name = [dir, 'global_minimum_', dataset_name];
saveas (hfig, fig_saved_name, 'pdf');



end





