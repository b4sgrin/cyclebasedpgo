import sys, os, getopt
import random



def main ():
    
    if len(sys.argv) < 5:
        print "Please use the script in the format:\n"
        print "python <script_name> <input_filename> <output_filename> <sparsity_ratio> <num_poses>\n"
        sys.exit()


    inputfile = sys.argv[1]
    outputfile = sys.argv[2]
    sptyRatio = float(sys.argv[3])
    numPoses = int(sys.argv[4])


    if not os.path.isfile(inputfile):
        print "Input file {} does not exist.".format(inputfile)
        sys.exit()    


    # num_odom = numPoses - 1
    # num_loop / (num_odom + num_loop) = sptyRatio
    # num_loop = sptyRatio * num_odom / (1 - sptyRatio)
    numLoops = int(round( (sptyRatio * (numPoses -1)) / (1 - sptyRatio ) ))


    fin = open(inputfile, 'r')
    fout = open(outputfile, 'w')

    
    # loop closure stack
    loop_closure_stack = list()

    
    for line in fin:
        prefix, node_id1, node_id2 = obtainPrefixNodeIDs(line)

        if isVertexPrefix (prefix):
            # print 'vetex line: node_id1 = ', node_id1, '    numPoses = ', numPoses, 'node_id1 < numPoses: ', (node_id1 < numPoses)
            if (node_id1 < numPoses):
                fout.write (line) # keep this vertex
                continue
            else:
                continue # discard this vertex

        if isEdgePrefix (prefix):
            # print 'edge line: node_id1 < numPoses: ', (node_id1 < numPoses), '    node_id2 < numPoses: ', (node_id2 < numPoses)
            if isOdometry (node_id1, node_id2):
                if (node_id1 < numPoses and node_id2 < numPoses):
                    fout.write (line) # keep this odometry
                    continue
                else:
                    continue # discard this odometry
            else: # a loop-closure edge
                if (node_id1 < numPoses and node_id2 < numPoses):
                    loop_closure_stack.append (line) # save this loop-closure edge
                    continue
                else:
                    continue # discard this loop-closure edge
                
        
    print 'Loop-clousres Required:', numLoops, ' CandidateSize: ', len (loop_closure_stack)
    
    if (numLoops < len (loop_closure_stack)):
        lpcl_samples = random.sample (loop_closure_stack, numLoops)
        for line in lpcl_samples:
            fout.write (line)
    else:
        print 'Not enough candidate loop-closures in the file!'
        fin.close()
        fout.close()
        exit (1)
            

    fin.close()
    fout.close()



def obtainPrefixNodeIDs(line):
    chline = line.split(' ')
    prefix = chline[0]
    if isVertexPrefix(prefix):
        node1 = int(chline[1])
        node2 = -1
    else:
        node1 = int(chline[1])
        node2 = int(chline[2])
    return(prefix, node1, node2)


def isEdgePrefix(prefix):
    return (prefix[0:4]=='EDGE')


def isVertexPrefix(prefix):
    return (prefix[0:6]=='VERTEX')


def isOdometry(node1, node2):
    return (node2 - node1 == 1 or node1 - node2 == 1)


if __name__ == '__main__':
    main()
