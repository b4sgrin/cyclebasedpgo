import glob
import sys, os
import math


def isfloat (value):
    try:
        float (value)
        return True
    except ValueError:
        return False
    

def median(lst):
    n = len(lst)
    if n < 1:
            return None
    if n % 2 == 1:
            return sorted(lst)[n//2]
    else:
            return sum(sorted(lst)[n//2-1:n//2+1])/2.0


def mean (lst):
    if (len(lst) > 0):
        return float(sum(lst)) / len(lst)
    else:
        return .0

    
def stdev (lst):
    m = mean (lst);
    s = .0
    for item in lst:
        s = s + (item - m) * (item - m)
    s = s / ( len(lst) - 1 )
    return math.sqrt (s)


    

def checkValid (f_val):
    if ( abs(f_val - 1) < 1e-2 ):
        return 1.0
    else:
        return 0.0


def getLineInfo (line):

    lstr = line.split(' ')

    f_time_mcb = .0
    
    for n_cnt in range (len(lstr)):

        if (lstr[n_cnt] == 'mc:'):
            mc_cnt = float (lstr[n_cnt+1])

        if (lstr[n_cnt] == 'poses:'):
            num_poses = float (lstr[n_cnt+1])

        if (lstr[n_cnt] == ']'):
            flag = lstr[n_cnt-1]

        if (lstr[n_cnt] == '*n_iteration:'):
            n_iteration = float (lstr[n_cnt+1])

        if (lstr[n_cnt] == '*f_time_overall:'):
            f_time_overall = float (lstr[n_cnt+1])
                    
        if (lstr[n_cnt] == '*f_objFunc:'):
            f_objFunc = float (lstr[n_cnt+1])

        if (lstr[n_cnt] == '*globalMinimum:'):
            globalMinimum = float (lstr[n_cnt+1])

        if (lstr[n_cnt] == '*f_time_mcb:'):
            f_time_mcb = float (lstr[n_cnt+1])
            
    return (flag, mc_cnt, num_poses, n_iteration, f_time_overall, f_objFunc, globalMinimum, f_time_mcb)




def StatisiticByCase (filename, numPoses):

    time_ne_pgo = list()
    iter_ne_pgo = list()
    nglb_ne_pgo = list()

    time_ne_chord_pgo = list()
    iter_ne_chord_pgo = list()
    nglb_ne_chord_pgo = list()
    
    time_ec_pgo = list()
    iter_ec_pgo = list()
    nglb_ec_pgo = list()
    
    time_ec_chord_pgo = list()
    iter_ec_chord_pgo = list()
    nglb_ec_chord_pgo = list()
    
    mcb_ec_pgo = list()
    mcb_ec_chord_pgo = list()

    
    with open(filename) as fin:

        for line in fin:
            
            (flag, mc_cnt, num_poses, n_iteration, f_time_overall, f_objFunc, globalMinimum, f_time_mcb) = getLineInfo(line)

            if (num_poses == numPoses):

                if (flag == 'nepgo'):

                    time_ne_pgo.append (f_time_overall)

                    iter_ne_pgo.append (n_iteration)

                    nglb_ne_pgo.append ( checkValid(globalMinimum) )


                if (flag == 'nepgo_chord'):

                    time_ne_chord_pgo.append (f_time_overall)

                    iter_ne_chord_pgo.append (n_iteration)

                    nglb_ne_chord_pgo.append ( checkValid(globalMinimum) )

                    
                    
                if (flag == 'ecpgo'):

                    time_ec_pgo.append (f_time_overall)

                    iter_ec_pgo.append (n_iteration)

                    nglb_ec_pgo.append ( checkValid(globalMinimum) )

                    mcb_ec_pgo.append (f_time_mcb)

                    
                if (flag == 'ecpgo_chord'):

                    time_ec_chord_pgo.append (f_time_overall)

                    iter_ec_chord_pgo.append (n_iteration)

                    nglb_ec_chord_pgo.append ( checkValid(globalMinimum) )

                    mcb_ec_chord_pgo.append (f_time_mcb)

                    
    print 'numPoses:', numPoses, 'NE-PGO', 'time: mean', mean(time_ne_pgo), 'stdev', stdev(time_ne_pgo), 'iter:', median(iter_ne_pgo), 'Ratio:', mean(nglb_ne_pgo)

    print 'numPoses:', numPoses, 'NE-PGO-Chord', 'time: mean', mean(time_ne_chord_pgo), 'stdev', stdev(time_ne_chord_pgo), 'iter:', median(iter_ne_chord_pgo), 'Ratio:', mean(nglb_ne_chord_pgo)
    
    print 'numPoses:', numPoses, 'EC-PGO', 'time: mean', mean(time_ec_pgo), 'stdev', stdev(time_ec_pgo), 'iter:', median(iter_ec_pgo), 'Ratio:', mean(nglb_ec_pgo), 'mcb-mean', mean(mcb_ec_pgo), 'mcb-stdev', stdev(mcb_ec_pgo)

    print 'numPoses:', numPoses, 'EC-PGO-Chord', 'time: mean', mean(time_ec_chord_pgo), 'stdev', stdev(time_ec_chord_pgo), 'iter:', median(iter_ec_chord_pgo), 'Ratio:', mean(nglb_ec_chord_pgo), 'mcb-stdev', stdev(mcb_ec_chord_pgo)

    
def getPoseCases (filename):

    NumPoses = list()

    
    with open(filename) as fin:

        for line in fin:

            (flag, mc_cnt, num_poses, n_iteration, f_time_overall, f_objFunc, globalMinimum, f_time_mcb) = getLineInfo(line)

            if ( num_poses not in NumPoses ):

                NumPoses.append ( int(num_poses))

    return NumPoses



def main():


    filename = sys.argv[1]

    NumPoses = getPoseCases (filename)


    for num_poses in NumPoses:
        
        StatisiticByCase (filename, num_poses)
        




    
    
if __name__ == '__main__':
    main()
