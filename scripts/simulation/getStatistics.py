import glob
import sys, os


def isfloat (value):
    try:
        float (value)
        return True
    except ValueError:
        return False
    




def getStatistics (filename):

    n_iteration = 0

    f_objFunc = 0

    f_time_overall = 0

    f_time_mcb = 0

    f_time_chord = 0
    
    
    with open(filename) as fin:
        
        for line in fin:

            lstr = line.split(' ')

            if (lstr[0] == 'used_iterations'):
                for itt in lstr:
                    if ( isfloat (itt) ):
                        n_iteration = float (itt)


            if (lstr[0] == 'Final_ObjFunc'):
                for itt in lstr:
                    if ( isfloat (itt) ):
                        f_objFunc = float (itt)

                        
            if (lstr[0] == 'time_chord_init'):
                for itt in lstr:
                    if ( isfloat (itt) ):
                        f_time_chord = float (itt)
                        
                        
            if (lstr[0] == 'time_used'):
                for n_cnt in range (len(lstr)):
                    if ( lstr[n_cnt] == 'Overall:' ):
                        f_time_overall = float(lstr[n_cnt+1])


            if (lstr[0] == 'time_overall'):
                for n_cnt in range (len(lstr)):
                    if ( lstr[n_cnt] == 'cycle_basis:' ):
                        f_time_mcb = float(lstr[n_cnt+1])
                    if ( lstr[n_cnt] == 'sum:' ):
                        f_time_overall = float(lstr[n_cnt+1])
                        
    f_time_overall = f_time_overall + f_time_chord
    
    return (n_iteration, f_time_overall, f_objFunc, f_time_mcb)




def main():

    # mc trial cnt
    str_mc_cnt = sys.argv[1]

    # num_poses
    str_num_poses = sys.argv[2]


    # read global minimum file
    filename = 'mc/cs' + str_mc_cnt + '/' + 's_' + 'global_minimum_' + str_num_poses + '.txt'
    n_iteration, f_time_overall, f_objFunc, f_time_mcb = getStatistics (filename)
    # print '['+ filename + ']',  ' *n_iteration:', n_iteration, ' *f_time_overall:', f_time_overall, ' *f_objFunc:', f_objFunc


    # get optimal value (objFunc) of global minimum
    f_global_objFunc = f_objFunc
    

    # read NE-PGO file
    filename = 'mc/cs' + str_mc_cnt + '/' + 's_' + 'nepgo_' + str_num_poses + '.txt'
    n_iteration, f_time_overall, f_objFunc, f_time_mcb = getStatistics (filename)
    print '[', 'mc:', str_mc_cnt, 'poses:', str_num_poses, 'nepgo', ']',  ' *n_iteration:', n_iteration, ' *f_time_overall:', f_time_overall, ' *f_objFunc:', f_objFunc, ' *globalMinimum:', f_objFunc/f_global_objFunc

    
    # read NE-PGO-Chordal file
    filename = 'mc/cs' + str_mc_cnt + '/' + 's_' + 'nepgo_chord_' + str_num_poses + '.txt'
    n_iteration, f_time_overall, f_objFunc, f_time_mcb = getStatistics (filename)
    print '[', 'mc:', str_mc_cnt, 'poses:', str_num_poses, 'nepgo_chord', ']',  ' *n_iteration:', n_iteration, ' *f_time_overall:', f_time_overall, ' *f_objFunc:', f_objFunc, ' *globalMinimum:', f_objFunc/f_global_objFunc

    
    
    # read EC-PGO file
    filename = 'mc/cs' + str_mc_cnt + '/' + 's_' + 'ecpgo_' + str_num_poses + '.txt'
    n_iteration, f_time_overall, f_objFunc, f_time_mcb = getStatistics (filename)
    print '[', 'mc:', str_mc_cnt, 'poses:', str_num_poses, 'ecpgo', ']',  ' *n_iteration:', n_iteration, ' *f_time_overall:', f_time_overall, ' *f_objFunc:', f_objFunc, ' *globalMinimum:', f_objFunc/f_global_objFunc, ' *f_time_mcb:', f_time_mcb


    # read EC-PGO-Chordal file
    filename = 'mc/cs' + str_mc_cnt + '/' + 's_' + 'ecpgo_chord_' + str_num_poses + '.txt'
    n_iteration, f_time_overall, f_objFunc, f_time_mcb = getStatistics (filename)
    print '[', 'mc:', str_mc_cnt, 'poses:', str_num_poses, 'ecpgo_chord', ']',  ' *n_iteration:', n_iteration, ' *f_time_overall:', f_time_overall, ' *f_objFunc:', f_objFunc, ' *globalMinimum:', f_objFunc/f_global_objFunc

    
if __name__ == '__main__':
    main()
