#/bin/bash

NumMC=100

#SPTY_RATIO=( 0.01 0.05 0.1 0.15 0.2 0.25 )

#NUM_POSES=( 500 1000 4000 7000 10000 30000 50000 70000 100000 )


SPTY_RATIO=( 0.01 0.05 0.1 0.15 0.2 0.25 )

NUM_POSES=( 1000 4000 10000 30000 )


bin_dir=../../bin

MC_TRIALS=( $(seq 1 ${NumMC} ) )


out_root_dir=../../result/simulation

mkdir ${out_root_dir} -p



eval g2o_simulator3d -hasOdom -hasPoseSensor -nLandmarks 0 -worldSize 5000 -simSteps 100000


for sptyRatio in "${SPTY_RATIO[@]}"
do

    
    # generate data for Monte-Carlo simulation
    
    for mc_cnt in "${MC_TRIALS[@]}"
    do

	#eval g2o_simulator2d -hasOdom -hasPoseSensor -nlandmarks 0 -nSegments 0 -worldSize 1000 -simSteps 10000
		
	f_dir="mc/cs${mc_cnt}"

	mkdir ${f_dir} -p

	for numPoses in "${NUM_POSES[@]}"
	do
	    
	    python graphPrune.py simulator_out.g2o ${f_dir}/sim_${numPoses}.txt ${sptyRatio} ${numPoses}

	done

    done


    
    # run the Monte-Carlo simulation by NE-PGO and EC-PGO

    for mc_cnt in "${MC_TRIALS[@]}"
    do
	
	f_dir=mc/cs${mc_cnt}

	for numPoses in "${NUM_POSES[@]}"
	do
	    
	    eval ${bin_dir}/NE_PGO -i ${f_dir}/sim_${numPoses}.txt -g ${f_dir}/sim_${numPoses}.txt -v ${f_dir}/v_global_minimum_${numPoses}.txt -s ${f_dir}/s_global_minimum_${numPoses}.txt -n 50

	    eval ${bin_dir}/EC_PGO -i ${f_dir}/sim_${numPoses}.txt -v ${f_dir}/v_ecpgo_${numPoses}.txt -s ${f_dir}/s_ecpgo_${numPoses}.txt -n 50

	    eval ${bin_dir}/EC_PGO -c -i ${f_dir}/sim_${numPoses}.txt -v ${f_dir}/v_ecpgo_chord_${numPoses}.txt -s ${f_dir}/s_ecpgo_chord_${numPoses}.txt -n 50

	    eval ${bin_dir}/NE_PGO -i ${f_dir}/sim_${numPoses}.txt -v ${f_dir}/v_nepgo_${numPoses}.txt -s ${f_dir}/s_nepgo_${numPoses}.txt -n 50

	    eval ${bin_dir}/NE_PGO -c -i ${f_dir}/sim_${numPoses}.txt -v ${f_dir}/v_nepgo_chord_${numPoses}.txt -s ${f_dir}/s_nepgo_chord_${numPoses}.txt -n 50


	done
	
    done


    # calculate the statistics

    OutputFile="mc/results_${sptyRatio}.txt"

    for mc_cnt in "${MC_TRIALS[@]}"
    do
	
	f_dir=mc/cs${mc_cnt}

	for numPoses in "${NUM_POSES[@]}"
	do
	    
		python getStatistics.py $mc_cnt $numPoses >> $OutputFile

	done
	
    done



    # move the files to a new directory
    mv mc "mc_${sptyRatio}"

    sleep 5s
    
done


for sptyRatio in "${SPTY_RATIO[@]}"
do

    OutputFile="mc_${sptyRatio}/results_${sptyRatio}.txt"

    echo " " >> "results.txt"
    echo "Sparsity: ${sptyRatio}" >> "results.txt"
    echo "--------------------- " >> "results.txt"
    python sumStatistics.py $OutputFile >> "results.txt"
    echo " " >> "results.txt"

    mv ${OutputFile} ${out_root_dir}/

done


mv results.txt ${out_root_dir}/

rm simulator_out.g2o
rm mc_* -rf

