# a python scripts that averages the numbers in different output files (obtaind by Monte Carlo)
# python <script_name> <input_dir> <output_file>


import glob
import sys, os


def isfloat (value):
    try:
        float (value)
        return True
    except ValueError:
        return False
    




if __name__ == '__main__':


    input_dir = sys.argv[1]

    output_file = sys.argv[2]


    FILES = glob.glob( input_dir + '/*.txt')

    ofile = open (output_file, 'w')

    
    file_handles = { filename : open(filename, 'r') for filename in FILES }


    STRINGS = []

    
    for filename, filehandle in file_handles.items():
        STRINGS.append ( filehandle.read().split(' ') )


    #print STRINGS[0]
    

    iter = 0

    file_num = len (STRINGS)

    word_num = len (STRINGS[0])


    print 'output_file:', output_file
    print 'input_dir: ', input_dir
    print 'file_num: ', file_num
    print 'file_len: ', word_num
    print ' '


    for iter in range( word_num ):

        sum = 0

        for string in STRINGS:

            val = string [iter]

            if ( isfloat (val) ):

                sum = sum + float (val)

        sum = sum / file_num

        if ( sum == 0):    

            print STRINGS[0][iter],
            ofile.write (STRINGS[0][iter] + " ")

        else:
            print sum,
            ofile.write (str(sum) + " ")


    for filehandle in file_handles.values():
        filehandle.close()


    ofile.close()  

    
