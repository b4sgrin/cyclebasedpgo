import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

if __name__=="__main__":
    filename = '/home/brendon/Documents/research/cyclebasedpgo/bin/vertex.txt'

    x, y, z = [], [], []
    with open(filename) as f:
        data = f.readlines()
        for line in data:
            words = line.split()
            x.append(float(words[2]))
            y.append(float(words[3]))
            if len(words) > 5:
                z.append(float(words[4]))

    if len(z) > 0:
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.plot(x, y, z)
    else:
        plt.plot(x, y)

    plt.show()
