# append content of argv[2] to argv[1]

import glob
import sys, os



def main():

    # file 1
    file_out = sys.argv[1]
    file_object = open(file_out, 'a')

               
    
    # file 2
    file_in = sys.argv[2]

    with open(file_in) as fin:
        
        for line in fin:

            lstr = line.split(' ')

            if (lstr[0] == 'odometry_error'):
                file_object.write("\n\n")
                file_object.write(line)


            if (lstr[0] == 'obj_func_vertex_based'):
                file_object.write("\n")
                file_object.write(line)
                        
    file_object.close()
    
    
if __name__ == '__main__':
    main()
