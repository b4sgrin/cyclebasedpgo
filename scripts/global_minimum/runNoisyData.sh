#/bin/bash



function runFile ()
{

    f_data=$1
    
    n_max_trial=$2

    data_file=../../data/groundtruth/${f_data}_groundtruth.txt

    MC_TRIALS=( $(seq 1 ${n_max_trial} ) )

    out_root_dir=../../result/global_minimum

    RotaNoiseLevel=$3


    posNoise=0.1 # the translational noise is constant, set to 0.1


    for rotNoise in "${RotaNoiseLevel[@]}"
    do
	
	for mc_cnt in "${MC_TRIALS[@]}"
	do

	    # output directory
	    out_dir=${out_root_dir}/${f_data}/${f_data}_${rotNoise}/mc_${mc_cnt}

	    # noisy input file
	    noisy_file=${out_dir}/${f_data}_${rotNoise}_${mc_cnt}.txt


	    rm ${out_dir} -r
	
	    mkdir ${out_dir} -p

     
	    # create noisy dataset
	    eval ../../bin/AddNoise -i ${data_file} -r ${rotNoise} -t ${posNoise} -o ${noisy_file}


	    mkdir ${out_dir}/glob ${out_dir}/NE ${out_dir}/EC_MCB ${out_dir}/EC_FCB ${out_dir}/NE_Chord ${out_dir}/EC_MCB_Chord ${out_dir}/EC_FCB_Chord -p


	    # initialized by ground_truth, serve as global minimum
	    eval ../../bin/EC_PGO -i ${noisy_file} -g ${data_file} -v ${out_dir}/glob/v_global_minimum.txt -s ${out_dir}/glob/s_global_minimum.txt -n 50 -t 0.0001 -p

	    
	    # solved by NE_PGO
	    eval ../../bin/NE_PGO -i ${noisy_file} -v ${out_dir}/NE/vertex.txt -s ${out_dir}/NE/summary.txt -n 50 -t 0.0001 -p
	    
	    # solved by EC_PGO_MCB
	    eval ../../bin/EC_PGO -i ${noisy_file} -v ${out_dir}/EC_MCB/vertex.txt -s ${out_dir}/EC_MCB/summary.txt -n 50 -t 0.0001 -p

	    # solved by EC_PGO_FCB
	    eval ../../bin/EC_PGO_FCB -i ${noisy_file} -v ${out_dir}/EC_FCB/vertex.txt -s ${out_dir}/EC_FCB/summary.txt -n 50 -t 0.0001 -p
	    
	    # solved by NE_PGO_Chord
	    eval ../../bin/NE_PGO -i ${noisy_file} -v ${out_dir}/NE_Chord/vertex.txt -s ${out_dir}/NE_Chord/summary.txt -n 50 -t 0.0001 -p -c
	    
	    # solved by EC_PGO_MCB_Chord
	    eval ../../bin/EC_PGO -i ${noisy_file} -v ${out_dir}/EC_MCB_Chord/vertex.txt -s ${out_dir}/EC_MCB_Chord/summary.txt -n 50 -t 0.0001 -p -c

	    # solved by EC_PGO_FCB_Chord
	    eval ../../bin/EC_PGO_FCB -i ${noisy_file} -v ${out_dir}/EC_FCB_Chord/vertex.txt -s ${out_dir}/EC_FCB_Chord/summary.txt -n 50 -t 0.0001 -p -c
	    	
	done

    done
}


runStatistics ( )
{
    f_data=$1
    
    n_max_trial=$2
    
    RotaNoiseLevel=$3


    posNoise=0.1 # the translational noise is constant, set to 0.1


    out_root_dir=../../result/global_minimum


    for rotNoise in "${RotaNoiseLevel[@]}"
    do
	
	python getStatistics.py ${out_root_dir}/${f_data}/${f_data}_${rotNoise} ${n_max_trial} >> ${out_root_dir}/${f_data}_statistics.txt
	
    done
    
}




NumMC=100


RotaNoiseLevel=( 0.01 0.05 0.1 0.15 0.2 ) 

#runFile "sphere2500" $NumMC $RotaNoiseLevel 
#runStatistics "sphere2500" $NumMC $RotaNoiseLevel



runFile "manhattanOlson3500" $NumMC $RotaNoiseLevel
runStatistics "manhattanOlson3500" $NumMC $RotaNoiseLevel


