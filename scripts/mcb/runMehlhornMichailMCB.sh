#!/bin/bash



function MonteCarloRun ()
{

    # number of MonteCarlo trials
    n_max_trial=$1


    for n in $(seq 1 ${n_max_trial})
    do
      
	outfile=MehlhornMichailMCB_mcb_${n}.txt


	# remove temporary files in save dir
	rm save -rf
	mkdir save
	
	bash ./mcb_run.sh ../data/MITb.txt save/MITb_o.txt save/MITb_summary.txt

	bash ./mcb_run.sh ../data/INTEL.txt save/INTEL_o.txt save/INTEL_summary.txt

	bash ./mcb_run.sh ../data/kitti.txt save/kitti_o.txt save/kitti_summary.txt	
	
	bash ./mcb_run.sh ../data/intel.txt save/intel_o.txt save/intel_summary.txt

	bash ./mcb_run.sh ../data/M3500.txt save/M3500_o.txt save/M3500_summary.txt

	bash ./mcb_run.sh ../data/sphere2500.txt save/sphere2500_o.txt save/sphere2500_summary.txt

	bash ./mcb_run.sh ../data/city10k.txt save/city10k_o.txt save/city10k_summary.txt

	bash ./mcb_run.sh ../data/torus10000.txt save/torus10000_o.txt save/torus10000_summary.txt




	
	echo MITb >> $outfile
	echo "---------------------" >>  $outfile
	cat save/MITb_summary.txt >> $outfile
	echo " " >> $outfile

	echo INTEL >>  $outfile
	echo "---------------------" >> $outfile
	cat save/INTEL_summary.txt >> $outfile
	echo " " >>  $outfile

	echo kitti >>  $outfile
	echo "---------------------" >>  $outfile
	cat save/kitti_summary.txt >>  $outfile
	echo " " >> $outfile
	
	echo intel >> $outfile
	echo "---------------------" >>  $outfile
	cat save/intel_summary.txt >>  $outfile
	echo " " >>  $outfile

	echo M3500 >>  $outfile
	echo "---------------------" >>  $outfile
	cat save/M3500_summary.txt >> $outfile
	echo " " >> $outfile

	echo sphere2500 >> $outfile
	echo "---------------------" >>  $outfile
	cat save/sphere2500_summary.txt >>  $outfile
	echo " " >> $outfile

	echo city10k >>  $outfile
	echo "---------------------" >> $outfile
	cat save/city10k_summary.txt >>  $outfile
	echo " " >>  $outfile

	echo torus10000 >>  $outfile
	echo "---------------------" >>  $outfile
	cat save/torus10000_summary.txt >>  $outfile
	echo " " >> $outfile


    done


    # process MonteCarlo results
    
    result_root_dir=../result/mcb
    mkdir ${result_root_dir}/save -p

    mv MehlhornMichailMCB_mcb_*.txt ${result_root_dir}/save

    python ../scripts/python_scripts/avg_data.py ${result_root_dir}/save ${result_root_dir}/MehlhornMichailMCB_mcb.txt

    rm ${result_root_dir}/save -rf

    rm -rf save
}





MC_NUM=20


cd ../../mcb-leda6-docker/

# firstly run docker_run.sh to compile the image and exe
bash ./docker_run.sh

MonteCarloRun ${MC_NUM}

docker stop mcbcontainer

cd ../scripts/mcb
