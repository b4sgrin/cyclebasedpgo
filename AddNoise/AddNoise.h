//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#ifndef PGO_PROJECT_ADDNOISE_H
#define PGO_PROJECT_ADDNOISE_H


#include <iostream>

#include <string>
#include <sstream>
#include <iterator>


#include <fstream>


#include <vector>

#include <utility>

#include <ctime>

#include <random>

#include "assert.h"

#include "LieGroup.h"




namespace msc {
    template<class Container>
    void SplitString(const std::string &str, Container &container) {
        std::istringstream iss(str);
        std::copy(std::istream_iterator<std::string>(iss),
                  std::istream_iterator<std::string>(),
                  std::back_inserter(container));
    }
}




template <bool b_2D_flag>
class AddNoise {

public:

    enum
    {
        n_v_dimension = (b_2D_flag) ? 3 : 6,  /// vector dimension
        n_m_dimension = (b_2D_flag) ? 3 : 4   /// matrix dimension
    };

    typedef typename LieGroup< b_2D_flag >::_TySE _TySE;

    typedef Eigen::Matrix<double, n_m_dimension, n_m_dimension> _TyMatrix;

    typedef Eigen::Matrix<double, n_v_dimension, 1> _TyVector;


private:


    std::default_random_engine m_generator;

    std::normal_distribution<double> m_distribution;

    _TyVector m_v_noise_sigma;

    std::vector < _TyMatrix, Eigen::aligned_allocator<_TyMatrix> > m_vertex_pool;

    std::vector < std::pair<int, int> > m_edge_index_pool;

    std::vector < _TyMatrix, Eigen::aligned_allocator<_TyMatrix> > m_edge_pool;


public:

    AddNoise () :  m_generator (time(0)),
		   m_distribution(0.0, 1.0)
    {}

    ~AddNoise()
    {
        m_v_noise_sigma.setOnes();
    }


    void GenNoisyData (const char * p_s_output_file)
    {
        std::cout << "noise-level:" << m_v_noise_sigma.transpose() << "\n";

        std::ofstream ofs (p_s_output_file);

        int n_dim = (b_2D_flag) ? 2 : 3;

        if (m_vertex_pool.size() == 0)  // pure edge file
        {
            assert ( m_edge_index_pool.size() != m_edge_pool.size() && "data storage error" );

            for (size_t i = 0; i < m_edge_index_pool.size(); ++i)
            {
                ofs << "EDGE" << n_dim << " " << m_edge_index_pool[i].first << " " << m_edge_index_pool[i].second << " ";

                _TyMatrix rlvpose = m_edge_pool[i];

                _TyVector noiseSample = GaussianSampling( m_v_noise_sigma );  /// sampling noise

                _TyMatrix noisyRlvPose = rlvpose * _TySE::Exp( noiseSample );  /// noise model

                // print relative poses and information matrix.
                PrintRlvPose(ofs, noisyRlvPose);
                PrintInfoMatr( ofs );

                ofs << "\n";
            }
        }
        else
        {
            for (std::pair<int, int> & item : m_edge_index_pool)
            {
                ofs << "EDGE" << n_dim << " " << item.first << " " << item.second << " ";

                _TyMatrix rlvpose = _TySE::Inv( m_vertex_pool [item.first] ) * m_vertex_pool [item.second];

                _TyVector noiseSample = GaussianSampling( m_v_noise_sigma );  /// sampling noise

                _TyMatrix noisyRlvPose = rlvpose * _TySE::Exp( noiseSample );  /// noise model

                // print relative poses and information matrix.
                PrintRlvPose(ofs, noisyRlvPose);
                PrintInfoMatr( ofs );

                ofs << "\n";
            }
        }

        ofs.close();
    }


    bool ReadGroundTruth (const char * p_s_input_file)
    {
        std::ifstream ifs(p_s_input_file);

        if( ifs.peek() == std::ifstream::traits_type::eof() )
        {
            std::cerr << "Empty measurement data (edge) file!" << std::endl;
            return false;
        }

        if (ifs.is_open())
        {
            std::string line;
            while (std::getline(ifs, line))
            {
                std::vector<std::string> tokens;
                msc::SplitString(line, tokens);

                if( !tokens.empty() )
                {
                    if (tokens[0].compare(0, 6, "Vertex") == 0 || tokens[0].compare(0, 6, "VERTEX") == 0)
                    {
                        int n_vid = std::atoi(tokens[1].c_str());

                        if ( m_vertex_pool.size() == n_vid)
                        {
                            _TyVector v = this->GetVertexData<b_2D_flag> ( tokens );
                            m_vertex_pool.push_back( _TySE::V2M( v ) );
                        }
                        else
                        {
                            std::cerr << "Vertex indices should start from 0, and in contiguous order!";
                            return false;
                        }
                    }

                    if (tokens[0].compare(0, 4, "Edge") == 0 || tokens[0].compare(0, 4, "EDGE") == 0)
                    {
                        _TyVector e = this->GetEdgeData <b_2D_flag> ( tokens );
                        m_edge_pool.push_back( _TySE::V2M( e ) );
                        int n_v1 = std::atoi(tokens[1].c_str());
                        int n_v2 = std::atoi(tokens[2].c_str());
                        m_edge_index_pool.push_back(std::make_pair(n_v1, n_v2));
                    }
                }
            }
        }
        else{
            std::cerr << "Read edge data failed! Aborting.." << std::endl;
            return false;
        }

        ifs.close();

        printf("Vertices: %zd\t Edges: %zd\n", m_vertex_pool.size(), m_edge_index_pool.size());

        return true;
    }



    void SetSigmaDiag (const _TyVector & rv)
    {
        m_v_noise_sigma = rv;
    }



    void SetSigmaDiag (double tranNoise, double rotaNoise)
    {
        if (b_2D_flag)
        {
            m_v_noise_sigma << tranNoise, tranNoise, rotaNoise;
        }
        else
        {
            m_v_noise_sigma << tranNoise, tranNoise, tranNoise, rotaNoise, rotaNoise, rotaNoise;
        }
    }



protected:


    double GaussianRandom (double f_std_dev)
    {
        // generate random double, N (0, f_std_dev)

        double f_val = m_distribution(this->m_generator);  /// generate N(0,1) first, use same generator and distribution class, make sure it indeed generate Gaussian samples.

        while ( std::abs(f_val) > 3 )
        {
            f_val = m_distribution(this->m_generator);  /// make sure sampled value inside (-3 sigma, 3 sigma)
        }

        return f_val * f_std_dev;
    }


    _TyVector GaussianSampling (const _TyVector & v_std_dev)
    {
        _TyVector v;

        for (size_t i = 0; i < n_v_dimension; ++i)
            v(i) = this->GaussianRandom( v_std_dev(i) );

        return v;
    }


    void PrintRlvPose (std::ostream & os, const _TyMatrix & rlv_matr) const
    {
        _TyVector v_rlv = _TySE::M2V( rlv_matr );

        assert(v_rlv.rows() == n_v_dimension && "\nError: vector dimension does not match!");

        for (size_t i = 0; i < v_rlv.rows(); ++i)
            os << v_rlv(i) << " ";
    }


    void PrintInfoMatr (std::ostream & os) const
    {
        int n_zeros = n_v_dimension - 1;

        for (size_t j = 0; j < n_v_dimension; ++j)
        {
            os <<  1 / ( m_v_noise_sigma(j) * m_v_noise_sigma(j) ) << " ";

            for (int i = 0; i < n_zeros; ++i)
                os << 0 << " ";

            --n_zeros;
        }
    }




    template <bool Ty_2D>
    _TyVector GetVertexData (const std::vector<std::string> & tokens)
    {
        _TyVector v;

        if (Ty_2D)
        {
            v << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str());
        }
        else
        {
            if (tokens[0].compare("VERTEX_SE3:QUAT") == 0)
            {
                double qw = std::atof(tokens[8].c_str());

                Eigen::Vector3d rv (std::atof(tokens[5].c_str()), std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()));

                if (rv.norm() < 1e-15)
                    rv.setZero();
                else
                    rv =  ( 2 * std::acos(qw) / rv.norm() ) * rv;

                v << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()),
                        rv[0], rv[1], rv[2];
            }
            else
            {
                v << std::atof(tokens[2].c_str()), std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()),
                        std::atof(tokens[5].c_str()), std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str());
            }
        }

        return v;
    }



    template <bool Ty_2D>
    _TyVector GetEdgeData (const std::vector<std::string> & tokens)
    {
        _TyVector v;

        if (Ty_2D)
        {
            v << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str());
        }
        else
        {
            if (tokens[0].compare("EDGE_SE3:QUAT") == 0)
            {
                double qw = std::atof(tokens[9].c_str());

                Eigen::Vector3d rv (std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str()));

                if (rv.norm() < 1e-15)
                    rv.setZero();
                else
                    rv =  ( 2 * std::acos(qw) / rv.norm() ) * rv;

                v << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                        rv[0], rv[1], rv[2];
            }
            else
            {
                v << std::atof(tokens[3].c_str()), std::atof(tokens[4].c_str()), std::atof(tokens[5].c_str()),
                        std::atof(tokens[6].c_str()), std::atof(tokens[7].c_str()), std::atof(tokens[8].c_str());
            }
        }

        return v;
    }







};


#endif //PGO_PROJECT_ADDNOISE_H
