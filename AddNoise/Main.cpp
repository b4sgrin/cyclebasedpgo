//
// This file is part of the "CB-PGO vs VB-PGO C++ Software Package"
//
// Copyright and license (c) 2018-2020 Fang Bai <fang dot bai at yahoo dot com> @UTS:CAS; 
// All rights reserved.
// Maintained by Fang Bai <fang.bai@yahoo.com; fang.bai@student.uts.edu.au>
// The software is published under GNU General Public License. See GNU_GENERAL_PUBLIC_LICENSE.txt
//
// This is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the software package. If not, see <http://www.gnu.org/licenses/>.
//
// For academic use, please cite our publication:
// "Sparse Pose Graph Optimization in Cycle Space"
// Fang Bai, Teresa A. Vidal-Calleja, Giorgio Grisetti
// IEEE Transactions on Robotics
//


#include "AddNoise.h"

#include "getopt.h"

namespace msc_func
{
    template<class Container>
    void SplitString(const std::string &str, Container &container) {
        std::istringstream iss(str);
        std::copy(std::istream_iterator<std::string>(iss),
                  std::istream_iterator<std::string>(),
                  std::back_inserter(container));
    }


    int CheckDataDimension(const char *p_s_file_name) {
        int n_dimension = 0;
        std::ifstream ifs(p_s_file_name);
        std::string line;
        if (ifs.is_open()) {
            while (std::getline(ifs, line)) {
                std::vector<std::string> tokens;
                msc_func::SplitString(line, tokens);
                if (!tokens.empty()) {
                    size_t n_elements = tokens.size();
                    if (tokens[0].compare(0, 5, "Edge2") == 0 || tokens[0].compare(0, 5, "EDGE2") == 0 ||
                        tokens[0].compare(0, 8, "Edge_SE2") == 0 || tokens[0].compare(0, 8, "EDGE_SE2") == 0) {
                        n_dimension = 3;
                        assert(n_elements == 3 + n_dimension + n_dimension * (n_dimension + 1) / 2);
                        ifs.close();
                        return n_dimension;
                    } else if (tokens[0].compare(0, 5, "Edge3") == 0 || tokens[0].compare(0, 5, "EDGE3") == 0 ||
                               tokens[0].compare(0, 8, "Edge_SE3") == 0 || tokens[0].compare(0, 8, "EDGE_SE3") == 0) {
                        n_dimension = 6;
                        assert(n_elements == 3 + n_dimension + n_dimension * (n_dimension + 1) / 2);
                        ifs.close();
                        return n_dimension;
                    } else if(tokens[0].compare("Edge") == 0 ||
                              tokens[0].compare("EDGE") == 0 ||
                              tokens[0].compare("ODOMETRY") == 0)
                    {
                        if(n_elements == 12)
                        {
                            n_dimension = 3;
                            ifs.close();
                            return n_dimension;
                        }
                        else if(n_elements == 30)
                        {
                            n_dimension = 6;
                            ifs.close();
                            return n_dimension;
                        }
                    }
                } else{
                    std::cerr << "tokens are empty" << std::endl;
                }
            }
        }
        else {
            std::cerr << "Open data (edge file) failed.!" << std::endl;
            return n_dimension;
        }
        ifs.close();
        return n_dimension;
    }
}


class RunningInfo
{
public:

    char * p_s_input_file;

    char * p_s_output_file;

    bool b_2d_flag;

    double rotNoise;

    double posNoise;


    RunningInfo():
            p_s_input_file(nullptr),
            p_s_output_file(nullptr),
            b_2d_flag(true),
            rotNoise(0.01),
            posNoise(0.1)
    {}

};


template <class T>
int Run (T & addNoise, RunningInfo & info)
{

    typedef typename T::_TyVector _TyVector;

    _TyVector v;

    if (info.b_2d_flag)
    {
        v << info.posNoise, info.posNoise, info.rotNoise;
    }
    else
    {
        v << info.posNoise, info.posNoise, info.posNoise,
                info.rotNoise, info.rotNoise, info.rotNoise;
    }

    addNoise.SetSigmaDiag (v);

//
//    addNoise.SetSigmaDiag (info.posNoise, info.rotNoise);
//


    if(!addNoise.ReadGroundTruth (info.p_s_input_file))
    {
        std::cerr << "Read Ground Truth File: <" << info.p_s_output_file << "> Failed!\n";
        return 1;
    }


    if (info.p_s_output_file != nullptr)
        addNoise.GenNoisyData (info.p_s_output_file);
    else
        addNoise.GenNoisyData ("default.txt");


    return 0;
}


void print_usage(char * program)
{
    std::cout << "Usage: " << program
              << "\n\t [-i input_ground_truth_file]"
              << "\n\t [<-o output_file>]"
              << "\n\t [<-t translation noise>]"
              << "\n\t [<-r rotation noise>]"
              << "\n\t [<-h <--help-->>]"
              << std::endl;
}



int main (int argc, char** argv)
{
    RunningInfo info;

    if(argc < 2)
    {
        std::cerr << "Not enough input arguments for " << argv[0] << std::endl;
        print_usage(argv[0]);
        return 0;
    }

    /**@brief option parser*/
    opterr = 0;
    int c;
    while ((c = getopt(argc, argv, "i:o:t:r:h")) != -1)
        switch (c)
        {
            case 'i':
                info.p_s_input_file = optarg;
                break;
            case 'o':
                info.p_s_output_file = optarg;
                break;
            case 't':
                info.posNoise = std::atof (optarg);
                break;
            case 'r':
                info.rotNoise = std::atof (optarg);
                break;
            case 'h':
            case '?':
            default:
                print_usage(argv[0]);
                return 0;
        }

    if(info.p_s_input_file == NULL)
    {
        std::cerr << "Please use <-i input_ground_truth_file > !" << std::endl;
        return 1;
    }

    switch (msc_func::CheckDataDimension(info.p_s_input_file))
    {
        case 3:
            info.b_2d_flag = true;
            break;
        case 6:
            info.b_2d_flag = false;
            break;
        case 0:
        default:
            std::cerr <<"Cannot recognize data type!" << std::endl;
            return 1;
    }



    typedef AddNoise < true > AddNoise2D;
    typedef AddNoise < false > AddNoise3D;


    if (info.b_2d_flag)
    {
        AddNoise2D addNoise;
        return Run(addNoise, info);
    }
    else
    {
        AddNoise3D addNoise;
        return Run(addNoise, info);
    }


}
